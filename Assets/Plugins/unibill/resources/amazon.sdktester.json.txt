{
  "lives": {
    "itemType": "CONSUMABLE",
    "description": "Stock up on repait kits!",
    "price": 0.99,
    "smallIconUrl": "http://example.com",
    "title": "Repair Kit Stash"
  },
  "crystals": {
    "itemType": "CONSUMABLE",
    "description": "Stock up on crystals!",
    "price": 0.99,
    "smallIconUrl": "http://example.com",
    "title": "Crystals Vault"
  },
  "disableads": {
    "itemType": "ENTITLED",
    "description": "Disable all the in-game ads",
    "price": 0.99,
    "smallIconUrl": "http://example.com",
    "title": "Disable Ads"
  },
  "double_crystals": {
    "itemType": "ENTITLED",
    "description": "Gain double crystals!",
    "price": 0.99,
    "smallIconUrl": "http://example.com",
    "title": "Double Crystals"
  }
}