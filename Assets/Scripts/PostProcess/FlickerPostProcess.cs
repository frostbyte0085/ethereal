﻿using UnityEngine;
using System.Collections;

public class FlickerPostProcess : MessageBehaviour {

	public Shader shader;

	public float animationDuration = 1f;

	public float chromaticAbberationOffset = 0.0045f;
	public float uvSpeed = 50f;
	public float uvAmplitude = 0.002f;
	public float uvPhase = 50f;

	private Material flickerMat;

	private float timeStart;

	private void screenDistortEventListener(ConvexEvent evt) {
		enabled = true;
	}

	// Use this for initialization
	void Awake () {
	
		flickerMat = new Material(shader);
		enabled = false;

		registerEventListener (typeof(CEScreenDistortion), screenDistortEventListener);
	}

	void OnDestroy(){ 
		base.OnDestroy();

		if (flickerMat != null) {
			DestroyImmediate (flickerMat);
		}
	}

	void OnEnable() {
		elapsed = 0f;
		currentAnim = 0f;
		timeStart = Time.realtimeSinceStartup;
	}

	private float elapsed;
	private float currentAnim;

	void Update() {

		float timeNow = Time.realtimeSinceStartup;

		float dt = timeNow - timeStart;

		if (elapsed < 2f) {

			elapsed += dt * 1f / (animationDuration*2f);

			currentAnim = Mathf.PingPong (elapsed, 1f);
		}

		flickerMat.SetFloat ("_AnimationFactor", currentAnim);
		flickerMat.SetFloat ("_ChromaOffset", chromaticAbberationOffset);
		flickerMat.SetFloat ("_UVSpeed", uvSpeed);
		flickerMat.SetFloat ("_UVAmplitude", uvAmplitude);
		flickerMat.SetFloat ("_UVPhase", uvPhase);

		timeStart = Time.realtimeSinceStartup;

		if (elapsed >= 2f) {
			enabled = false;
		}
	}

	void OnRenderImage (RenderTexture src, RenderTexture dst) {


		Graphics.Blit (src, dst, flickerMat);
	}
}
