﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class RunInstance {

	public static void Reset() {
		CrystalsCollected = 0;
		DistanceTravelled = 0;
		NumPowerups = 0;
		Difficulty = 0;
	}

	public static uint CrystalsCollected {get; set;}
	public static uint DistanceTravelled {get;set;}
	public static uint NumPowerups {get;set;}
	public static float Difficulty {get;set;} // this is mostly for the UIResultsPopup.

	public static long CalculateScore() {
		const long distanceFactor = 4;
		const long crystalsFactor = 1;
		const long powerupFactor = 4;
		
		long score = DistanceTravelled * distanceFactor + 
			CrystalsCollected * crystalsFactor + 
				NumPowerups * powerupFactor;

		return score;
	}
}
