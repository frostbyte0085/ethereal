﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FadeCamera : MessageBehaviour {

	public Material fadeMat;
	public int renderQueue=30000;

	private Material usedFadeMat;
	private float fadeElapsed;

	private float duration;

	private Color fadeColor;
	private FadeMode lastFadeMode = FadeMode.Invalid;
	private OnFadeFinishedDelegate fadeDelegate;

	private float lastTime;

	#region event listeners
	private void fadeEventListener(ConvexEvent evt) {
		CEFade e = (CEFade)evt;

		switch (e.Mode) {
		case FadeMode.Out:
			fadeDelegate = e.OnFadeOutFinished;
			GetComponent<Camera>().enabled = true;
			break;

		case FadeMode.In:
			fadeDelegate = e.OnFadeInFinished;
			GetComponent<Camera>().enabled = true;
			break;
		}

		fadeElapsed = 0f;

		duration = e.Duration;

		lastFadeMode = e.Mode;
	}

	#endregion

	void Awake() {
		usedFadeMat = Instantiate(fadeMat) as Material;

		registerEventListener (typeof(CEFade), fadeEventListener);
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();

		DestroyImmediate (usedFadeMat);
	}

	void OnPostRender() {

		if (!GetComponent<Camera>().enabled)
			return;

		usedFadeMat.renderQueue = renderQueue;

		fadeElapsed += Time.unscaledDeltaTime * 1f / duration;

		if (lastFadeMode == FadeMode.Out) {

			fadeColor = Color.Lerp (Color.clear, Color.black, fadeElapsed);
		}
		else if (lastFadeMode == FadeMode.In) {

			fadeColor = Color.Lerp (Color.black, Color.clear, fadeElapsed);

		}

		if (fadeElapsed >= 1f) {
			if (fadeDelegate != null) {
				fadeDelegate (lastFadeMode);
			}
			fadeDelegate = null;
			
			GetComponent<Camera>().enabled = false;
		}

		usedFadeMat.SetPass (0);
		_renderFSQ();

	}

	private void _renderFSQ() {
		GL.PushMatrix();
		
		GL.LoadOrtho();
		GL.Begin(GL.QUADS);

		GL.Color (fadeColor);

		GL.TexCoord2(0,0);
		GL.Vertex3(0,0,0);

		GL.TexCoord2(1,0);
		GL.Vertex3(1,0,0);
		
		GL.TexCoord2(1,1);
		GL.Vertex3(1,1,0);
		
		GL.TexCoord2(0,1);
		GL.Vertex3(0,1,0);
		GL.End();
		
		GL.PopMatrix();
	}
}
