﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MessageBehaviour {

	public float additionalX = 1f;
	public float xoffset = 2.5f;
	public float warpdriveOrthoSize = 9.5f;

	private Transform cachedTrans;
	private CESpaceshipStatus lastSpaceshipStatus;
	private CEDifficultyUpdate lastDifficulty;

	private float originalXOffset;

	private float targetAddOffset;
	private float currentAddOffset;
	private float currentAddDampening;

	private const float warpdriveOffset = -10f;
	private float targetOrthoSize;
	private float startOrthoSize;
	private float orthoVel;

	private GamePhaseMode lastGameMode = GamePhaseMode.Invalid;

	private float minCameraX;

	private Vector3 prevPosition;
	private Vector3 currentPosition;

	private CECurrentSaveData saveData;

	private float currentAdditional;
	private float targetAdditional;

	private Camera particleCamera;
	private ParticleSystem starsPS;
	private ParticleSystem warpdriveStarsPS;
	private ParticleSystemRenderer warpdriveStarsPSRenderer;
	private float targetStarsScale;
	private float starsScaleVel;

	private Quaternion startStarsRot;
	private Quaternion startWarpdriveStarsRot;

	private bool isInWarpdrive;
	#region event listeners
	private void spaceshipStatusEventListener(ConvexEvent evt) {
		CESpaceshipStatus e = (CESpaceshipStatus)evt;

		// snap the camera here
		if (lastSpaceshipStatus == null) {
			snapCameraToShip(e);
		}

		lastSpaceshipStatus = e;
	}

	private void currentSaveDataEventListener (ConvexEvent evt) {
		CECurrentSaveData e = (CECurrentSaveData)evt;

		saveData = e;

		if (e.NeedsTutorial) {
			targetAdditional = 0f;
		}
		else {
			targetAdditional = additionalX;
		}
	}

	private void warpdriveCollectEventListener(ConvexEvent evt) {
		CEWarpdriveCollect e = (CEWarpdriveCollect)evt;

		if (e.Phase == WarpdrivePhase.Start) {
			targetAddOffset = warpdriveOffset;
			currentAddOffset = 0f;
			currentAddDampening = 0.65f;
			targetOrthoSize = warpdriveOrthoSize;

			warpdriveStarsPS.Play();
			targetStarsScale = 25f;

			isInWarpdrive = true;
		}
		else if (e.Phase == WarpdrivePhase.End) {
			targetAddOffset = 0f;
			currentAddDampening = 0.45f;
			targetOrthoSize = startOrthoSize;

			warpdriveStarsPS.Stop();
			targetStarsScale = 0f;

			isInWarpdrive = false;
		}
	}

	private void difficultyEventListener(ConvexEvent evt) {
		CEDifficultyUpdate e = (CEDifficultyUpdate)evt;

		lastDifficulty = e;
	}

	private void gamePhaseEventListener(ConvexEvent evt) {
		CEGamePhase e = (CEGamePhase)evt;

		if (lastGameMode != e.Mode) {
			switch (e.Mode) {
			case GamePhaseMode.Gameplay:
				starsPS.Play();
				break;

			case GamePhaseMode.Lose:
				break;
			}

		}
		lastGameMode = e.Mode;
	}

	private void diedEventListener(ConvexEvent evt) {
		CEDied e = (CEDied)evt;

		starsPS.Stop();
	}

	private void continueEventListener (ConvexEvent evt) {
		starsPS.Play();
	}

	#endregion

	private float startY;

	// Use this for initialization
	void Awake () {
		cachedTrans = transform;

		startOrthoSize = GetComponent<Camera>().orthographicSize;
		targetOrthoSize = startOrthoSize;

		registerEventListener (typeof(CESpaceshipStatus), spaceshipStatusEventListener);
		registerEventListener (typeof(CEDifficultyUpdate), difficultyEventListener);
		registerEventListener (typeof(CEGamePhase), gamePhaseEventListener);
		registerEventListener (typeof(CEDied), diedEventListener);
		registerEventListener (typeof(CEWarpdriveCollect), warpdriveCollectEventListener);
		registerEventListener (typeof(CECurrentSaveData), currentSaveDataEventListener);
		registerEventListener (typeof(CEContinue), continueEventListener);

		particleCamera = transform.Find("ParticleEffectsCamera").GetComponent<Camera>();
		starsPS = transform.Find ("vfx_stars").GetComponent<ParticleSystem>();
		warpdriveStarsPS = transform.Find ("vfx_warpdrive_stars").GetComponent<ParticleSystem>();
		warpdriveStarsPSRenderer = warpdriveStarsPS.GetComponent <ParticleSystemRenderer>();

		startWarpdriveStarsRot = warpdriveStarsPS.transform.rotation;
		startStarsRot = starsPS.transform.rotation;

		startY = cachedTrans.position.y;
		originalXOffset = xoffset;

		minCameraX = cachedTrans.position.x;

		prevPosition = cachedTrans.position;
	}

	private float moveVel;

	private void snapCameraToShip(CESpaceshipStatus status) {
		Vector3 camPos = cachedTrans.position;
		camPos.x = status.ShipTransform.position.x + xoffset;
		cachedTrans.position = camPos;
	}

	private float offsetAddVel;

	// Update is called once per frame
	void LateUpdate () {
	
		if (lastSpaceshipStatus == null)
			return;

		currentAddOffset = Mathf.SmoothDamp (currentAddOffset, targetAddOffset, ref offsetAddVel, currentAddDampening);

		updateOffsetRatio();

		switch (lastGameMode) {
		case GamePhaseMode.Gameplay:
			updateGameplayMode();
			break;

		case GamePhaseMode.Lose:
			updateLoseMode();
			break;

		}
	}

	private void updateOffsetRatio() {

		if (saveData == null) return;

		if (saveData.NeedsTutorial) {
			currentAdditional = 0f;
		}
		else {
			currentAdditional = Mathf.Lerp (currentAdditional, targetAdditional, Time.deltaTime);
		}

		// update the xoffset based on the aspect ratio
		float refRatio = ((float)Screen.width/(float)Screen.height) / (4f/3f);
		xoffset = originalXOffset * refRatio + currentAddOffset + currentAdditional;
		//
	}

	private void updateMenuMode() {
		// simply snap the camera to the ship adding the xoffset
		snapCameraToShip(lastSpaceshipStatus);

	}

	private void updateGameplayMode() {

		if (lastDifficulty == null && lastSpaceshipStatus != null)
			return;

		if (!lastSpaceshipStatus.IsAlive)
			return;

		prevPosition = currentPosition;

		// change the orthographic size while we're transitioning in/out of warpdrive mode
		GetComponent<Camera>().orthographicSize = Mathf.SmoothDamp (GetComponent<Camera>().orthographicSize, targetOrthoSize, ref orthoVel, 1f);
		particleCamera.orthographicSize = GetComponent<Camera>().orthographicSize;

		// animate the stars scale and speed for the warpdrive effect
		warpdriveStarsPSRenderer.lengthScale = Mathf.SmoothDamp (warpdriveStarsPSRenderer.lengthScale, targetStarsScale, ref starsScaleVel, 0.2f);

		// zero those out, to prevent world space simulation artifacts while shaking the camera's rotation
		warpdriveStarsPS.transform.rotation = startWarpdriveStarsRot;
		starsPS.transform.rotation = startStarsRot;
		Vector3 starsPos = warpdriveStarsPS.transform.position;
		starsPos.y = startY;
		warpdriveStarsPS.transform.position = starsPos;
		//

		// update the x if we're moving.
		Vector3 camPos = cachedTrans.position;
		camPos.x = Mathf.Max(minCameraX, lastSpaceshipStatus.ShipTransform.position.x + xoffset);
		cachedTrans.position = camPos;

		currentPosition = cachedTrans.position;

		DeltaPos = (currentPosition - prevPosition);
		postEvent (new CECameraStatus(DeltaPos));
	}

	private void updateLoseMode() {

	}

	public Vector3 DeltaPos {
		get; private set;
	}
}
