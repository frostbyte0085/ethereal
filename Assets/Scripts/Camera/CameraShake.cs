﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraShake : MessageBehaviour {
	
	private float elapsed;
	private float currentMagnitude;
	private float currentDuration;
	private float currentRotation;
	private Vector3 currentOffset;
	private Vector3 currentEuler;
	private bool isShaking;

	private Transform cachedTrans;
	private float startY;

	private void cameraShakeEventListener(ConvexEvent evt) {
		CECameraShake e = (CECameraShake)evt;

		isShaking = true;
		elapsed = 0f;
		currentOffset = Vector3.zero;
		currentMagnitude = e.Magnitude;
		currentDuration = e.Duration;
		currentRotation = e.Rotation;
	}

	// Use this for initialization
	void Start () {
		isShaking = false;
		cachedTrans = transform;

		startY = cachedTrans.position.y;

		registerEventListener (typeof(CECameraShake), cameraShakeEventListener);
	}

	void LateUpdate() {

		cachedTrans.position -= currentOffset;
		cachedTrans.eulerAngles -= currentEuler;
		currentOffset = Vector3.zero;

		if (isShaking) {
			if (elapsed < 1f) {

				elapsed += Time.unscaledDeltaTime * 1f / currentDuration;

				currentOffset = new Vector3(Random.Range(-1f,1f), Random.Range(-1f,1f), 0f) * Mathf.Lerp(currentMagnitude, 0f, elapsed);
				currentEuler = new Vector3(0,0,Random.Range(-1f,1f)) * Mathf.Lerp(currentRotation, 0f, elapsed);

				if (elapsed >= 1f) {
					isShaking = false;
					currentOffset = Vector3.zero;
					Vector3 p = cachedTrans.position;
					p.y = startY;
					cachedTrans.position = p;
					cachedTrans.rotation = Quaternion.identity;
				}
			}
		}

		cachedTrans.eulerAngles += currentEuler;
		cachedTrans.position += currentOffset;
	}
}
