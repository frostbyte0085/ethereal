﻿using UnityEngine;
using System.Collections;

public enum AdActionType {
	ShowVideo,
	ShowVideoForReward,
	ShowRandom
}

public class CEDoAd : ConvexEvent {

	public CEDoAd(AdActionType type, int giveCrystals=0, bool checkForAds=true) {
		Type = type;
		GiveCrystals = giveCrystals;
		CheckForAds = checkForAds;
	}

	public int GiveCrystals {
		get; private set;
	}

	public bool CheckForAds {
		get; private set;
	}

	public AdActionType Type {
		get; private set;
	}
}
