using System.Collections;

public class CEChangeScreen : ConvexEvent {

	public CEChangeScreen (System.Type type) {
		ClassType = type;
	}

	public System.Type ClassType {
		get; private set;
	}

}