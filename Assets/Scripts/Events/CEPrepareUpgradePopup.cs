using System.Collections;

public class CEPrepareUpgradePopup : ConvexEvent {

	public CEPrepareUpgradePopup(SpaceshipMenu ship, StatType stat, int nextLevel) {
		NextLevel = nextLevel;
		Ship = ship;
		Stat = stat;
	}

	public SpaceshipMenu Ship {
		get; private set;
	}

	public StatType Stat {
		get; private set;
	}

	public int NextLevel {
		get; private set;
	}

}