using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ChangeShipDirection {
	Next,
	Previous,
	Default
}

public class CEChangeShip : ConvexEvent {

	public CEChangeShip(ChangeShipDirection direction) {
		Direction = direction;
	}

	// this is simply used to update the index
	public CEChangeShip (string shipName) {
		ShipName = shipName;
	}

	public ChangeShipDirection Direction {
		get; private set;
	}

	public string ShipName {
		get; private set;
	}
}