using System.Collections;
using UnityEngine;

public enum WarpdrivePhase {
	Start,
	End
}

public class CEWarpdriveCollect : ConvexEvent {

	public CEWarpdriveCollect(WarpdrivePhase phase, float duration) {
		Phase = phase;
		Duration = duration;
	}

	public CEWarpdriveCollect(WarpdrivePhase phase) {
		Phase = phase;
	}

	public WarpdrivePhase Phase {
		get; private set;
	}

	public float Duration {
		get; private set;
	}
}