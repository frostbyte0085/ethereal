using System.Collections;
using UnityEngine;


public class CEInputState : ConvexEvent {

	public CEInputState (bool enabled) {
		IsEnabled = enabled;
	}

	public bool IsEnabled {
		get; private set;
	}
}