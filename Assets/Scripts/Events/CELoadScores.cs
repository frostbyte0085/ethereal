using System.Collections;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class CELoadScores : ConvexEvent {

	public CELoadScores (string id, UserScope userScope, TimeScope timeScope, Range range) {
		ID = id;
		UserScope = userScope;
		TimeScope = timeScope;
		Range = range;
	}

	public UserScope UserScope {
		get; private set;
	}

	public TimeScope TimeScope {
		get; private set;
	}

	public Range Range {
		get; private set;
	}

	public string ID {
		get; private set;
	}
}