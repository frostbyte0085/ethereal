using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CESocialShare : ConvexEvent {

	public CESocialShare(SocialMedia media) {
		Media = media;
	}

	public SocialMedia Media {
		get; private set;
	}
}