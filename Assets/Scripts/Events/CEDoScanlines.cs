using UnityEngine;
using System.Collections;

public class CEDoScanlines : ConvexEvent {

	public CEDoScanlines(bool enabled) {
		Enabled = enabled;
	}

	public bool Enabled {
		get; private set;
	}
}