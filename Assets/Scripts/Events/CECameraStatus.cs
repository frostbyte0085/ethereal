using UnityEngine;
using System.Collections;

public class CECameraStatus : ConvexEvent {

	public CECameraStatus(Vector3 delta) {
		DeltaPos = delta;
	}

	public Vector3 DeltaPos {
		get; private set;
	}
}