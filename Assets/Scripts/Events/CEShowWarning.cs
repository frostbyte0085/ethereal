﻿using UnityEngine;
using System.Collections;

public class CEShowWarning : ConvexEvent {

	public CEShowWarning (string title, string sound) {
		Title = title;
		Sound = sound;
	}

	public string Title {
		get; private set;
	}

	public string Sound {
		get; private set;
	}
}
