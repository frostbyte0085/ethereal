using System.Collections;
using UnityEngine;

public class CESpaceshipUnlock : ConvexEvent {

	public CESpaceshipUnlock(SpaceshipType type) {
		Type = type;
	}

	public SpaceshipType Type {
		get; private set;
	}
}