﻿using UnityEngine;
using System.Collections;

public class CEUpgradeStat : ConvexEvent {

	public CEUpgradeStat(SpaceshipType type, StatType stat) {
		ShipType = type;
		Stat = stat;
	}

	public SpaceshipType ShipType {
		get; private set;
	}

	public StatType Stat {
		get; private set;
	}

}
