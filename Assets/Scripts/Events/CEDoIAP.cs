﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CEDoIAP : ConvexEvent {
	
	public CEDoIAP(string id) {
		ID = id;
	}

	public string ID {
		get; private set;
	}
}
