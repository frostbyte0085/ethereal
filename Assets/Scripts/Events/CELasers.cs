using System.Collections;

public enum LaserMode {
	Enabled,
	Disabled
}

public class CELasers : ConvexEvent {

	public CELasers(LaserMode mode) {
		Mode = mode;
	}

	public LaserMode Mode {
		get; private set;
	}
}