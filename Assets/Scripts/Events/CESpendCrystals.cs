using System;
using System.Collections;

public class CESpendCrystals : ConvexEvent {

	public CESpendCrystals(uint amount) {
		Amount = amount;
	}

	public uint Amount {
		get; private set;
	}
}