using System.Collections;
using UnityEngine;

public class CEPushPopup : ConvexEvent {

	public CEPushPopup (System.Type type) {
		ClassType = type;
	}

	public System.Type ClassType {
		get; private set;
	}
}