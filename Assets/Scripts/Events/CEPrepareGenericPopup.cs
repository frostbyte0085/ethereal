using System;
using System.Collections;

public class CEPrepareGenericPopup : ConvexEvent {

	public CEPrepareGenericPopup(string title, string body, string acceptText, string cancelText,
	                             OnGenericButtonClickedDelegate onAccept, OnGenericButtonClickedDelegate onCancel=null) {
		Title = title;
		Body = body;
		AcceptText = acceptText;
		CancelText = cancelText;
		OnAccept = onAccept;
		OnCancel = onCancel;
	}

	public string AcceptText {
		get; private set;
	}

	public string CancelText {
		get; private set;
	}

	public string Title {
		get; private set;
	}

	public string Body {
		get; private set;
	}

	public OnGenericButtonClickedDelegate OnAccept {
		get; private set;
	}

	public OnGenericButtonClickedDelegate OnCancel {
		get; private set;
	}
}