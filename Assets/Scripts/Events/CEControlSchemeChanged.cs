﻿using UnityEngine;
using System.Collections;

public class CEControlSchemeChanged : ConvexEvent {

	public CEControlSchemeChanged(ControlScheme scheme) {
		Scheme = scheme;
	}

	public ControlScheme Scheme {
		get; private set;
	}
}
