using System.Collections;
using UnityEngine;

public class CESpaceshipSelected : ConvexEvent {

	public CESpaceshipSelected (Spaceship ship) {
		SelectedSpaceship = ship;
	}

	public Spaceship SelectedSpaceship {
		get; private set;
	}
}