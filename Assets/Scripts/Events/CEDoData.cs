using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DoDataType {
	Save,
	Load
}

public class CEDoData : ConvexEvent {

	public CEDoData(DoDataType type) {

		Type = type;

	}

	public DoDataType Type {
		get; private set;
	}
}