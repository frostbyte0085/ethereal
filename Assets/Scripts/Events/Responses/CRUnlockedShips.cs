using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CRUnlockedShips : ConvexResponse {

	public CRUnlockedShips(string[] names) {
		Names = names;
	}

	public string[] Names {
		get; private set;
	}

}