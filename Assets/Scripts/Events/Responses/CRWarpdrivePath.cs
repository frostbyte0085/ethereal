using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CRWarpdrivePath : ConvexResponse {

	public CRWarpdrivePath(List<Vector2> points) {
		Points = points;
	}

	public List<Vector2> Points {
		get; private set;
	}
}