using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CRChangedShip : ConvexResponse {

	public CRChangedShip(string menuShipResourceName, SpaceshipSpec spec) {
		MenuShipResourceName = menuShipResourceName;
		ShipSpec = spec;
	}

	public string MenuShipResourceName {
		get; private set;
	}

	public SpaceshipSpec ShipSpec {
		get; private set;
	}

}