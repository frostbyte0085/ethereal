using System;

public enum GamePhaseMode {
	Invalid,
	Menu,
	PreGameplay,
	Gameplay,
	Lose,
	Results
}

public class CEGamePhase : ConvexEvent {

	public CEGamePhase(GamePhaseMode mode) {
		Mode = mode;
	}

	public GamePhaseMode Mode {
		get; private set;
	}
}