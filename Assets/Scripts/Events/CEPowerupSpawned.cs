using UnityEngine;
using System.Collections;

public class CEPowerupSpawned : ConvexEvent {

	public CEPowerupSpawned(Transform trans, string spriteName) {
		Trans = trans;
		SpriteName = spriteName;
	}

	public string SpriteName {
		get; private set;
	}

	public  Transform Trans {
		get; private set;
	}
}