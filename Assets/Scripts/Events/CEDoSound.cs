﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum DoSoundAction {
	Play,
	Pause,
	Resume,
	Stop,
	SetVolume,
	StopAll,
	PauseAll,
	ResumeAll,
	PauseComponent,
	ResumeComponent,
	Mute,
	Unmute,
	None
}

public class CEDoSound : ConvexEvent {

	public CEDoSound(DoSoundAction action) {
		Action = action;
	}

	public CEDoSound (DoSoundAction action, string name, float param) {
		Action = action;
		Name = name;
		Param = param;
	}

	public CEDoSound (DoSoundAction action, string name) {
		Action = action;
		Name = name;
	}

	public DoSoundAction Action {
		get; private set;
	}

	public string Name {
		get; private set;
	}

	public float Param {
		get; private set;
	}

	public uint SoundInstanceID {
		get; set;
	}
}
