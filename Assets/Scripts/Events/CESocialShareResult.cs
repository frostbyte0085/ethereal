using System.Collections;
using UnityEngine;

public class CESocialShareResult : ConvexEvent {

	public CESocialShareResult(SocialResult result, SocialMedia media) {
		Result = result;
		Media = media;
	}

	public SocialResult Result {
		get; private set;
	}

	public SocialMedia Media {
		get; private set;
	}

}