﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CENeedsTutorial : ConvexEvent {

	public CENeedsTutorial(bool tutorial) {
		Tutorial = tutorial;
	}

	public bool Tutorial {
		get; private set;
	}
}
