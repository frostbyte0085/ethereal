using UnityEngine;
using System;

public class CESpaceshipInput : ConvexEvent {

	public CESpaceshipInput(InputData input) {
		Axis = input.axis;
		TargetPosition = input.targetPosition;
		IsTranslating = input.translating;
		IsTouching = input.touching;
	}

	public Vector3 TargetPosition {
		get; private set;
	}

	public bool IsTranslating {
		get; private set;
	}

	public bool IsTouching {
		get; private set;
	}

	public float Axis {
		get; private set;
	}

}