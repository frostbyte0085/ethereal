using System.Collections;
using UnityEngine;

public class CECrystalCollect : ConvexEvent {

	public CECrystalCollect(CrystalType type) {
		Type = type;
	}

	public CrystalType Type {
		get; private set;
	}

}