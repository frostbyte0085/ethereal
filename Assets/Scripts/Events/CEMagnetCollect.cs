using System.Collections;
using UnityEngine;

public enum MagnetPhase {
	Start,
	End
}

public class CEMagnetCollect : ConvexEvent {

	public CEMagnetCollect(MagnetPhase phase, float duration) {
		Phase = phase;
		Duration = duration;
	}

	public CEMagnetCollect(MagnetPhase phase) {
		Phase = phase;
	}

	public MagnetPhase Phase {
		get; private set;
	}

	public float Duration {
		get; private set;
	}
}