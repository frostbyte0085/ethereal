using System;

public class CEDifficultyUpdate : ConvexEvent {

	public CEDifficultyUpdate(float speed, float difficulty) {
		Speed = speed;
		Difficulty = difficulty;
	}

	public float Difficulty {
		get; private set;
	}

	public float Speed {
		get; private set;
	}
}