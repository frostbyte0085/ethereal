using System.Collections;
using UnityEngine;

public class CEWarpdriveInfo : ConvexEvent {

	public CEWarpdriveInfo(bool show, float duration=0f, bool force=false) {
		Show = show;
		Duration = duration;
		Force = force;
	}

	public bool Force {
		get; private set;
	}

	public float Duration {
		get; private set;
	}
	
	public bool Show {
		get; private set;
	}
}