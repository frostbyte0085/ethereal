﻿using UnityEngine;
using System.Collections;

public class CEReportScore : ConvexEvent {

	public CEReportScore (string leaderboardID, long score) {
		LeaderboardID = leaderboardID;
		Score = score;
	}

	public string LeaderboardID {
		get; private set;
	}

	public long Score {
		get; private set;
	}
}
