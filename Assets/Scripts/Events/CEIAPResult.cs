﻿using UnityEngine;
using System.Collections;


public enum IAPResult {
	Success,
	Failed,
	Cancelled,
	RestoredPurchases
}

public class CEIAPResult : ConvexEvent {

	public CEIAPResult(IAPResult result) {
		Result = result;
	}

	public IAPResult Result {
		get; private set;
	}
}
