﻿using UnityEngine;
using System.Collections;

public class CEReportAchievement : ConvexEvent {

	public CEReportAchievement (string achievementID, float progress) {
		AchievementID = achievementID;
		Progress = progress;
	}

	public string AchievementID {
		get; private set;
	}

	public float Progress {
		get; private set;
	}
}
