using System.Collections;
using UnityEngine;

public enum FadeMode {
	Invalid,
	Out,
	In
}

public delegate void OnFadeFinishedDelegate(FadeMode mode);

public class CEFade : ConvexEvent {

	public CEFade(FadeMode mode, float duration, OnFadeFinishedDelegate fadeInFinished=null, OnFadeFinishedDelegate fadeOutFinished=null) {
		Mode = mode;
		Duration = duration;
		OnFadeInFinished = fadeInFinished;
		OnFadeOutFinished = fadeOutFinished;
	}

	public FadeMode Mode {
		get; private set;
	}

	public float Duration {
		get; private set;
	}

	public OnFadeFinishedDelegate OnFadeInFinished {
		get; private set;
	}

	public OnFadeFinishedDelegate OnFadeOutFinished {
		get; private set;
	}
}