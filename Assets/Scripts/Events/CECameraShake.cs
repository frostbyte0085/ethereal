using System.Collections;

public class CECameraShake : ConvexEvent {

	public CECameraShake (float magnitude, float duration) {
		Magnitude = magnitude;
		Duration = duration;
	}

	public CECameraShake (float magnitude, float rotation, float duration) {
		Magnitude = magnitude;
		Rotation = rotation;
		Duration = duration;
	}

	public float Rotation {
		get; private set;
	}

	public float Duration {
		get; private set;
	}

	public float Magnitude {
		get; private set;
	}
}