using System.Collections;
using UnityEngine;

public class CESpaceshipStatus : ConvexEvent {

	public CESpaceshipStatus(Transform shipTrans, Vector2 velocity, float health, Bounds bounds, bool isInvincible) {
		ShipTransform = shipTrans;
		Velocity = velocity;
		Health = health;
		Size = new Vector2(bounds.size.x, bounds.size.y);
		IsInvincible = isInvincible;
	}

	public Transform ShipTransform {
		get; private set;
	}

	public Vector2 Velocity{
		get; private set;
	}

	public float Health {
		get; private set;
	}

	public bool IsAlive {
		get {
			return Health > 0f;
		}
	}

	public bool IsInvincible {
		get; private set;
	}

	public Vector2 Size {
		get; private set;
	}
}