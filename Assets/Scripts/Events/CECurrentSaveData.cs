using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CECurrentSaveData : ConvexEvent {

	public CECurrentSaveData(ShipSaveData[] shipData, bool tutorial, bool socialTutorial, int runs, bool firstRun,
	                         bool music, bool sfx, bool menuSfx, int controlScheme) {
		ShipData = shipData;
		NeedsTutorial = tutorial;
		NeedsSocialTutorial = socialTutorial;
		NumRuns = runs;
		IsFirstRun = firstRun;

		MusicEnabled = music;
		SfxEnabled = sfx;
		MenuSfxEnabled = menuSfx;
		ControlScheme = controlScheme;
	}

	public ShipSaveData[] ShipData {
		get; private set;
	}

	public int NumRuns {
		get; private set;
	}

	public bool IsFirstRun {
		get; private set;
	}

	public bool MusicEnabled {
		get; private set;
	}

	public bool SfxEnabled {
		get; private set;
	}

	public bool MenuSfxEnabled {
		get; private set;
	}

	public int ControlScheme {
		get; private set;
	}

	public float GetStatBonus (SpaceshipType type, StatType stat) {
		return (float)ShipData[(int)type].statBonus[(int)stat] / 10f;
	}

	public bool HasMoreUpgrades (SpaceshipType type, StatType stat) {
		int bonus = ShipData[(int)type].statBonus[(int)stat];

		return bonus < 3;
	}

	public int NextUpgradeIndex (SpaceshipType type, StatType stat) {
		return ShipData[(int)type].statBonus[(int)stat];
	}

	public bool NeedsSocialTutorial {
		get; private set;
	}

	public bool NeedsTutorial {
		get; private set;
	}
}