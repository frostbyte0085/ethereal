﻿using UnityEngine;
using System.Collections;

public enum InputMode {
	Disabled,
	Tutorial,
	Free
}

public class CEInputMode : ConvexEvent {

	public CEInputMode(InputMode mode) {
		Mode = mode;
	}

	public InputMode Mode {
		get; private set;
	}
}
