﻿using UnityEngine;
using System.Collections;

public static class TagStringManager {

	public static string Spaceship {
		get {
			return "Spaceship";
		}
	}

	public static string Tile {
		get {
			return "Tile";
		}
	}

	public static string Magnet {
		get {
			return "Magnet";
		}
	}

	public static string Warpdrive {
		get {
			return "Warpdrive";
		}
	}

	public static string ParticlesCamera {
		get {
			return "ParticlesCamera";
		}
	}

	public static string LaserCamera {
		get {
			return "LaserCamera";
		}
	}

	public static string WarpdrivePoint {
		get {
			return "WarpdrivePoint";
		}
	}

	public static string AsteroidField {
		get {
			return "AsteroidField";
		}
	}

	public static string MainCamera {
		get {
			return "MainCamera";
		}
	}
}
