﻿using UnityEngine;
using System.Collections;

public static class ResourceStringManager {

	private static Object[] laserObjects;
	static ResourceStringManager() {
		laserObjects = Resources.LoadAll ("Lasers");
	}

	public static string ShipsPath {
		get {
			return "Ships";
		}
	}

	public static string MenuShipsPath {
		get {
			return "MenuShips";
		}
	}

	public static string TilesPath {
		get {
			return "Tiles";
		}
	}

	public static string ScreensPath {
		get {
			return "UI/Screens";
		}
	}

	public static string PopupsPath {
		get {
			return "UI/Popups";
		}
	}

	public static string PathForParticles {
		get {
			return "Particles";
		}
	}

	public static string GetRandomPowerup() {
		string[] names = {"magnet", "repair", "warp_drive"};

		return "Collectables/" + names[Random.Range(0, names.Length)];
	}

	public static string PathForCrystalGroups {
		get {
			return "CrystalGroups";
		}
	}

	public static Object GetRandomLaser(float difficulty) {

		return laserObjects[0];

		// TODO: make it less linear with difficulty
		float total = 0f;
		float[] weights=new float[laserObjects.Length];
		for (int i=0; i<weights.Length;i++) {
			float w0 = (float)i / (float)(weights.Length-1);
			float w1 = 1 - w0;

			weights[i] = Mathf.Lerp (w1, w0, Mathf.Clamp01(difficulty));
			weights[i] = Mathf.Pow (weights[i], weights.Length);

			total += weights[i];
		}

		float x = Random.Range(0f, total);
		total = 0f;

		for (int i=0;i<laserObjects.Length; i++) {
			total += weights[i];

			if (x <= total) {
				return laserObjects[i];
			}
		}

		return null;
	}

}
