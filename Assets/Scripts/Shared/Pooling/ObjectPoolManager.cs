using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// this is a generic pooling manager.
// ObjectPool instances are registered here and the user can 'spawn' them 
// without using instantiate / destroy
public sealed class ObjectPoolManager {
	
	#region singleton
	private static ObjectPoolManager instance;
	
	public static ObjectPoolManager Instance {
		get {
			if (instance == null) {
				instance = new ObjectPoolManager();
			}
			
			return instance;
		}
	}
	
	#endregion
	
	#region Pool
	private Dictionary<string, ObjectPool> pools;
	
	private ObjectPoolManager() {
		pools = new Dictionary<string, ObjectPool>();
	}
	
	public void RegisterObjectPool (ObjectPool pool) {
		if (pools.ContainsKey (pool.name)) {
			Debug.LogError ("ObjectPool with name " + pool.name + " already exists!");
			Debug.Break();
		}
		
		pools[pool.name] = pool;
	}
	
	public void UnregisterObjectPool (ObjectPool pool) {
		if (!pools.ContainsKey (pool.name)) {
			Debug.LogError ("ObjectPool with name " + pool.name + " does not exist!");
			Debug.Break();
		}
		
		pools.Remove (pool.name);
	}
	
	public PooledInstance Spawn(string poolName, string objectName) {
		if (!pools.ContainsKey (poolName)) {
			Debug.LogWarning ("ObjectPool with name " + poolName + " does not exist!");
			return null;
		}
	
		return pools[poolName].Spawn (objectName);
	}
	
	public void Recycle (PooledInstance instance) {
		
		if (instance == null) {
			Debug.LogWarning ("Trying to recycle a null instance!");
			return;
		}
		
		// find which pool this instance belongs to
		string poolName = instance.PoolName;
		if (!pools.ContainsKey (poolName)) {
			Debug.LogError ("ObjectPool with name " + poolName + " does not exist!");
			Debug.Break();
		}
		else {
			pools[poolName].Recycle (instance);
		}
	}
	
	// the pool update is a low frequency update method, run it every 15 frames to clear excess instances
	public void Update() {
		if ( (pools.Count == 0) || (Time.frameCount % 15 != 0) )
			return;
		
		foreach (KeyValuePair<string, ObjectPool> kvp in pools) {
			if (kvp.Value != null) {
				kvp.Value.FreeExcessInstances();	
			}
		}
		
	}
	
	#endregion
}
