using UnityEngine;
using System.Collections;

public class AutoRecycleParticles : MonoBehaviour {
	
	private ParticleSystem particles;
	private PooledInstance currentInstance;
	
	private ObjectPoolManager poolMgr;
	
	// Use this for initialization
	void Start () {
		particles = GetComponent<ParticleSystem>();
		
		poolMgr = ObjectPoolManager.Instance;
	}
	
	// Update is called once per frame
	void Update () {

		if (currentInstance != null) {
			if (!particles.IsAlive(true)) {
				poolMgr.Recycle (currentInstance);
			}
		}
	}
	
	void OnSpawn(PooledInstance instance) {
		currentInstance = instance;
	}
	
	void OnRecycle() {
		currentInstance = null;
	}
}
