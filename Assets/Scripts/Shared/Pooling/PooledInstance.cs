using UnityEngine;
using System.Collections;

public sealed class PooledInstance {
	
	private float timestamp;
	
	#region cached components
	private Transform transform;
	private Rigidbody rigidbody;
	private Light light;
	private Collider collider;
	private Collider2D collider2d;
	private Camera camera;
	private ParticleSystem particles;
	
	private GameObject gameObject;
	#endregion
	
	private string poolName;
	private string objectName;
	
	public PooledInstance (GameObject instantiatedObject, string poolName, string objectName) {
		if (instantiatedObject == null) {
			Debug.LogError ("PooledInstance's instantiated object is null!");
			Debug.Break();
		}
		
		gameObject = instantiatedObject;
		
		transform = instantiatedObject.transform;
		rigidbody = instantiatedObject.GetComponent<Rigidbody>();
		light = instantiatedObject.GetComponent<Light>();
		collider = instantiatedObject.GetComponent<Collider>();
		collider2d = instantiatedObject.GetComponent<Collider2D>();
		camera = instantiatedObject.GetComponent<Camera>();
		particles = instantiatedObject.GetComponent<ParticleSystem>();
		
		this.poolName = poolName;
		this.objectName = objectName;
	}
	
	public void OnSpawn() {
		timestamp = Time.timeSinceLevelLoad;	
	}
	
	public void OnRecycle() {
		timestamp = 0f;	
	}
	
	public float Timestamp {
		get {
			return timestamp;
		}
	}
	
	public string PoolName {
		get {
			return poolName;
		}
	}
	
	public string ObjectName {
		get {
			return objectName;
		}
	}
	
	public GameObject CachedGameObject {
		get {
			return gameObject;
		}
	}
	
	public Camera CachedCamera {
		get {
			return camera;
		}
	}
	
	public Transform CachedTransform {
		get {
			return transform;
		}
	}
	
	public Rigidbody CachedRigidBody {
		get {
			return rigidbody;
		}
	}
	
	public Light CachedLight {
		get {
			return light;
		}
	}
	
	public Collider CachedCollider {
		get {
			return collider;
		}
	}

	public Collider2D CachedCollider2D {
		get {
			return collider2d;
		}
	}
	
	public ParticleSystem CachedParticleSystem {
		get {
			return particles;
		}
	}
}
