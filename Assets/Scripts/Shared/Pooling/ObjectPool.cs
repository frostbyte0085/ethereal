using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PoolStealing {
	None,
	Oldest
}

[System.Serializable]
public class PooledObject {
	
	// the pooled object name, used to spawn it
	public string name;
	// our prefab
	public Object prefab;
	// particle system name
	public bool useParticleSystemQuality;
	// how many instances to instantiate at start
	public int preloadInstances = 2;
	// how many instances to keep, after passing the preload instance limit.
	// if this limit is reached, perform the stealing behaviour
	public int maxInstances = 5;
	// perform stealing mode:
	// None: no stealing, simply cannot instantiate more elements
	// Oldest: find and recycle the oldest instance, and return it again
	public PoolStealing stealing = PoolStealing.None;
	
	[HideInInspector]
	public int currentInstanceCount=0;
}

public class ObjectPool : MonoBehaviour {
	
	public PooledObject[] objects;
	
	private ObjectPoolManager poolMgr;
	private Dictionary<string, List<PooledInstance>> pooledInstances;
	private Dictionary<string, PooledObject> pooledObjects;

	private List<PooledInstance> activeInstances;
	
	// just an easy way to get all the instances, used to destroy them.
	private List<PooledInstance> allInstances;
	
	void Awake () {
		
		poolMgr = ObjectPoolManager.Instance;
		pooledObjects = new Dictionary<string, PooledObject>();
		pooledInstances = new Dictionary<string, List<PooledInstance>>();
		activeInstances = new List<PooledInstance>();
		allInstances = new List<PooledInstance>();
		
		for (int i=0; i<objects.Length; i++) {
			pooledObjects[objects[i].name] = objects[i];
		}
		
		poolMgr.RegisterObjectPool (this);
	}
	
	void Start() {
		
		// preload here
		for (int i=0; i<objects.Length; i++) {
			PooledObject current = objects[i];
			if (current.prefab == null)
				continue;
			
			List<PooledInstance> currentPooledList = new List<PooledInstance>();
			
			int spawnCount = current.preloadInstances;
			Object prefab = current.prefab;
			for (int spawn=0; spawn<spawnCount; spawn++) {
				
				currentPooledList.Add ( newInstance (current) );
			}
			
			pooledObjects[current.name].currentInstanceCount = currentPooledList.Count;
			pooledInstances[current.name] = currentPooledList;
		}
	}
	
	private PooledInstance newInstance (PooledObject obj) {
		GameObject go = null;
		
		// the object is treated as a particle system
		if (obj.useParticleSystemQuality) {
			go = ParticleSystemManager.Instantiate (obj.prefab.name);
		}
		else {
			go = Instantiate (obj.prefab) as GameObject;
		}
		
		
		go.hideFlags = HideFlags.HideInHierarchy;
		go.SetActive (false);
		
		PooledInstance inst = new PooledInstance(go, name, obj.name);
		allInstances.Add (inst);
		return inst;
	}
	
	void OnDestroy() {
		poolMgr.UnregisterObjectPool (this);
		
		// destroy all the pooled objects
		for (int i=0; i<allInstances.Count; i++) {
			if (allInstances[i].CachedGameObject != null) {
				DestroyImmediate (allInstances[i].CachedGameObject);
			}
		}
		
		allInstances.Clear();
		pooledInstances.Clear();
		activeInstances.Clear();
	}
	
	// returns a pooled instance of the specified prefab name.
	// please not that this *can* return null, if the prefab cannot be spawned.
	public PooledInstance Spawn (string objectName) {
		PooledInstance instance = null;
		
		PooledObject objDesc = pooledObjects[objectName];
		List<PooledInstance> freeInstances = pooledInstances[objectName];
				
		// if there is an available free instance, remove it and add it to the active ones.
		if (freeInstances.Count > 0) {
			
			PooledInstance firstInstance = freeInstances[0];
			activeInstances.Add (firstInstance);
			freeInstances.RemoveAt (0);
						
			instance = firstInstance;
		}
		else {
			// no available instances. can we instantiate a new one? (is count+1 < maxInstances ?)
			int targetInstanceCount = objDesc.currentInstanceCount + 1;
			int maxCount = objDesc.maxInstances;
			
			
			if (targetInstanceCount < maxCount) {
				// we can, add a new one and increase the current instance count by one.
				instance = newInstance (objDesc);
				freeInstances.Add (instance);
				objDesc.currentInstanceCount++;
			}
			else {
				// there is no more room, let's free an old one if we are allowed to
				if (objDesc.stealing == PoolStealing.Oldest) {
					
					// go through all of them and check for the oldest one
					float oldest = Mathf.Infinity;
					int oldestIndex = -1;
					// make sure we find an instance with the same ObjectName
					for (int i=0; i<activeInstances.Count; i++) {
						if (activeInstances[i].Timestamp < oldest && activeInstances[i].ObjectName == objectName) {
							oldest = activeInstances[i].Timestamp;
							oldestIndex = i;
						}
					}
					
					// we found one, let's recycle it and try to spawn it again.
					if (oldestIndex != -1) {
						Recycle (activeInstances[oldestIndex]);
						return Spawn (objectName);
					}
				}
			}
		}
		
		if (instance != null) {
			instance.CachedGameObject.hideFlags = 0;
			instance.CachedGameObject.SetActive (true);
			instance.CachedGameObject.SendMessage ("OnSpawn", instance, SendMessageOptions.DontRequireReceiver);
			instance.OnSpawn();
		}
		
		return instance;
	}
	
	public void Recycle (PooledInstance instance) {
		
		instance.CachedGameObject.SendMessage ("OnRecycle", SendMessageOptions.DontRequireReceiver);
		instance.OnRecycle();
		
		instance.CachedGameObject.SetActive (false);
		instance.CachedGameObject.hideFlags = HideFlags.HideInHierarchy;
		
		// add it back where it was
		pooledInstances[instance.ObjectName].Add (instance);
		// and remove it from the active ones
		activeInstances.Remove (instance);
	}
	
	// here we free free instances that are not required anymore
	public void FreeExcessInstances() {
		//
		foreach (KeyValuePair<string, List<PooledInstance>> kvp in pooledInstances) {
			List<PooledInstance> freeInstances = kvp.Value;
			PooledObject objDesc = pooledObjects[kvp.Key];
			
			// if the current instance count of this prefab has reached the maximum amount, and also we have the same amount of free instances
			// (which means that the pool is not using up any instances at the moment),
			// remove one at a time.
			if ( (freeInstances.Count == objDesc.currentInstanceCount) && (objDesc.currentInstanceCount > objDesc.preloadInstances) ) {
				Destroy (freeInstances[0].CachedGameObject);
				allInstances.Remove (freeInstances[0]);
				freeInstances.RemoveAt(0);
				objDesc.currentInstanceCount--;
			}
		}
	}
}
