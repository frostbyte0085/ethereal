using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ParticleSystemManager {
	
	public static GameObject Instantiate(string prefabName) {
		Quality currentQuality = QualityControlManager.CurrentSetting.maxParticleQuality;
		
		string path = ResourceStringManager.PathForParticles + "/" + currentQuality + "/" + prefabName;
		GameObject go = (GameObject)GameObject.Instantiate (Resources.Load (path));
		return go;
	}
}
