using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;
using ICSharpCode.SharpZipLib.BZip2;

public static class StringExtensions {

	public static string Compress(this string text) {
		
		MemoryStream m_msBZip2 = null;
		BZip2OutputStream m_osBZip2 = null;
		string result;
		m_msBZip2 = new MemoryStream();
		System.Int32 size = text.Length;
		// Prepend the compressed data with the length of the uncompressed data (firs 4 bytes)
		//
		using (BinaryWriter writer = new BinaryWriter(m_msBZip2, System.Text.Encoding.ASCII))
		{
			writer.Write( size );
			 
			m_osBZip2 = new BZip2OutputStream(m_msBZip2);
			m_osBZip2.Write(Encoding.ASCII.GetBytes(text), 0, text.Length);
			 
			m_osBZip2.Close();
			result = System.Convert.ToBase64String(m_msBZip2.ToArray());
			m_msBZip2.Close();
			
			writer.Close();
			m_msBZip2.Dispose();
		}
		
		return result;
	}
	
	public static string Decompress(this string text) {
		
		string result;
		MemoryStream m_msBZip2 = null;
		BZip2InputStream m_isBZip2 = null;

		m_msBZip2 = new MemoryStream(System.Convert.FromBase64String(text));
		// read final uncompressed string size stored in first 4 bytes
		//
		using (BinaryReader reader = new BinaryReader(m_msBZip2, System.Text.Encoding.ASCII))
		{
			System.Int32 size = reader.ReadInt32();
			 
			m_isBZip2 = new BZip2InputStream(m_msBZip2);
			byte[] bytesUncompressed = new byte[size];
			m_isBZip2.Read(bytesUncompressed, 0, bytesUncompressed.Length);
			m_isBZip2.Close();
			m_msBZip2.Close();
			 
			result = Encoding.ASCII.GetString(bytesUncompressed);
			
			reader.Close();
			m_msBZip2.Dispose();
		}
		
		return result;
	}
	
	public static string Encrypt(this string text)
    {
		AES aes = new AES();
		return aes.Encrypt (text);
    }

    public static string Decrypt(this string text)
    {
        AES aes = new AES();
		return aes.Decrypt(text);
	}	
	
	public static Stream ToStream(this string text) {
		MemoryStream stream = new MemoryStream();
	    StreamWriter writer = new StreamWriter(stream);
	    writer.Write(text);
	    writer.Flush();
	    stream.Position = 0;
	    return stream;	
	}
}