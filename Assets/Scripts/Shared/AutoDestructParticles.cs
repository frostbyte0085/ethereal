﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class AutoDestructParticles : MonoBehaviour {

	private ParticleSystem ps;

	// Use this for initialization
	void Awake () {
		ps = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!ps.IsAlive(true)) {
			Destroy (gameObject);
		}
	}
}
