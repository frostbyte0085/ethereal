﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Utilities {

	public static List<Transform> FindTransformChildrenWithTag (Transform t, string tag) {
		
		List<Transform> total = new List<Transform>();
		findChildrenRecursive (t, tag, ref total);
		
		return total;
	}
	
	private static void findChildrenRecursive(Transform t, string tag, ref List<Transform> previous) {
		foreach (Transform child in t) {
			
			if (child.gameObject.tag == tag) {
				previous.Add (child);
			}
			findChildrenRecursive (child, tag, ref previous);
		}
	}

	public static void RemovePostfix (ref string str, string postfix) {

		int idx = str.LastIndexOf (postfix);
		if (idx >= 0) {
			str = str.Substring (0, idx);
		}
	}

}
