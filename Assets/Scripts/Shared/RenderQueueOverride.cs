using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum BaseRenderQueue {
	Invalid,
	Background = 1000,
	Geometry = 2000,
	AlphaTest = 2450,
	Transparent = 3000,
	Overlay = 4000
}

public enum RenderQueueOverrideMode {
	ApplyAll,
	ApplySelective
}

[System.Serializable]
public class RenderQueueOverrideSelection {
	public string materialName;
	public BaseRenderQueue baseQueue;
	public int queueOffset;
	
	[HideInInspector]
	public int prevQueue=-1;
}

[ExecuteInEditMode]
/**
 * \brief Use this to override the render queue of a renderer (no particles!). Can be applied to children.
 */
public class RenderQueueOverride : MonoBehaviour {
	
	public RenderQueueOverrideMode mode = RenderQueueOverrideMode.ApplyAll;
	public bool force = false;
	public BaseRenderQueue baseQueue;
	public int queueOffset;
	public bool applyToChildren = false;
	
	public RenderQueueOverrideSelection[] selectiveQueues;
	
	private List<Material> materials;
	
	private int prevQueue=-1;
	
	// Use this for initialization
	void Start () {
		
		materials = new List<Material>();
		
		if (GetComponent<Renderer>() != null) {
			if (!Application.isPlaying)
				materials.AddRange (GetComponent<Renderer>().sharedMaterials);
			else
				materials.AddRange (GetComponent<Renderer>().materials);
		}
		
		if (applyToChildren) {
			Renderer[] renderers = GetComponentsInChildren<Renderer>(true);
			if (renderers != null) {
				for (int i=0; i<renderers.Length; i++) {
					if (!Application.isPlaying)
						materials.AddRange (renderers[i].sharedMaterials);
					else
						materials.AddRange (renderers[i].materials);
				}
			}
		}
	}
	
	void LateUpdate() {
		if (mode == RenderQueueOverrideMode.ApplyAll)
			applyAll();
		else
			applySelective();
	}
	
	private void applyAll() {
		int desiredQueue = (int)baseQueue + queueOffset;
		
		if (desiredQueue != prevQueue || force) {
		
			for (int i=0; i<materials.Count; i++) {
				if (materials[i] != null) {
					materials[i].renderQueue = desiredQueue;
				}
			}
			
			prevQueue = desiredQueue;
		}
	}
	
	private void applySelective() {
		
		for (int i=0; i<selectiveQueues.Length; i++) {
			RenderQueueOverrideSelection selection = selectiveQueues[i];
			int desiredQueue = (int)selection.baseQueue + selection.queueOffset;
			
			if (desiredQueue != selection.prevQueue || force) {
				for (int matID=0; matID<materials.Count; matID++) {
					if (materials[matID] != null) {
						// check the name, if it corresponds to the one in the selective queue
						if (materials[matID].name.StartsWith (selection.materialName)) {
							materials[matID].renderQueue = desiredQueue;
						}
					}
				}
				
				selection.prevQueue = desiredQueue;
			}
		}
		
	}
	
	void OnDestroy() {
		if (!Application.isPlaying)
			return;
		
		if (materials != null) {
			for (int i=0; i<materials.Count; i++) {
				if (materials[i] != null)
					DestroyImmediate (materials[i]);
				materials[i] = null;
			}
		}
	}
}
