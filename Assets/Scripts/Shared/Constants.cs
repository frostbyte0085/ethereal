using System.Collections;
using UnityEngine;

public static class Constants {

	public static string fbPageApp = "fb://profile/321637088020495";
	public static string fbPageURL = "http://www.facebook.com/321637088020495";

	public static string twApp = "twitter://user?screen_name=ConvexGames";
	public static string twURL = "https://twitter.com/ConvexGames";

	public static string iosURL = "https://itunes.apple.com/app/ethereal/id884502090?ls=1&mt=8";
	public static string androidURL = "";

	public readonly static float tileScreenSize = 20.48f;
	public readonly static float safeMargin = 2f;

	public readonly static uint normalCrystal = 1;
	public readonly static uint bigCrystal = 10;

	public readonly static float healthWarningLevel = 0.25f;

	// default data constants
	public readonly static uint defaultRepairKits = 5; // this corresponds to 2 revives. 1 first, 4 second = 5
	public readonly static uint defaultCrystals = 0;

	#if UNITY_ANDROID
	public readonly static string leaderboardID = "CgkIs9eviOMUEAIQAQ";
	#elif UNITY_IPHONE
	public readonly static string leaderboardID = "total_score";
	#endif

}