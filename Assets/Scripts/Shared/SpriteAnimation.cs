﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// this is not entirely needed, as Unity will cache the calls to Resources.LoadAll.
// however it will be faster to check like this before loading the sprites
// this should be cleared at game launch.
public static class SpriteAnimationCache {

	private static Dictionary<string, Sprite[]> spritesForPath;

	static SpriteAnimationCache() {
		spritesForPath = new Dictionary<string, Sprite[]>();
	}

	public static Sprite[] GetSprites (string path) {
		if (spritesForPath.ContainsKey (path)) {
			return spritesForPath[path];
		}

		// we don't have this path stored, let's load the resources and return them
		Sprite[] sprites = Resources.LoadAll<Sprite> (path);
		if (sprites != null) {
			// rebuild the sprite array
			List<Sprite> validSprites = new List<Sprite>();
			foreach (Sprite s in sprites) {
				if (s.packed) continue;

				validSprites.Add (s);
			}

			sprites = validSprites.ToArray();
			spritesForPath[path] = sprites;
		}

		return sprites;
	}

	public static void Clear() {
		spritesForPath.Clear();
	}
}

public enum SpriteAnimationTarget {
	DFGUI_Texture,
	Unity_Sprite
}

public class SpriteAnimation : MonoBehaviour {

	public string spriteName = "";
	public SpriteAnimationTarget target =  SpriteAnimationTarget.Unity_Sprite;
	public bool startRandomized = true;

	private SpriteRenderer sRenderer;
	private dfTextureSprite dfTexture;
	private Sprite[] sprites;
	private int currentIndex;

	// Use this for initialization
	void Awake () {
		sprites = SpriteAnimationCache.GetSprites (spriteName);
		sRenderer = GetComponent<SpriteRenderer>();
		dfTexture = GetComponent<dfTextureSprite>();
	}

	void OnEnable() {
		// randomize the currentIndex
		if (startRandomized) {
			currentIndex = Random.Range (0, sprites.Length);
			sRenderer.sprite = sprites[currentIndex];
		}
	}
	
	// Update is called once per frame
	void Update () {

		if (Time.frameCount % 3 == 0) {
			currentIndex = (++currentIndex) % sprites.Length;

			if (target == SpriteAnimationTarget.Unity_Sprite){
				sRenderer.sprite = sprites[currentIndex];
			}
			else {
				dfTexture.Texture = sprites[currentIndex].texture;
			}
		}
	}

	void OnDestroy() {
		sprites = null;
		dfTexture = null;
		sRenderer = null;
	}
}
