﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// a pooled sound instance
public class SoundInstance: MonoBehaviour {

	private bool isPaused;
	private uint instanceID;

	void Start() {
		
	}

	public float TimeStart {
		get; private set;
	}

	public void OnSpawn() {
		enabled = true;
		isPaused = false;
	}
	
	public void OnRecycle() {
		enabled = false;
		isPaused = false;
	}

	public uint InstanceID {
		get {
			return instanceID;
		}
	}

	public void Play(uint instanceID, float delay=0f) {
		this.instanceID = instanceID;
		isPaused = false;
		if (delay > 0f) {
			GetComponent<AudioSource>().PlayDelayed (delay);
		}
		else {
			GetComponent<AudioSource>().Play();
		}

		TimeStart = Time.realtimeSinceStartup;
	}

	public void Stop() {
		isPaused = false;
		GetComponent<AudioSource>().Stop();
	}

	public void Pause() {
		isPaused = true;
		GetComponent<AudioSource>().Pause();
	}

	public bool IsPaused {
		get{
			return isPaused;
		}
	}

	public bool IsPlaying {
		get {
			return GetComponent<AudioSource>().isPlaying;
		}
	}
}