﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomSound : Sound {

	public AudioClip[] clips;

	public override void Play (float delay, uint instanceID)
	{
		if (clips != null && clips.Length > 0) {
			clip = clips[Random.Range(0, clips.Length)];
		}
		base.Play (delay, instanceID);
	}
}
