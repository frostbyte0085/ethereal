﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundComponent : MonoBehaviour {

	[Range(0f, 1f)]
	public float volume=1f;

	public bool mute=false;

	private SoundComponent parentComponent;

	void Start() {
		if (transform.parent != null) {
			parentComponent = transform.parent.GetComponent<SoundComponent>();
		}
	}

	public float Volume {
		get {
			if (parentComponent != null) {
				if (parentComponent.mute) {
					return 0;
				}

				return volume * parentComponent.Volume;

			}

			if (mute)
				return 0f;

			return volume;
		}
	}
}
