﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SoundStealing {
	Oldest,
	Newest,
	None
}

public class Sound : SoundComponent {

	public SoundStealing stealing = SoundStealing.Oldest;
	[Range(1, 100)]
	public int pooledInstances = 1;
	public AudioClip clip;	
	public bool isLooping=false;

	private List<SoundInstance> freeInstances;
	private List<SoundInstance> playingInstances;

	// Use this for initialization
	void Awake () {
		freeInstances = new List<SoundInstance>();
		playingInstances = new List<SoundInstance>();

		for (int i=0; i<pooledInstances; i++) {

			GameObject instance = new GameObject("pooled_instance_"+i);
			instance.transform.parent = transform;
			
			instance.AddComponent<AudioSource>();
			instance.GetComponent<AudioSource>().clip = clip;
			instance.GetComponent<AudioSource>().playOnAwake = false;
			instance.GetComponent<AudioSource>().loop = isLooping;
			
			SoundInstance sndInstance = instance.AddComponent<SoundInstance>();

			freeInstances.Add (sndInstance);
		}
	}

	void Update() {

		for (int i=playingInstances.Count-1; i>=0; i--) {
			SoundInstance instance = playingInstances[i];
			AudioSource source = instance.gameObject.GetComponent<AudioSource>();
			bool validRecycle = !(source.time > 0 && source.time < source.clip.length);

			if (!source.isPlaying && !instance.IsPaused && validRecycle) {

				playingInstances.RemoveAt (i);
				freeInstances.Add (instance);
				instance.OnRecycle();
			}
			else {
				source.volume = Volume;
			}
		}

	}

	public void PauseAll() {
		for (int i=0; i<playingInstances.Count; i++) {
			playingInstances[i].Pause();
		}
	}

	public void ResumeAll() {
		for (int i=0; i<playingInstances.Count; i++) {
			playingInstances[i].Play(playingInstances[i].InstanceID);
		}
	}

	public void StopAll() {
		for (int i=0; i<playingInstances.Count; i++) {
			if (playingInstances[i].IsPlaying) {

				playingInstances[i].Stop();
			}
		}
	}

	public virtual bool Stop(uint instanceID) {
		for (int i=0; i<playingInstances.Count; i++) {
			SoundInstance instance = playingInstances[i];
			if (instance != null) {
				if (instance.InstanceID == instanceID) {
					instance.Stop();
					return true;
				}
			}
		}

		return false;
	}

	// instance id -1 means that we're working on all the instances in this sound
	public virtual bool Pause(uint instanceID, bool ignoreID=false) {
		for (int i=0; i<playingInstances.Count; i++) {
			SoundInstance instance = playingInstances[i];
			if (instance != null) {
				if (instance.InstanceID == instanceID || ignoreID) {
					instance.Pause();
					return true;
				}
			}
		}
		return false;
	}

	public virtual bool Resume(uint instanceID, bool ignoreID=false) {
		for (int i=0; i<playingInstances.Count; i++) {
			SoundInstance instance = playingInstances[i];
			if (instance != null) {
				if (instance.InstanceID == instanceID || ignoreID) {
					instance.Play(instance.InstanceID);
					return true;
				}
			}
		}
		return false;
	}

	public virtual void Play(float delay, uint instanceID) {
		// try to get a sound instance
		if (freeInstances.Count > 0) {
			SoundInstance instance = freeInstances[0];
			if (instance != null) {

				freeInstances.Remove (instance);
				playingInstances.Add (instance);
				instance.OnSpawn();

				instance.GetComponent<AudioSource>().clip = clip;

				// update component
				instance.gameObject.GetComponent<AudioSource>().volume = Volume;
				//

				instance.Play(instanceID, delay);
			}
		}
		else {
			// stealing
			// go through the playing instances and steal one based on the stealing mode
			SoundInstance instance = null;

			for (int i=0; i<playingInstances.Count; i++) {
				SoundInstance current = playingInstances[i];

				if (stealing == SoundStealing.Oldest) {
					if (instance == null || (current.TimeStart < instance.TimeStart)) {
						instance = current;
					}
				}
				else if (stealing == SoundStealing.Newest) {
					if (instance == null || (current.TimeStart > instance.TimeStart)) {
						instance = current;
					}
				}
			}

			if (instance != null) {
				instance.Stop();
				instance.Play(instanceID, delay);
			}
		}
	}
}
