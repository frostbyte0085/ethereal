﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TileType {
	Normal,
	Adaptor,
	Reward
}

public enum TileFamily {
	AsteroidField,
	SpaceStation,
	Invalid
}

public enum TileDifficulty {
	Easy=0,
	Medium=1,
	Hard=2
}

public enum RewardType {
	None,
	CrystalGroup,
	Powerup
}

public enum FieldType {
	MineField,
	ZapField,
	SawbotField,
	LaserField,
	None
}

public class Tile : MessageBehaviour {

	public float adaptorEmptySpace = 20f;

	public TileDifficulty difficulty=TileDifficulty.Easy;
	public TileType type = TileType.Normal;
	public TileFamily from = TileFamily.Invalid;
	public TileFamily to = TileFamily.Invalid;
	public int pooledInstances=1;
	
	public FieldType field = FieldType.None;

	public bool isTutorial;
	public int minefieldCellSize = 4;
	public float minefieldJitter = 1.5f;

	public Transform adaptorStart;
	public Transform adaptorEnd;

	// serialised, exposed to custom inspector for building the tile
	[HideInInspector]
	public List<TileElement> elements;

	private List<Tile> freeTiles;
	private List<Tile> usedTiles;

	private float adaptorSize;
	private bool inWarpdrive;

	private TileElement[] usedElements;

	private SawbotRig[] sawbotsrigs;
	private ZapRig[] zaprigs;
	private Mine[] mines;

	private ulong tileOrdinal;

	// Use this for initialization
	void Awake () {

		DontDestroyOnLoad (gameObject);

		freeTiles = new List<Tile>();
		usedTiles = new List<Tile>();
		//

		usedElements = GetComponentsInChildren<TileElement>(true);
		//

		List<Transform> warpdrivePointsTrans = Utilities.FindTransformChildrenWithTag (transform, TagStringManager.WarpdrivePoint);
		if (warpdrivePointsTrans != null) {
			warpdrivePointsTrans.Reverse();

			WarpdrivePoints = new Vector2[warpdrivePointsTrans.Count];

			for (int i=0; i<warpdrivePointsTrans.Count; i++) {
				WarpdrivePoints[i] = warpdrivePointsTrans[i].localPosition;
			}
		}

		mines = GetComponentsInChildren<Mine>(true);
		sawbotsrigs = GetComponentsInChildren<SawbotRig>(true);
		zaprigs = GetComponentsInChildren<ZapRig>(true);

		registerEventListener (typeof(CEWarpdriveCollect), warpdriveEventListener);
	}

	private void warpdriveEventListener (ConvexEvent evt) {
		CEWarpdriveCollect e = (CEWarpdriveCollect)evt;

		if (e.Phase == WarpdrivePhase.Start) {
			inWarpdrive = true;
		}
		else if (e.Phase == WarpdrivePhase.End) {
			inWarpdrive = false;
		}
	}

	public Transform AdaptorStart {
		get {
			return adaptorStart;
		}
	}

	public Transform AdaptorEnd {
		get {
			return adaptorEnd;
		}
	}

	public float TileSize {
		get {
			float size = Constants.tileScreenSize * elements.Count;
			if (type == TileType.Adaptor) {
				//size += adaptorEmptySpace;
			}

			return size;
		}
	}

	public Vector2[] WarpdrivePoints {
		get; private set;
	}

	public void Precache() {
		for (int i=0; i<pooledInstances; i++) {
			freeTiles.Add (makeNewTile());
		}

	}

	private Tile makeNewTile() {

		GameObject go = (GameObject)Instantiate(gameObject);
		go.hideFlags = HideFlags.HideInHierarchy;

		Tile t = go.GetComponent<Tile>();
		t.ParentTile = this;

		go.SetActive (false);

		return t;
	}

	public Tile ParentTile {
		get; private set;
	}

	private static bool spawnedNormalCrystals;
	public Tile GetPooled(float mineProbability=0, float sawbotProbability=0, float zapProbability=0, RewardType reward = RewardType.None) {

		// if there are no free tiles, make a new one
		if (freeTiles.Count == 0) {
			freeTiles.Add (makeNewTile());
		}

		// get the first available tile
		Tile t = freeTiles[0];
		t.gameObject.hideFlags = HideFlags.None;
		t.gameObject.SetActive (true);

		usedTiles.Add (t);
		freeTiles.Remove (t);

		int idx=0;
		switch (t.field) {
		case FieldType.None:
			foreach (TileElement el in t.usedElements) {
				el.OnSpawn();
				
				if (reward == RewardType.CrystalGroup) { 
					el.SpawnCrystalGroup();
					spawnedNormalCrystals = true;
				}
				else if (reward == RewardType.Powerup) el.SpawnPowerup();
				else {

					if (!isTutorial) {
						// enable a crystal group based on a probability.
						// this is when we are in normal tiles and adaptors
						float r = Random.Range(0f, 1f);
						if (r <= 0.85f && !spawnedNormalCrystals) {
							el.SpawnCrystalGroup();
							spawnedNormalCrystals = true;
						}
						else {
							spawnedNormalCrystals = false;
						}
					}

				}
			}
			break;

		case FieldType.MineField:
			doFieldCheck (t.mines, mineProbability);
			break;

		case FieldType.LaserField:
			break;

		case FieldType.SawbotField:
			doFieldCheck (t.sawbotsrigs, sawbotProbability);
			break;

		case FieldType.ZapField:
			doFieldCheck (t.zaprigs, zapProbability);
			break;

		}

		/*
		if (tileOrdinal == 0 && t.type == TileType.Adaptor) {
			t.AdaptorEnd.localPosition -= Vector3.right * t.adaptorEmptySpace;
		}
		*/

		tileOrdinal++;

		return t;
	}

	private void doFieldCheck (MonoBehaviour[] items, float probability) {

		for (int i=0; i<items.Length; i++) {
			items[i].gameObject.SetActive (false);
		}

		// choose some sawbots to enable based on current difficulty
		bool atLeastOne = false;
		for (int i=0; i<items.Length; i++) {
			bool p = Random.Range (0f, 1f) < probability;
			items[i].gameObject.SetActive (p);
			
			if (p) {
				atLeastOne = true;
			}
		}
		
		if (!atLeastOne) {
			int idx = Random.Range(0, items.Length);
			items[idx].gameObject.SetActive (true);
		}
	}

	public void Recycle(Tile t) {

		freeTiles.Add (t);
		usedTiles.Remove (t);

		switch (t.field) {
		case FieldType.None:
			//enable the start tile if it's an adaptor
			// this may have been disabled if this adaptor was spawned after the start tile.
			if (t.type == TileType.Adaptor) {
				t.AdaptorStart.gameObject.SetActive (true);
			}
			
			break;
			
		case FieldType.MineField:
			break;
			
		case FieldType.LaserField:
			break;
			
		case FieldType.SawbotField:
			break;
			
		case FieldType.ZapField:
			break;
			
		}

		t.gameObject.SetActive (false);
		t.gameObject.hideFlags = HideFlags.HideInHierarchy;
	}
}
