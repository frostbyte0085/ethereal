﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
[RequireComponent(typeof(LineRenderer))]
public class Zap : MonoBehaviour {
	
	public int numPoints = 20;
	public float duration = 0.01f;
	public Vector2 noise = new Vector2(0.3f, 0.2f);
	
	private Transform startTrans;
	private Transform endTrans;
	
	private LineRenderer lineRenderer;
	
	private Transform cachedTrans;
	
	private Vector3[] points;
	private Vector3[] previousPoints;
	
	private float interpolation;

	private TrailRenderer[] trails;

	void Awake() {
		cachedTrans = transform;

		startTrans = cachedTrans.Find ("start");
		endTrans = cachedTrans.Find ("end");

		trails = GetComponentsInChildren<TrailRenderer>(true);
	}

	void OnBecameVisible() {
		if (trails != null) {
			foreach (TrailRenderer trail in trails) {
				trail.enabled = true;
			}
		}
	}
	
	void OnBecameInvisible() {
		if (trails != null) {
			foreach (TrailRenderer trail in trails) {
				trail.enabled = false;
			}
		}
	}
	
	void OnDisable() {
		if (trails != null) {
			foreach (TrailRenderer trail in trails) {
				trail.enabled = false;
			}
		}
	}

	// Use this for initialization
	void Start () {
		
		lineRenderer = GetComponent<LineRenderer>();
		lineRenderer.SetVertexCount (numPoints);
		
		points = new Vector3[numPoints+1];
		previousPoints = new Vector3[numPoints+1];
	}

	public LineRenderer Line {
		get {
			return lineRenderer;
		}
	}

	private void calculatePoints() {
		points.CopyTo (previousPoints, 0);
		
		points[0] = startTrans.localPosition;
		
		for (int i=1; i<numPoints; i++) {
			points[i] = Vector3.Lerp (startTrans.localPosition, endTrans.localPosition, (float)(i+1)/(float)numPoints);
			
			if (i < numPoints-1) {
				points[i].x += Random.Range(-noise.x, noise.x);
				points[i].y += Random.Range(-noise.y, noise.y);
			}
		}
		
		points[numPoints] = endTrans.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		if (!Application.isPlaying)
			return;
		
		if (interpolation < 1f) {
			interpolation += Time.deltaTime * 1f / duration;
			
			for (int i=0; i<numPoints; i++) {
				Vector3 targetPosition = Vector3.Lerp (previousPoints[i], points[i], interpolation);
				
				lineRenderer.SetPosition (i, targetPosition);
			}
			
			if (interpolation >= 1f) {
				interpolation = 0f;
				calculatePoints();
			}
		}
		
	}

	public Transform StartTrans {
		get {
			return startTrans;
		}
	}

	public Transform EndTrans {
		get {
			return endTrans;
		}
	}
	
	void OnDrawGizmos() {

		Gizmos.color = Color.blue;
		if (startTrans != null) {
			Gizmos.DrawSphere (startTrans.position, 0.25f);
		}
		
		if (endTrans != null) {
			Gizmos.DrawSphere (endTrans.position, 0.25f);
		}
	}
}
