﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Minefield : MonoBehaviour {

	private Mine[] mines;

	void Awake() {
		mines = GetComponentsInChildren<Mine>(true);
	}

	void OnEnable() {
		foreach (Mine m in mines) {
			m.GetComponent<Renderer>().enabled = true;
		}
	}


}
