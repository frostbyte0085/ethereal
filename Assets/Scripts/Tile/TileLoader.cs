﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// This is a one-off loading of all the tiles in the game.
// They must fit in memory to subsequent reduce loading times to none!
public static class TileLoader {

	private static bool isLoaded = false;
	
	public static Dictionary<TileFamily, Dictionary<TileFamily, Tile>> AdaptorTiles { get; private set; }
	public static List<Tile> MinefieldTiles {get; private set; }
	public static List<Tile> SawbotFieldTiles {get; private set; }
	public static List<Tile> ZapFieldTiles {get; private set; }
	public static List<Tile> LaserFieldTiles {get; private set; }

	public static Dictionary<TileFamily, List<Tile>> NormalTiles_Easy {get; private set; }
	public static Dictionary<TileFamily, List<Tile>> NormalTiles_Medium {get; private set; }
	public static Dictionary<TileFamily, List<Tile>> NormalTiles_Hard {get; private set; }

	public static List<Tile> RewardTiles {get; private set;}


	public static Dictionary<TileFamily, List<Tile>> GetTiles (TileDifficulty difficulty) {
		if (difficulty == TileDifficulty.Easy) return NormalTiles_Easy;
		else if (difficulty == TileDifficulty.Medium) return NormalTiles_Medium;
		else if (difficulty == TileDifficulty.Hard) return NormalTiles_Hard;

		return null;
	}

	public static Tile TutorialTile {
		get; private set;
	}

	private static void precacheCrystals() {

		Object[] groups = Resources.LoadAll(ResourceStringManager.PathForCrystalGroups);
		if (groups != null) {
			for (int i=0; i<groups.Length; i++) {
				// precache it
				GameObject go = (GameObject)GameObject.Instantiate (groups[i]);
				GameObject.Destroy (go);
			}
		}

		groups = null;
	}

	public static void EnumerateAndLoad() {
		if (isLoaded) {
			return;
		}
		precacheCrystals();

		RewardTiles = new List<Tile>();

		NormalTiles_Easy = new Dictionary<TileFamily, List<Tile>>();
		NormalTiles_Medium = new Dictionary<TileFamily, List<Tile>>();
		NormalTiles_Hard = new Dictionary<TileFamily, List<Tile>>();

		AdaptorTiles = new Dictionary<TileFamily, Dictionary<TileFamily, Tile>>();
		MinefieldTiles = new List<Tile>();
		SawbotFieldTiles = new List<Tile>();
		ZapFieldTiles = new List<Tile>();
		LaserFieldTiles = new List<Tile>();
		
		//Object[] allTiles = Resources.LoadAll ("Tiles");
		//Tile[] allTiles = 
		GameObject[] allTiles = GameObject.FindGameObjectsWithTag ("Tile");
		
		for (int i=0; i<allTiles.Length; i++) {

			Tile t = allTiles[i].GetComponent<Tile>();

			// otherwise, normal/adaptor tile
			if (t.field == FieldType.None) {
				switch (t.type) {
				case TileType.Normal:
					
					// if this tile is the tutorial one, don't add it to the dictionary of tiles,
					// simply set the TutorialTile property in this class.
					if (t.isTutorial) {
						if (TutorialTile != null) {
							Debug.LogError ("Tutorial tile has already been set!");
							Debug.Break();
						}
						
						TutorialTile = t;
					}
					else {
						TileFamily family = t.from;
						
						Dictionary<TileFamily, List<Tile>> tiles = GetTiles (t.difficulty);
						
						List<Tile> tilesForFamily = null;
						if (!tiles.TryGetValue (family, out tilesForFamily)) {
							tilesForFamily = new List<Tile>();
						}
						
						tilesForFamily.Add (t);
						
						tiles[family] = tilesForFamily;
					}
					
					break;
					
				case TileType.Reward:
					RewardTiles.Add (t);
					break;
					
				case TileType.Adaptor:
					Dictionary<TileFamily, Tile> fromTiles = null;
					if (!AdaptorTiles.TryGetValue (t.from, out fromTiles)) {
						fromTiles = new Dictionary<TileFamily, Tile>();
					}
					
					fromTiles[t.to] = t;
					AdaptorTiles[t.from] = fromTiles;
					
					break;
					
				default:break;
				}
			}
			// only spacestation tiles are allowed to be fields.
			else {
				if ( (t.from == TileFamily.SpaceStation) && (t.to == TileFamily.SpaceStation) ) {

					if (t.field == FieldType.MineField) {
						MinefieldTiles.Add (t);
					}
					else if (t.field == FieldType.LaserField) {
						LaserFieldTiles.Add (t);
					}
					else if (t.field == FieldType.SawbotField) {
						SawbotFieldTiles.Add (t);
					}
					else if (t.field == FieldType.ZapField) {
						ZapFieldTiles.Add (t);
					}
				}
			}

			t.Precache();
			t.gameObject.hideFlags = HideFlags.HideInHierarchy;
			t.gameObject.SetActive (false);

		}

		Resources.UnloadUnusedAssets();
		isLoaded = true;
	}
}
