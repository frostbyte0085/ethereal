﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZapRig : MonoBehaviour {

	public Vector2 rotationSpeed;
	public Vector2 wobbleMagnitude;
	public Vector2 wobbleSpeed;

	public Color glowFrom=new Color(0, 0.75f, 1f, 0.25f);
	public Color glowTo = new Color(0, 0.85f, 1f, 0.4f);

	private Zap[] zaps;
	private BoxCollider2D b2d;
	private SpriteRenderer glow;

	private Transform cachedTrans;

	private float targetRotationSpeed;
	private float targetWobbleSpeed;
	private float targetWobbleMagnitude;

	private float targetWobblePhase;

	void Awake() {
		cachedTrans = transform;

		GameObject b2dGO = new GameObject("collider");
		b2dGO.transform.parent = transform;
		b2dGO.AddComponent<ZapSensor>();
		b2d = b2dGO.AddComponent<BoxCollider2D>();
		b2d.isTrigger = true;

		GameObject glowGO = new GameObject("glow");
		glowGO.transform.parent = transform;
		glow = glowGO.AddComponent<SpriteRenderer>();
		glow.sprite = Resources.Load<Sprite> ("glow_zap");
		glow.sortingLayerID = 1;
		glow.gameObject.layer = LayerMask.NameToLayer ("Zap");

		zaps = GetComponentsInChildren<Zap>(true);

		gameObject.layer = glow.gameObject.layer = LayerMask.NameToLayer ("Zap");
		foreach (Zap z in zaps) {
			z.gameObject.layer = LayerMask.NameToLayer ("Zap");
		}
	}

	void OnEnable() {
		float sgn = (Random.Range(0, 2) == 0) ? -1f : 1f;

		targetRotationSpeed = Random.Range (rotationSpeed.x, rotationSpeed.y) * sgn;
		targetWobbleMagnitude = Random.Range (wobbleMagnitude.x, wobbleMagnitude.y);
		targetWobbleSpeed = Random.Range (wobbleSpeed.x, wobbleSpeed.y);

		targetWobblePhase = Random.Range(0f, 1f);
	}

	void Update() {
		if (zaps != null && zaps.Length > 0) {
			
			Vector3 avgStart = Vector3.zero;
			Vector3 avgEnd = Vector3.zero;

			avgStart = Vector3.zero;
			avgEnd = Vector3.zero;
			float avgNoise = 0f;

			for (int i=0; i<zaps.Length; i++) {
				avgNoise += zaps[i].noise.x*4f;
				avgStart += zaps[i].StartTrans.localPosition;
				avgEnd += zaps[i].EndTrans.localPosition;
			}

			avgNoise /= (float)zaps.Length;
			avgStart /= (float)zaps.Length;
			avgEnd /= (float)zaps.Length;

			b2d.transform.localPosition = (avgStart + avgEnd) * 0.5f;

			float orientation = 90f + Mathf.Rad2Deg * Mathf.Atan2 ( (avgEnd.y - avgStart.y), (avgEnd.x - avgStart.x) );
			Vector3 localEuler = b2d.transform.localEulerAngles;
			localEuler.z = orientation;
			b2d.transform.localEulerAngles = localEuler;

			Vector2 size = b2d.size;
			size.y = Vector3.Distance (avgStart, avgEnd);
			b2d.size = size;


			// rotate the thing!
			cachedTrans.Rotate (new Vector3(0,0,1), targetRotationSpeed * Time.deltaTime, Space.Self);


			float scaleMul = Mathf.Lerp(0.9f, 1.1f, Mathf.PingPong(Time.time*0.5f, 1f));

			glow.transform.localPosition = (avgStart + avgEnd) * 0.5f;
			glow.transform.localEulerAngles = localEuler;
			glow.transform.localScale = new Vector3(avgNoise, size.y, 1f) * scaleMul;

			glow.color = Color.Lerp (glowFrom, glowTo, Mathf.PingPong(Time.time*2f, 1f));


			Vector3 dirToCenter = (new Vector3(b2d.offset.x, b2d.offset.y,0) - avgStart).normalized;
			for (int i=0; i<zaps.Length; i++) {
				zaps[i].StartTrans.localPosition += dirToCenter * targetWobbleMagnitude * Mathf.Sin (Time.time * targetWobbleSpeed + targetWobblePhase);
				zaps[i].EndTrans.localPosition += dirToCenter * targetWobbleMagnitude * Mathf.Cos (Time.time * targetWobbleSpeed + targetWobblePhase);
			}
		}
	}

}
