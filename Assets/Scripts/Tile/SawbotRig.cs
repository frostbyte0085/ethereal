﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class SawbotRig : MonoBehaviour {

	private Transform startTrans;
	private Transform endTrans;
	private Sawbot sawbot;

	private Transform cachedTrans;

	private float offset;
	private float elapsed;
	private float speed;


	private TrailRenderer trails;

	void Awake() {
		trails = GetComponentInChildren<TrailRenderer>();
		trails.enabled = false;
	}

	// Use this for initialization
	void Start () {

		cachedTrans = transform;

		sawbot = GetComponentInChildren<Sawbot>();
		startTrans = cachedTrans.Find ("start");
		endTrans = cachedTrans.Find ("end");
	}

	void OnEnable() {
		// initialise it randomly
		elapsed = Random.Range(0, 5f);
		speed = Random.Range(1.5f, 3f);

		trails.enabled = true;
	}

	void OnDisable() {

		trails.enabled = false;
	}

	// Update is called once per frame
	void Update () {
		if (!Application.isPlaying)
			return;

		elapsed += Time.deltaTime * speed;

		// ping-pong between start and end transform positions
		float f = Mathf.Sin(elapsed) * 0.5f + 0.5f;
		//f = Mathf.SmoothStep(0,1,f);

		sawbot.transform.localPosition = Vector3.Lerp (startTrans.localPosition, endTrans.localPosition, f);
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.green;
		if (startTrans != null) {
			Gizmos.DrawSphere (startTrans.position, 0.25f);
		}

		if (endTrans != null) {
			Gizmos.DrawSphere (endTrans.position, 0.25f);
		}
	}
}
