﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Mine : Collectable {

	public float[] moveAngles; // a series of predefined movement directions

	private CESpaceshipStatus lastShipStatus;

	private void spaceshipStatusEventListener (ConvexEvent evt) {
		lastShipStatus = (CESpaceshipStatus)evt;
	}

	private bool inMagnet;
	private bool inWarpdrive;
	private bool canHit;
	private bool hasHit;

	private Vector3 startPosition;

	private float targetAngle;
	private float targetSpeed;

	private TrailRenderer trail;

	protected override void OnEnable ()
	{
		base.OnEnable ();

		hasHit = false;

		transform.localPosition = startPosition;

		if (moveAngles != null && moveAngles.Length > 0) {
			targetAngle = moveAngles[Random.Range(0, moveAngles.Length)];
		}

		targetSpeed = Random.Range(1f, 3f);

		destroyedByMagnet = false;
	}

	void OnDisable() {
		inWarpdrive = false;
		canHit = true;

		trail.enabled = false;
	}

	void OnBecameVisible() {
		trail.enabled = true;
	}

	void OnBecameInvisible() {
		trail.enabled = false;
	}

	protected override void Awake ()
	{
		base.Awake ();

		registerEventListener (typeof(CESpaceshipStatus), spaceshipStatusEventListener);

		startPosition = transform.localPosition;

		trail = GetComponent<TrailRenderer>();
		trail.enabled = false;
	}

	protected override void OnTriggerEnter2D (Collider2D other)
	{
		if (!inWarpdrive && canHit) {
			if (other.tag == TagStringManager.Spaceship && !hasHit) {

				spawnExplosion();

				postEvent (new CECameraShake (Random.Range(0.1f, 0.25f), Random.Range(-2f, 2f), Random.Range(0.3f, 0.45f)));
				postEvent (new CEHitMine());
				postEvent (new CEDoSound(DoSoundAction.Play, "MineHit"));

				hasHit = true;
			}

			base.OnTriggerEnter2D (other);
		}
	}

	private void spawnExplosion() {
		PooledInstance inst = ObjectPoolManager.Instance.Spawn ("GenericPool", "mine_explosion");
		if (inst != null) {
			inst.CachedTransform.position = cachedTrans.position;
		}
	}

	private void mineWobble() {
		Vector3 dir = getDirection(targetAngle);

		// adjust speed based on distance from edges
		float edgeFactor = 1f;
		// moving down
		if (cachedTrans.position.y < 0) {
			edgeFactor = Mathf.InverseLerp (-6f, -3f, cachedTrans.position.y);
		}
		// moving up
		else {
			edgeFactor = Mathf.InverseLerp (6f, 3f, cachedTrans.position.y);
		}

		float edgeSpeed = targetSpeed * edgeFactor;

		cachedTrans.localPosition += dir * Time.deltaTime * edgeSpeed;
	}

	void OnDrawGizmos() {
		if (moveAngles != null) {
			foreach (float f in moveAngles) {
				Gizmos.DrawLine (transform.position, transform.position + getDirection(f) * 5f);
			}
		}

	}

	private Vector3 getDirection(float angle) {
		float a = -angle*Mathf.Deg2Rad;
		Vector3 dir = Vector3.zero;
		
		dir.x = -Mathf.Sin(a);
		dir.y = Mathf.Cos(a);
		dir.z = 0f;

		return dir;
	}

	private bool destroyedByMagnet;
	private void mineMagnetPush() {
		if (destroyedByMagnet) return;

		float distToShip = Vector3.Distance (cachedTrans.position, lastShipStatus.ShipTransform.position);
		Vector3 moveDir = (cachedTrans.position - lastShipStatus.ShipTransform.position).normalized;

		// bad way of getting the magnet from the ship, this should have been cached
		CircleCollider2D magnetCollider = (CircleCollider2D)lastShipStatus.ShipTransform.Find ("magnet").GetComponent<Collider2D>();

		float pushFactor = Mathf.InverseLerp (magnetCollider.radius, 0, distToShip);

		cachedTrans.localPosition += cachedTrans.InverseTransformDirection(moveDir) * pushFactor * Time.deltaTime * magnetCollider.radius;

		bool hit = false;
		if (cachedTrans.position.y < 0) {
			if (cachedTrans.position.y < -6f) {
				hit = true;
			}
		}
		else {
			if (cachedTrans.position.y > 6f) {
				hit = true;
			}
		}

		if (hit) {
			// kill!
			spawnExplosion();
			
			postEvent (new CECameraShake (Random.Range(0.05f, 0.15f), Random.Range(-0.5f, 0.5f), Random.Range(0.2f, 0.3f)));
			postEvent (new CEDoSound(DoSoundAction.Play, "MineHit"));

			GetComponent<Collider2D>().enabled = false;
			foreach (SpriteRenderer r in renderers) {
				r.enabled = false;
			}

			destroyedByMagnet = true;
		}
	}

	// if we are in warpdrive mode, destroy the mines that are left behind in a random fashion
	void Update() {
		if (lastShipStatus==null) return;
		if (lastShipStatus.ShipTransform == null) return;
		if (destroyedByMagnet) return;

		Spaceship ship = lastShipStatus.ShipTransform.GetComponent<Spaceship>();

		canHit = !ship.IsInvincible;
		inWarpdrive = ship.InWarpdrive;
		inMagnet = ship.InMagnet;

		mineWobble();

		if (inMagnet) {
			mineMagnetPush();
		}

		if (inWarpdrive && GetComponent<Collider2D>().enabled) {
			float x = cachedTrans.position.x;
			float leaveBehind = (Mathf.Sin (x) * 0.5f + 0.5f)*2f;

			if (x - leaveBehind < lastShipStatus.ShipTransform.position.x) {

				spawnExplosion();
				postEvent (new CEDoSound(DoSoundAction.Play, "MineHit"));

				GetComponent<Collider2D>().enabled = false;
				foreach (SpriteRenderer r in renderers) {
					r.enabled = false;
				}
			}
		}

	}
}
