﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ColliderType {
	None,
	Polygon,
	Box,
	Circle
}

public static class CrystalGroupCache {

	private static Dictionary<string, Object> crystalObjects;

	public static void Initialise() {
		crystalObjects = new Dictionary<string, Object>();
	}

	public static bool Contains (string path) {
		return crystalObjects.ContainsKey (path);
	}

	public static void Add (string path, Object obj) {
		crystalObjects[path] = obj;
	}

	public static Object Get(string path) {
		Object obj = null;
		crystalObjects.TryGetValue (path, out obj);
		return obj;
	}
}

[ExecuteInEditMode]
public class TileElement : MonoBehaviour {

	public ColliderType foregroundColliderType = ColliderType.Polygon;
	public bool kinematic = false;

	//
	public int warpdriveStepX=2;
	public int warpdriveStepY=2;

	public int backgroundLayer = 0;
	public int foregroundLayer = 1;

	public bool rotateForegroundElements=false;
	public float minForegroundRotateSpeed=-45f;
	public float maxForegroundRotateSpeed=45f;

	private Transform[] foregroundsTrans;
	private Transform backgroundTrans;

	private float[] targetRotationSpeeds;

	private Vector3[] localPositions;

	private Transform crystalsTrans;
	private Transform powerupTrans;
	
	private string crystalPrefix;
	private int crystalCount;


	private bool[] impulsed;
	private SpriteRenderer[] foregroundSprites;

	void Awake() {

		Transform foregroundRootTrans = transform.Find ("foregrounds");
		if (foregroundRootTrans != null) {

			localPositions = new Vector3[foregroundRootTrans.childCount];
			List<Transform> foregrounds = new List<Transform>();
			for (int i=0; i<foregroundRootTrans.childCount; i++) {
				foregrounds.Add (foregroundRootTrans.GetChild (i));

				foregrounds[i].localRotation = Quaternion.identity;
				SpriteRenderer foregroundSprite = foregrounds[i].GetComponent<SpriteRenderer>();
				if (foregroundSprite != null) {
					foregroundSprite.sortingLayerID = foregroundLayer;
				}

				localPositions[i] = foregrounds[i].localPosition;
			}

			foregroundsTrans = foregrounds.ToArray();
			impulsed = new bool[foregrounds.Count];
			foregroundSprites = new SpriteRenderer[foregrounds.Count];
			for (int i=0; i<foregrounds.Count; i++) {
				foregroundSprites[i] = foregrounds[i].GetComponent<SpriteRenderer>();
			}

			if (rotateForegroundElements) {
				targetRotationSpeeds = new float[foregroundsTrans.Length];
				for (int i=0; i<targetRotationSpeeds.Length; i++) {
					float pos = Random.Range (minForegroundRotateSpeed, maxForegroundRotateSpeed);
					float neg = Random.Range (-minForegroundRotateSpeed, -maxForegroundRotateSpeed);
					int posneg = Random.Range(0,2);
					targetRotationSpeeds[i] = (posneg == 0) ? pos : neg;
				}
			}
		}

		backgroundTrans = transform.Find ("background");
		if (backgroundTrans != null) {
			SpriteRenderer backgroundSprite = backgroundTrans.GetComponent<SpriteRenderer>();
			if (backgroundSprite != null) {
				backgroundSprite.sortingLayerID = backgroundLayer;
			}
		}

		crystalsTrans = transform.Find ("crystal_group"); // this is where we instantiate crystal groups
		powerupTrans = transform.Find ("power_group"); // this is where we instantiate powerup groups

		if (Application.isPlaying) {
			// enumerate the crystals for this tile element
			string n = name;
			Utilities.RemovePostfix (ref n, "(Clone)");
			Object[] crystals = Resources.LoadAll (ResourceStringManager.PathForCrystalGroups + "/" + n);
			crystalPrefix = ResourceStringManager.PathForCrystalGroups + "/" + n + "/group";
			crystalCount = crystals.Length;

			if (crystalCount > 0) {
				for (int i=0; i<crystalCount; i++) {
					string path = (crystalPrefix+(i+1).ToString());

					if (!CrystalGroupCache.Contains (path)) {

						CrystalGroupCache.Add (path, Resources.Load (path));
					}
				}
			}

			crystals = null;
			//
		}


		disableAllPowerupGroups();
	}

	public Transform[] Foregrounds {
		get{ 
			return foregroundsTrans;
		}
	}

	public Transform Background {
		get {
			return backgroundTrans;
		}
	}

	void FixedUpdate() {
		if (Foregrounds != null) {
			for (int i=0; i<Foregrounds.Length; i++) {
				Rigidbody2D r2d = Foregrounds[i].GetComponent<Rigidbody2D>();

				if (r2d != null) {
					r2d.angularVelocity = targetRotationSpeeds[i];
				}
			}
		}

		updateImpulses();
	}

	private void doKinematic() {
		if (Foregrounds != null) {
			for (int i=0; i<Foregrounds.Length; i++) {
				Rigidbody2D r2d = Foregrounds[i].GetComponent<Rigidbody2D>();

				if (r2d != null) {
					r2d.isKinematic = kinematic;
				}
			}
		}
	}

	public static ulong tileElementOrdinal; // a hack to prevent the first tile element to update the asteroids.

	public void OnSpawn() {
		// reset the impulses
		if (impulsed != null) {
			for (int i=0; i<impulsed.Length; i++) {

				impulsed[i] = (tileElementOrdinal == 0);
			}
		}

		doKinematic();

		disableAllPowerupGroups();

		tileElementOrdinal++;
	}

	// this goes through the list of asteroids and updates the impulse when it first becomes visible
	private void updateImpulses() {
		
		if (Foregrounds != null && Application.isPlaying) {
			for (int i=0; i<Foregrounds.Length; i++) {
				if (impulsed[i]) continue;

				if (foregroundSprites[i] != null && foregroundSprites[i].isVisible) {

					Rigidbody2D b2d = Foregrounds[i].GetComponent<Rigidbody2D>();
					
					if (b2d != null && !b2d.isKinematic) {
						b2d.AddForce (new Vector2(Random.Range(-3.3f, 3.3f), Random.Range(-3.3f, 3.3f)), ForceMode2D.Impulse);

						impulsed[i] = true;
					}
				}
			}
		}
	}

	public void SpawnPowerup() {
		// find a crystal group and enable it randomly
		if (powerupTrans != null) {

			float r = Random.Range(0f, 1f);
			if (r < 0.75f) {

				int idx = Random.Range (0, powerupTrans.childCount);
				Transform t = powerupTrans.GetChild(idx);
				Vector3 lp = t.localPosition;
				lp.y = Random.Range(-5f, 5f);
				t.localPosition = lp;

				t.gameObject.SetActive (true);
			}

		}
	}

	private GameObject lastCrystalGroup;
	public void SpawnCrystalGroup() {
		// find a crystal group and enable it randomly
		if (crystalsTrans != null) {
			int idx = Random.Range (0, crystalCount)+1;
			lastCrystalGroup = (GameObject)Instantiate (CrystalGroupCache.Get(crystalPrefix+idx));
			Vector3 pos = lastCrystalGroup.transform.position;
			Quaternion rot = lastCrystalGroup.transform.rotation;

			lastCrystalGroup.transform.parent = crystalsTrans;
			lastCrystalGroup.transform.localPosition = pos;
			lastCrystalGroup.transform.localRotation = rot;
		}
	}

	private void disableAllPowerupGroups() {
		if (powerupTrans != null) {
			for (int i=0; i<powerupTrans.childCount; i++) {
				Transform t = powerupTrans.GetChild (i);

				t.gameObject.SetActive (false);
			}
		}
	}

	void OnEnable() {

	}

	void OnDisable() {
		if (Application.isPlaying) {

			if (Foregrounds != null) {
				for (int i=0; i<Foregrounds.Length; i++) {
					Rigidbody2D r2d = Foregrounds[i].GetComponent<Rigidbody2D>();

					if (r2d != null) {
						r2d.isKinematic = true;
						r2d.velocity = Vector2.zero;
						r2d.angularVelocity = 0f;
						r2d.Sleep();
					}

					Foregrounds[i].localPosition = localPositions[i];
				}
			}

			if (lastCrystalGroup != null) {
				Destroy (lastCrystalGroup);
			}
		}
	}
}
