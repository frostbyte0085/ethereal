﻿using UnityEngine;
using System.Collections;

public class ZapSensor : MessageBehaviour {

	// Use this for initialization
	void Awake () {
	
	}

	private bool hasHit;
	void OnEnable() {
		hasHit = false;
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == TagStringManager.Spaceship) {

			Spaceship ship = other.GetComponent<Spaceship>();
			if (ship != null) {
				bool canHit = (!ship.IsInvincible && !ship.InWarpdrive);

				if (canHit && !hasHit) {

					PooledInstance fx = ObjectPoolManager.Instance.Spawn ("GenericPool", "zap_hit");
					if (fx != null) {
						fx.CachedTransform.parent = other.transform.Find ("zap_parent");
						fx.CachedTransform.localPosition = Vector3.zero;
					}

					postEvent (new CEHitZap());
					postEvent (new CECameraShake (Random.Range(0.1f, 0.25f), Random.Range(-2f, 2f), Random.Range(0.3f, 0.45f)));
					postEvent (new CEDoSound(DoSoundAction.Play, "ZapHit"));

					hasHit = true;
				}
			}
		}
	}

}
