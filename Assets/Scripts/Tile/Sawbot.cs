﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sawbot : MessageBehaviour {

	private TrailRenderer trail;

	void Awake() {
		trail = GetComponent<TrailRenderer>();
		trail.enabled = false;
	}

	void Start() {
		CircleCollider2D c2d = gameObject.AddComponent<CircleCollider2D>();
		c2d.isTrigger = true;
		c2d.radius = 1f;
	}

	private bool hasHit;
	void OnEnable() {
		hasHit = false;
	}

	void OnBecameVisible() {
		trail.enabled = true;
	}

	void OnBecameInvisible() {
		trail.enabled = false;
	}

	void OnDisable() {
		trail.enabled = false;
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == TagStringManager.Spaceship) {
			
			Spaceship ship = other.GetComponent<Spaceship>();
			if (ship != null) {
				bool canHit = (!ship.IsInvincible && !ship.InWarpdrive);
				
				if (canHit && !hasHit) {

					playEffect(other.transform.position);
					postEvent (new CEHitSawbot());
					postEvent (new CECameraShake (Random.Range(0.1f, 0.25f), Random.Range(-2f, 2f), Random.Range(0.3f, 0.45f)));
					postEvent (new CEDoSound(DoSoundAction.Play, "SawbotHit"));

					hasHit = true;
				}

				lastHitTime = Time.realtimeSinceStartup;
			}
		}
	}

	private float lastHitTime;
	void OnTriggerStay2D (Collider2D other) {
		if (other.tag == TagStringManager.Spaceship) {
			if (Time.realtimeSinceStartup - lastHitTime >= 0.1f) {

				playEffect(other.transform.position);
				lastHitTime = Time.realtimeSinceStartup;
			}
		}
	}

	private void playEffect(Vector3 position) {
		PooledInstance fx = ObjectPoolManager.Instance.Spawn ("GenericPool", "sawbot_hit");
		if (fx != null) {
			fx.CachedTransform.localPosition = position;
		}
	}
}
