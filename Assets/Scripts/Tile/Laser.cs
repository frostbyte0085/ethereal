﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Laser : MessageBehaviour {

	private enum LaserState {
		Unknown,
		WarmingUp,
		Firing,
		Damaging,
		Dying,
		Dead
	}

	public bool isTutorialMode=false;
	public Material material;
	public int beams=1;
	public float spacing = 2f;
	public Color minColor = Color.white;
	public Color maxColor = Color.white;
	public float minWidth = 0.05f;
	public float maxWidth = 0.3f;
	public float warmUpTime = 1f; // idle time, how long it waits
	public float firingTime = 1f; // the time it takes to build up to maximum width and intensity
	public float damagingTime = 0.5f; // the time duration for which damaging to the ship happens
	public float dyingTime = 1f; // how long it takes to fade out after damaging has been completed.

	#region beam class
	private class LaserBeam {

		private bool isTutorialMode;
		private LineRenderer lineRenderer;
		private GameObject go;
		private Vector3[] points;
		private float maxWidth;
		private float minWidth;
		private float yPosition;

		private Material material;

		private LaserState state;
		private float elapsed;

		private float warmUpTime;
		private float firingTime;
		private float damagingTime;
		private float dyingTime;

		private Color minColor;
		private Color maxColor;

		private float interpolator;
		private float colorBoost;
		private float alpha;

		private float currentStartX;

		private const float variation=0.15f; // percentage
		private BoxCollider2D b2d;

		public LaserBeam(Transform parent, Material material, float minWidth, float maxWidth, 
		                 Color minColor, Color maxColor, float y,
		                 float warmUpTime, float firingTime, float damagingTime, float dyingTime,
		                 bool isTutorialMode) {

			this.isTutorialMode = isTutorialMode;
			this.warmUpTime = warmUpTime + Random.Range(-warmUpTime*variation, warmUpTime*variation);
			this.firingTime = firingTime + Random.Range(-firingTime*variation, firingTime*variation);
			this.damagingTime = damagingTime + Random.Range(-damagingTime*variation, damagingTime*variation);
			this.dyingTime = dyingTime + Random.Range(-dyingTime*variation, dyingTime*variation);

			this.yPosition = y;
			this.minWidth = minWidth;
			this.maxWidth = maxWidth;
			this.minColor = minColor;
			this.maxColor = maxColor;

			this.material = (Material)Object.Instantiate (material);

			go = new GameObject("laser_beam");
			go.transform.parent = parent;
			go.layer = LayerMask.NameToLayer ("Particles");

			lineRenderer = go.AddComponent <LineRenderer>();
			lineRenderer.material = material;
			lineRenderer.SetVertexCount (2);
			points = new Vector3[2];

			Restart ();
		}

		public void Restart() {

			currentStartX = 1f;
			colorBoost = 0f;
			alpha = 1f;

			state = LaserState.Unknown;
			processStateChange (state, LaserState.WarmingUp);
		}

		private void updateState() {

			LaserState nextState = state;

			switch (nextState) {
			case LaserState.Firing:
				nextState = updateFiring();
				break;

			case LaserState.WarmingUp:
				nextState = updateWarmingUp();
				break;

			case LaserState.Damaging:
				nextState = updateDamaging();
				break;

			case LaserState.Dying:
				nextState = updateDying();
				break;

			default:break;
			}

			processStateChange (state, nextState);
		}

		private uint laserChargeInstanceID;
		private void processStateChange (LaserState state, LaserState nextState) {
			if (nextState != state) {
				// always reset the timer
				elapsed = 0f;

				if (nextState == LaserState.WarmingUp) {
					if (!isTutorialMode) {
						CEDoSound snd = new CEDoSound (DoSoundAction.Play, "LaserCharge");
						Messenger.instance.postEvent (snd);
						laserChargeInstanceID = snd.SoundInstanceID;
					}
				}
				else if (nextState == LaserState.Firing) {
					if (!isTutorialMode) {
						CEDoSound stopSnd = new CEDoSound(DoSoundAction.Stop, "LaserCharge");
						stopSnd.SoundInstanceID = laserChargeInstanceID;
						Messenger.instance.postEvent (stopSnd);

						CEDoSound snd = new CEDoSound(DoSoundAction.Play, "LaserShoot");
						Messenger.instance.postEvent (snd);
					}
				}
				else if (nextState == LaserState.Damaging) {
					// add a trigger box collider on the parent's object
					b2d = go.transform.parent.gameObject.AddComponent <BoxCollider2D>();
					b2d.isTrigger = true;
					Vector2 size = Vector2.zero;
					Vector2 center = Vector2.zero;

					Camera cam = isTutorialMode ? GameObject.FindGameObjectWithTag (TagStringManager.LaserCamera).GetComponent<Camera>() : Camera.main;

					size.x = cam.ViewportToWorldPoint(new Vector3(1, 0, cam.nearClipPlane)).x -
						cam.ViewportToWorldPoint(new Vector3(0, 0, cam.nearClipPlane)).x;
					size.y = maxWidth;
					
					b2d.size = size;
				}
				else if (nextState == LaserState.Dying) {
					if (b2d != null) {
						Object.Destroy (b2d);
					}
				}
				else if (nextState == LaserState.Dead) {
					if (!isTutorialMode) {

						Destroy ();
					}
				}

				this.state = nextState;
			}
		}

		private LaserState updateWarmingUp() {

			elapsed += Time.deltaTime;

			float xFactor = Mathf.Clamp01 (elapsed / 0.3f);
			currentStartX = Mathf.Lerp (1f, -0.5f, xFactor);

			if (elapsed >= warmUpTime) {
				return LaserState.Firing;
			}

			return LaserState.WarmingUp;
		}

		private LaserState updateFiring() {
			elapsed += Time.deltaTime;

			interpolator = Mathf.SmoothStep(0,1,elapsed / firingTime);
			interpolator = Mathf.Clamp01(interpolator);

			if (elapsed >= firingTime) {
				return LaserState.Damaging;
			}

			return LaserState.Firing;
		}

		private LaserState updateDamaging() {
			elapsed += Time.deltaTime;

			float factor = Mathf.Clamp01(elapsed / damagingTime);
			colorBoost = Mathf.Lerp ( 0f, 0.5f, factor);

			if (elapsed >= damagingTime) {
				return LaserState.Dying;
			}

			return LaserState.Damaging;
		}

		private LaserState updateDying() {
			elapsed += Time.deltaTime;

			float factor = Mathf.Clamp01(elapsed / dyingTime);
			colorBoost = Mathf.Lerp ( 0.5f, 0f, factor);
			alpha *= (1-factor);

			if (elapsed >= dyingTime) {
				// kill it
				return LaserState.Dead;
			}

			return LaserState.Dying;
		}

		public void Destroy() {
			Object.Destroy (material);
			GameObject.Destroy (go);
		}

		public void Update() {
			if (lineRenderer != null) {
				updateState();

				Camera cam = isTutorialMode ? GameObject.FindGameObjectWithTag (TagStringManager.LaserCamera).GetComponent<Camera>() : Camera.main;

				if (b2d != null) {
					Vector2 center = Vector2.zero;

					center.x = cam.ViewportToWorldPoint(new Vector3(0.5f, 0f, cam.nearClipPlane)).x;
					center.y = cam.ViewportToWorldPoint(new Vector3(0, yPosition, cam.nearClipPlane)).y;

					b2d.offset = center;
				}

				// snap the points to the edges of the screen
				points[0] = cam.ViewportToWorldPoint(new Vector3(currentStartX, yPosition, cam.nearClipPlane));
				points[1] = cam.ViewportToWorldPoint(new Vector3(1.5f, yPosition, cam.nearClipPlane));
				points[0].z = points[1].z = 0f;

				float width = Mathf.Lerp (minWidth, maxWidth, interpolator);
				Color color = Color.Lerp (minColor, maxColor, interpolator);
				color.r += colorBoost;
				color.g += colorBoost;
				color.b += colorBoost;

				color.a *= alpha;

				lineRenderer.SetWidth (width, width);
				lineRenderer.SetColors (color, color);

				lineRenderer.SetPosition (0, points[0]);
				lineRenderer.SetPosition (1, points[1]);
			}
		}

		public bool IsAlive {
			get {
				return state != LaserState.Dead;
			}
		}

		public bool IsDamaging {
			get {
				return state == LaserState.Damaging;
			}
		}

		public BoxCollider2D Collider {
			get {
				return b2d;
			}
		}
	}

	#endregion

	private LaserBeam[] laserBeams;
	private uint laserHitInstanceID;

	// Use this for initialization
	void Start () {
		if (isTutorialMode) {
			Spawn (0.5f);
		}
	}

	public void Spawn(float ypos) {
		laserBeams = new LaserBeam[beams];

		float yOffset = Mathf.Max(0, ypos - spacing*(beams-1)*0.5f);
		
		float y=yOffset;
		for (int i=0; i<beams; i++) {
			
			laserBeams[i] = new LaserBeam(transform, material, minWidth, maxWidth, minColor, maxColor, y,	
			                              warmUpTime, firingTime, damagingTime, dyingTime, isTutorialMode);

			y += spacing;
		}
	}

	public PooledInstance Instance {
		get; set;
	}
	
	// Update is called once per frame
	void Update () {

		if (laserBeams != null) {
			bool allDead = true;

			foreach (LaserBeam b in laserBeams) {
				b.Update();

				if (b.IsAlive) allDead = false;
			}

			if (allDead) {
				if (isTutorialMode) {
					foreach (LaserBeam b in laserBeams) {
						b.Restart();
					}
				}
				else {
					ObjectPoolManager.Instance.Recycle (Instance);
				}
			}
		}
	}

	private Spaceship lastShip;

	void OnTriggerStay2D (Collider2D other)
	{
		if (other.tag == TagStringManager.Spaceship) {
			if (lastShip == null) {
				lastShip = other.GetComponent<Spaceship>();
			}
			
			if (lastShip != null) {
				bool canHit = (!lastShip.IsInvincible && !lastShip.InWarpdrive);
				
				if (canHit) {
					postEvent (new CEHitLaser());
					postEvent (new CECameraShake (Random.Range(0.1f, 0.25f), Random.Range(-2f, 2f), Random.Range(0.3f, 0.45f)));
				}
			}
		}
	}
	
	// play the hit laser sound
	void OnTriggerExit2D (Collider2D other) {
		if (!isTutorialMode) {
			if (other.tag == TagStringManager.Spaceship) {
				stopHitSound();
			}
		}
	}

	// check for the instance id to spawn this only once.
	// the spaceship has multiple colliders!
	void OnTriggerEnter2D (Collider2D other) {
		if (!isTutorialMode) {
			if (other.tag == TagStringManager.Spaceship && laserHitInstanceID == 0) {
				CEDoSound snd = new CEDoSound(DoSoundAction.Play, "LaserHit");
				postEvent (snd);

				laserHitInstanceID = snd.SoundInstanceID;
			}
		}
	}

	private void stopHitSound() {
		if (laserHitInstanceID > 0 && !isTutorialMode) {
			CEDoSound sndStop = new CEDoSound(DoSoundAction.Stop, "LaserHit");
			sndStop.SoundInstanceID = laserHitInstanceID;
			
			postEvent (sndStop);

			laserHitInstanceID = 0;
		}
	}

	void OnSpawn() {
		if (isTutorialMode) {
		//	Spawn (0.5f);
		}
	}

	void OnRecycle() {
		stopHitSound();
		laserBeams = null;
	}

	void OnDestroy() {
		stopHitSound();
		laserBeams = null;
	}
}
