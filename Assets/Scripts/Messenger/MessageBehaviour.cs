using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MessageBehaviour : MonoBehaviour {

	private class RegisteredEventListener {
		public Type eventType;
		public Messenger.EventListener eventListener;
	}

	private List<RegisteredEventListener> registeredEventListeners = new List<RegisteredEventListener>();

    protected void registerEventListener(Type eventType, Messenger.EventListener eventListener, params Messenger.EventListener[] extraListeners) {
        Messenger.instance.registerEventListener(eventType, eventListener);

		// store event listener to be automatically removed OnDestroy
		RegisteredEventListener r = new RegisteredEventListener();
		r.eventType = eventType;
		r.eventListener += eventListener;

        registeredEventListeners.Add (r);
    }

    protected void unregisterEventListener(Type eventType, Messenger.EventListener eventListener) {
        Messenger.instance.unregisterEventListener(eventType, eventListener);

		// remove event listener from stored
		List<RegisteredEventListener> newList = new List<RegisteredEventListener>();
		foreach (RegisteredEventListener r in registeredEventListeners) {
			if (r.eventType == eventType && r.eventListener == eventListener) continue;
			newList.Add(r);
		}
		registeredEventListeners = newList;
    }

	protected void postEvent(ConvexEvent e, MessageBehaviour target) {
		Messenger.instance.postEvent(e, target);
	}

	protected void postEvent(ConvexEvent e, Dictionary<Type, int> executionOrder=null) {
        Messenger.instance.postEvent(e, executionOrder);
    }

	protected void postEvent(ConvexEvent e, out ConvexResponse[] responses, Dictionary<Type, int> executionOrder=null) {
		Messenger.instance.postEvent(e, out responses);
	}

	protected virtual void OnDestroy() {

		foreach (RegisteredEventListener r in registeredEventListeners) {
			unregisterEventListener(r.eventType, r.eventListener);
		}
		registeredEventListeners.Clear();
	}
}
