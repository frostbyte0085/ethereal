using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Messenger {
    // Singleton stuff
    private static Messenger _instance = null;
    public static Messenger instance {
        get
        {
            if (_instance == null) _instance = new Messenger();
            return _instance;
        }
    }

	// this is a class to sort listeners based on execution order.
	private class EventListenerSortContainer {

		public EventListenerSortContainer(EventListener listener, int executionOrder) {
			Listener = listener;
			ExecutionOrder = executionOrder;
		}

		public EventListener Listener {get; private set; }

		public int ExecutionOrder { get; private set; }

	}
	//

    public delegate void EventListener(ConvexEvent e);

    private Dictionary<Type, List<EventListener>> eventListenerDictionary = new Dictionary<Type, List<EventListener>>();

    public void registerEventListener(Type eventType, EventListener eventListener, params EventListener[] extraListeners) {

        if (!eventListenerDictionary.ContainsKey(eventType)) {
            eventListenerDictionary.Add(eventType, new List<EventListener>());
        }

        List<EventListener> eventListeners = eventListenerDictionary[eventType];
        eventListeners.Add(eventListener);
		if (extraListeners != null) {
			for (int i=0; i<extraListeners.Length; i++) {
				eventListeners.Add (extraListeners[i]);
			}
		}
    }

    public void unregisterEventListener(Type eventType, EventListener eventListener) {
        List<EventListener> eventListeners = eventListenerDictionary[eventType];
        eventListeners.Remove(eventListener);
    }

	private int sortListenerContainers (EventListenerSortContainer s1, EventListenerSortContainer s2) {

		return s1.ExecutionOrder.CompareTo (s2.ExecutionOrder);
	}

	// return a sorted list based on listener type priority
	// example:
	// priority 0 is high (executed first)
	// priotiyy 10 is low (executed last)
	private List<EventListener> sortListeners(List<EventListener> listeners, Dictionary<Type, int> executionOrder) {

		if (executionOrder != null) {
			List<EventListenerSortContainer> sortContainer = new List<EventListenerSortContainer>();

			List<EventListener> lateExecutionListeners = new List<EventListener>();

			// for all the listeners, add them to the new Sortable list
			for (int i=0; i<listeners.Count; i++) {
				EventListener listener = listeners[i];

				Type targetType = listener.Target.GetType();

				// if the type exists in the execution order list, add it.
				if (executionOrder.ContainsKey (targetType)) {
					int order = executionOrder[targetType];
					sortContainer.Add (new EventListenerSortContainer(listener, order));
				}
				// otherwise, we'll add those event listeners at the end of the list.
				else {
					lateExecutionListeners.Add (listener);
				}

			}

			// sort
			sortContainer.Sort (sortListenerContainers);

			List<EventListener> sortedListeners = new List<EventListener>();
			// now convert back to an EventListener list
			for (int i=0; i<sortContainer.Count; i++) {
				sortedListeners.Add (sortContainer[i].Listener);
			}

			sortedListeners.AddRange (lateExecutionListeners);

			// return the sorted listener list
			return sortedListeners;
		}
	
		// the default, unsorted listeners
		return listeners;
	}

    public void postEvent(ConvexEvent e, Dictionary<Type, int> executionOrder=null) {
        if (!eventListenerDictionary.ContainsKey(e.GetType())) return;

		List<EventListener> listeners = sortListeners (eventListenerDictionary[e.GetType()], executionOrder);

		for (int i=listeners.Count-1; i>=0; i--) {
			EventListener eh = listeners[i];
            eh(e);
        }
    }

	public void postEvent(ConvexEvent e, MessageBehaviour mb, Dictionary<Type, int> executionOrder = null) {
		if (!eventListenerDictionary.ContainsKey(e.GetType())) return;

		List<EventListener> listeners = sortListeners(eventListenerDictionary[e.GetType()], executionOrder);

		for (int i=listeners.Count-1; i>=0; i--) {
			EventListener eh = listeners[i];
			if (mb == eh.Target) eh(e);
		}
	}
	
	public void postEvent(ConvexEvent e, out ConvexResponse[] responses, Dictionary<Type, int> executionOrder=null) {
		responses = null;

		if (!eventListenerDictionary.ContainsKey(e.GetType())) return;

		List<EventListener> listeners = sortListeners (eventListenerDictionary[e.GetType()], executionOrder);

		List<ConvexResponse> responseList = new List<ConvexResponse>();
		for (int idx=listeners.Count-1; idx>=0; idx--) {
			EventListener eh = listeners[idx];

			eh(e);
			if (e.Responses != null) {
				for (int i=0; i<e.Responses.Count; i++) {
					responseList.Add (e.Responses[i] as ConvexResponse);
				}

				e.Responses.Clear();
			}
		}

		responses = responseList.ToArray();
	}
}
