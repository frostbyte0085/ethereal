using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class ConvexResponse {

}

public class ConvexEvent {
    
	public ConvexEvent() {
		responses = new List<ConvexResponse>();
	}

	public void AddResponse (ConvexResponse response) {
		if (responses.Contains (response))
		    return;
		
		responses.Add (response);
	}

	private List<ConvexResponse> responses;

	public List<ConvexResponse> Responses {

		get {
			return responses;
		}
	}

}
