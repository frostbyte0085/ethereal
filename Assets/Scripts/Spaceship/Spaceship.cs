﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum StatType {
	Agility,
	Durability,
	Powerup,
	MaxStat
}

public class Spaceship : MessageBehaviour {
	
	public SpaceshipDetails details;
	public Object damagePSObj;

	private Transform damagePSMountTrans;

	private float speed;
	private float durability;

	private float speedInterpTime;

	public float collisionTolerance = 0.65f;
	public float verticalSpeedDampen = 20;
	
	private float magnetDuration;
	private float magnetStrength;
	private float magnetTimeLeft;

	private float warpdriveDuration;
	private float warpdriveSpeed;
	private float warpdriveTimeLeft;

	private float repairEffectiveness;

	private float inputDeltaY;
	private float targetSpeed;
	private float axis;
	private Rigidbody2D r2d;
	private Transform cachedTrans;

	private ParticleSystem enginePS;

	private CEDifficultyUpdate difficulty;

	private CircleCollider2D magnetCollider;
	private CircleCollider2D permMagnetCollider; // this is the persistent magnet, very short range. used to help with the collection of crystals.

	private Collider2D[] mainColliders;

	private GamePhaseMode lastGameMode = GamePhaseMode.Invalid;
	
	public float warpdriveAxisFactor=0.5f;
	public float warpdriveAxisSpeed = 1f;

	private float sparksElapsed;
	private float hullSfxElapsed;

	private float currentHealth;

	private TrailRenderer warpdriveTrail;

	private Animator anim;
	
	private SpriteRenderer spriteRenderer;

	private bool isHandlingDeath;
	private bool isInvincible;

	private bool lasersActive;
	private Transform laserTrans;
	private float laserElapsed;

	#region looping sound instance ids
	private uint warpdriveInstanceID;
	private uint magnetInstanceID;
	private uint damageFireInstanceID;
	#endregion

	private Vector3 lastPosition;
	private Vector2 collisionTangent;

	private float collisionAxis;
	private float idleAxis;

	private ParticleSystem laserFeedbackPS;
	private ParticleSystem damagePS;

	public float timeAlive;

	#region constants
	private const float durabilityFactor = 0.055f;
	private Vector2 mineDurabilityFactor = new Vector2(0.5f, 0.3f);
	private Vector2 sawbotDurabilityFactor = new Vector2(0.6f, 0.35f);
	private Vector2 laserDurabilityFactor = new Vector2(0.75f, 0.45f);
	private Vector2 zapDurabilityFactor = new Vector2(0.4f, 0.25f);
	private const float sparksInterval = 0.15f;
	private const float hullSfxInterval = 0.25f;

	// restore warning health constants
	private Vector2 restoreHealthDelay = new Vector2(8f, 4f);
	private Vector2 restoreHealthMaxDelta = new Vector2(0.0001f, 0.0003f);
	private Vector2 restoreHealthTarget = new Vector2(Constants.healthWarningLevel*0.2f, Constants.healthWarningLevel+0.05f);
	#endregion

	#region wing trail animation & rendering
	private Transform wingTrailTrans;
	private Transform leftWingUpTrans;
	private Transform leftWingDownTrans;
	private Transform rightWingUpTrans;
	private Transform rightWingDownTrans;
	private TrailRenderer leftWingTrail;
	private TrailRenderer rightWingTrail;

	#endregion

	public float WarpdriveDuration { get { return warpdriveDuration; }  }
	public float MagnetDuration { get { return magnetDuration; } }

	private float warpdriveBuildupSeconds = 1.5f;

	private CECurrentSaveData saveData;


	// horrible... 3 days before launch
	private float lastMoveSoundTime;
	private float lastAxis;
	private bool upSoundPlayed;
	private bool downSoundPlayed;

	private float sgn(float s) {
		if (s <= 0) return -1f;
		return 1f;
	}

	#region event listeners
	private float inputVel;
	private void inputEventListener(ConvexEvent evt) {
		CESpaceshipInput e = (CESpaceshipInput)evt;

		if (Mathf.Abs(r2d.velocity.y) > 5f && e.IsTouching) {

			if ( (sgn(e.Axis) != sgn(lastAxis)) || Mathf.Approximately(lastAxis, 0f)) {

				lastAxis = e.Axis;

				if (Time.timeSinceLevelLoad - lastMoveSoundTime > 1f) {

					if (lastAxis > 0 && !upSoundPlayed) {
						postEvent (new CEDoSound (DoSoundAction.Play, "ShipMove"));
						lastMoveSoundTime = Time.timeSinceLevelLoad;
					}

					if (lastAxis < 0 && !downSoundPlayed) {
						postEvent (new CEDoSound (DoSoundAction.Play, "ShipMove"));
						lastMoveSoundTime = Time.timeSinceLevelLoad;
					}

					if (lastAxis < 0) {
						upSoundPlayed = false;
						downSoundPlayed = true;
					}
					
					if (lastAxis > 0) {
						downSoundPlayed = false;
						upSoundPlayed = true;
					}
				
				}
			}
		}
		else {
			lastAxis = 0f;
			downSoundPlayed = upSoundPlayed = false;
		}

		if (e.IsTranslating) {
			inputDeltaY = Mathf.Clamp((e.TargetPosition.y - cachedTrans.position.y),  -Constants.tileScreenSize/2, Constants.tileScreenSize/2);
		}
		else {
			inputDeltaY = Mathf.SmoothDamp (inputDeltaY, e.Axis, ref inputVel, speedInterpTime, 2000f, Time.smoothDeltaTime);
		}

		axis = inputDeltaY;
	}

	private void saveDataEventListener (ConvexEvent evt) {
		saveData = (CECurrentSaveData)evt;
	}

	private float timeLastHitLaser;

	private void hitLaserEventListener(ConvexEvent evt) {
		CEHitLaser e = (CEHitLaser)evt;

		// scale by time since it's done per frame while the spaceship is touching the laser.
		currentHealth -= Mathf.Lerp (laserDurabilityFactor.x, laserDurabilityFactor.y, durability) * Time.deltaTime;
		currentHealth = Mathf.Max (0, currentHealth);

		timeLastHitLaser = Time.realtimeSinceStartup;
		if (!laserFeedbackPS.isPlaying) {
			laserFeedbackPS.Play(true);
		}

	}

	private void hitMineEventListener(ConvexEvent evt) {
		CEHitMine e = (CEHitMine)evt;

		currentHealth -= Mathf.Lerp (mineDurabilityFactor.x, mineDurabilityFactor.y, durability);
		currentHealth = Mathf.Max (0, currentHealth);
	}

	private void hitSawbotEventListener(ConvexEvent evt) {
		CEHitSawbot e = (CEHitSawbot)evt;

		currentHealth -= Mathf.Lerp (sawbotDurabilityFactor.x, sawbotDurabilityFactor.y, durability);
		currentHealth = Mathf.Max (0, currentHealth);
	}

	private void hitZapEventListener(ConvexEvent evt) {
		CEHitZap e = (CEHitZap)evt;

		currentHealth -= Mathf.Lerp (zapDurabilityFactor.x, zapDurabilityFactor.y, durability);
		currentHealth = Mathf.Max (0, currentHealth);
	}

	private void lasersEventListener (ConvexEvent evt) {
		CELasers e = (CELasers)evt;

		lasersActive = (e.Mode == LaserMode.Enabled);
	}

	private bool playWarpdriveSound=true;
	private void continueEventListener(ConvexEvent evt) {
		CEContinue e = (CEContinue)evt;

		spriteRenderer.enabled = true;
		isHandlingDeath = false;

		warpdriveTimeLeft = 0f;

		ConvexResponse[] responses = null;
		postEvent (new CEGetWarpdrivePath(), out responses);
		if (responses != null) {

			CRWarpdrivePath wp = (CRWarpdrivePath)responses[0];
			if (wp.Points != null && wp.Points.Count > 0) {

				Vector2 p0 = wp.Points[0];

				Vector3 pos = cachedTrans.position;

				pos.x = p0.x;
				pos.y = p0.y;

				cachedTrans.position = pos;

				playWarpdriveSound = false;
				StartCoroutine (changeDuration());
				postEvent (new CEWarpdriveCollect (WarpdrivePhase.Start, warpdriveDuration));
				playWarpdriveSound = true;
			}
		}

		foreach (Collider2D c in mainColliders) {
			c.enabled = true;
		}

		if (enginePS != null) {
			enginePS.Play();
		}

		r2d.velocity = Vector2.zero;
		inputDeltaY = 0f;
		collisionAxis = 0f;
		collisionTangent = cachedTrans.right;
		currentHealth = 1f;
		axis = 0f;

		stopFires();
	}

	private IEnumerator changeDuration() {
		float oldBuildup = warpdriveBuildupSeconds;
		float oldDuration = warpdriveDuration;
		warpdriveDuration = 1f;
		warpdriveBuildupSeconds = 0.2f;
		yield return new WaitForSeconds (warpdriveDuration);

		warpdriveBuildupSeconds = oldBuildup;
		warpdriveDuration = oldDuration;
	}

	private void difficultyEventListener(ConvexEvent evt) {
		CEDifficultyUpdate e = (CEDifficultyUpdate)evt;

		difficulty = e;
	}

	private PooledInstance lastMagnetFxInstance;
	private void magnetCollectEventListener(ConvexEvent evt) {
		CEMagnetCollect e = (CEMagnetCollect)evt;

		if (e.Phase == MagnetPhase.Start) {
			// if we're in warpdrive, ignore this
			if (InWarpdrive) return;

			postEvent (new CEDoSound(DoSoundAction.Play, "MagnetCollect"));

			magnetTimeLeft = magnetDuration;

			magnetCollider.enabled = true;

			lastMagnetFxInstance = ObjectPoolManager.Instance.Spawn ("GenericPool", "magnet_aura");
			if (lastMagnetFxInstance != null) {
				lastMagnetFxInstance.CachedTransform.parent = cachedTrans;
				lastMagnetFxInstance.CachedTransform.localPosition = Vector3.zero;
			}

			CEDoSound snd = new CEDoSound (DoSoundAction.Play, "MagnetLoop");
			postEvent (snd);

			magnetInstanceID = snd.SoundInstanceID;

			postEvent (new CEMagnetInfo(true, e.Duration));

		}
		else if (e.Phase == MagnetPhase.End) {
			magnetCollider.enabled = false;

			if (lastMagnetFxInstance != null) {
				lastMagnetFxInstance.CachedParticleSystem.Stop();
			}

			magnetTimeLeft = 0f;

			CEDoSound sndStop = new CEDoSound(DoSoundAction.Stop, "MagnetLoop");
			sndStop.SoundInstanceID = magnetInstanceID;

			postEvent (sndStop);

			postEvent (new CEMagnetInfo(false));
		}
	}

	private IEnumerator warpdriveCameraShake() {

		float nextDelay = 0.5f;

		while (true) {
			yield return new WaitForSeconds(nextDelay);

			nextDelay = Random.Range(0.3f, 0.5f);
			postEvent (new CECameraShake (Random.Range(0.25f, 0.35f), Random.Range(-2.5f, 2.5f), nextDelay));
		}

	}

	private PooledInstance lastWarpdriveFxInstance;
	private void warpdriveCollectEventListener(ConvexEvent evt) {
		CEWarpdriveCollect e = (CEWarpdriveCollect)evt;
		
		if (e.Phase == WarpdrivePhase.Start) {
			// if the ship has a magnet, disable the magnet before picking up the warpdrive
			if (InMagnet) {
				postEvent (new CEMagnetInfo(false, 0f, true)); // force the magnet info to hide
				postEvent (new CEMagnetCollect(MagnetPhase.End));
			}

			if (playWarpdriveSound) {
				postEvent (new CEDoSound(DoSoundAction.Play, "WarpdriveEngaged", 0.75f));
			}
			postEvent (new CEDoSound(DoSoundAction.Play, "WarpdriveCollect"));

			//
			// the looping sound
			CEDoSound loopSnd = new CEDoSound(DoSoundAction.Play, "WarpdriveLoop");
			postEvent (loopSnd);
			warpdriveInstanceID = loopSnd.SoundInstanceID;
			//

			warpdriveTimeLeft = warpdriveDuration;

			disableCollisions();

			inputDeltaY = 0f;
			lastPosition = cachedTrans.position;

			// enable magnet
			magnetCollider.enabled = true;

			StartCoroutine (enableTrail());

			lastWarpdriveFxInstance = ObjectPoolManager.Instance.Spawn ("GenericPool", "warpdrive_aura");
			if (lastWarpdriveFxInstance != null) {
				lastWarpdriveFxInstance.CachedTransform.parent = cachedTrans;
				lastWarpdriveFxInstance.CachedTransform.localPosition = Vector3.zero;
				lastWarpdriveFxInstance.CachedTransform.Find ("Trail").GetComponent<ParticleSystem>().simulationSpace = ParticleSystemSimulationSpace.World;
			}

			StartCoroutine ("warpdriveCameraShake");

			postEvent (new CEWarpdriveInfo(true, e.Duration));
		}
		else if (e.Phase == WarpdrivePhase.End) {

			// don't enable the asteroid collisions immediately, wait until the player gets the spaceship in a safe location
			Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer("Tile"), LayerMask.NameToLayer("Spaceship"), false);
			Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer("Particles"), LayerMask.NameToLayer("Spaceship"), false); // this is for the lasers.

			StartCoroutine (disableTrail());

			//
			// disable the looping sound
			CEDoSound sndStopLoop = new CEDoSound(DoSoundAction.Stop, "WarpdriveLoop");
			sndStopLoop.SoundInstanceID = warpdriveInstanceID;
			postEvent (sndStopLoop);
			//

			// disable magnet
			magnetCollider.enabled = false;

			warpdriveTimeLeft = 0f;

			// start the invincibility shield
			StartCoroutine (makeInvincible());

			if (lastWarpdriveFxInstance != null) {
				lastWarpdriveFxInstance.CachedTransform.Find ("Trail").GetComponent<ParticleSystem>().simulationSpace = ParticleSystemSimulationSpace.Local;
				lastWarpdriveFxInstance.CachedParticleSystem.Stop();
			}

			StopCoroutine ("warpdriveCameraShake");

			postEvent (new CEWarpdriveInfo(false));
		}
		
	}

	private void disableCollisions() {
		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer("Tile"), LayerMask.NameToLayer("Spaceship"), true);
		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer("Zap"), LayerMask.NameToLayer("Spaceship"), true);
		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer("Asteroid"), LayerMask.NameToLayer("Spaceship"), true);
		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer("Particles"), LayerMask.NameToLayer("Spaceship"), true); // this is for the lasers.
	}

	private void enableCollisions() {
		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer("Zap"), LayerMask.NameToLayer("Spaceship"), false);
		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer("Tile"), LayerMask.NameToLayer("Spaceship"), false);
		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer("Asteroid"), LayerMask.NameToLayer("Spaceship"), false);
		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer("Particles"), LayerMask.NameToLayer("Spaceship"), false); // this is for the lasers.
	}

	private IEnumerator enableTrail() {
		yield return new WaitForSeconds (0.3f);

		warpdriveTrail.enabled = true;
	}

	private IEnumerator disableTrail() {
		yield return new WaitForSeconds (0.3f);

		warpdriveTrail.enabled = false;
	}
	
	private void repairCollectEventListener(ConvexEvent evt) {
		CERepairCollect e = (CERepairCollect)evt;

		postEvent (new CEDoSound(DoSoundAction.Play, "RepairCollect"));

		currentHealth += repairEffectiveness;
		currentHealth = Mathf.Clamp01(currentHealth);
	}

	private void gamePhaseEventListener(ConvexEvent evt) {
		CEGamePhase e = (CEGamePhase)evt;

		if (e.Mode != lastGameMode) {
			switch (e.Mode) {
			case GamePhaseMode.Gameplay:
				break;

			case GamePhaseMode.Lose:
				break;

			case GamePhaseMode.Menu:
				break;

			}
		}

		lastGameMode = e.Mode;
	}
	#endregion

	// Use this for initialization
	void Awake () {
		anim = GetComponent<Animator>();

		// reset collision layers
		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer("Tile"), LayerMask.NameToLayer("Spaceship"), false);

		registerEventListener (typeof(CESpaceshipInput), inputEventListener);
		registerEventListener (typeof(CEDifficultyUpdate), difficultyEventListener);
		registerEventListener (typeof(CEMagnetCollect), magnetCollectEventListener);
		registerEventListener (typeof(CEWarpdriveCollect), warpdriveCollectEventListener);
		registerEventListener (typeof(CERepairCollect), repairCollectEventListener);
		registerEventListener (typeof(CEGamePhase), gamePhaseEventListener);
		registerEventListener (typeof(CEContinue), continueEventListener);
		registerEventListener (typeof(CELasers), lasersEventListener);
		registerEventListener (typeof(CEHitMine), hitMineEventListener);
		registerEventListener (typeof(CEHitSawbot), hitSawbotEventListener);
		registerEventListener (typeof(CEHitZap), hitZapEventListener);
		registerEventListener (typeof(CEHitLaser), hitLaserEventListener);
		registerEventListener (typeof(CECurrentSaveData), saveDataEventListener);

		cachedTrans = transform;
		r2d = GetComponent<Rigidbody2D>();

		wingTrailTrans = cachedTrans.Find("wing_trails");
		if (wingTrailTrans != null) {
			leftWingUpTrans = wingTrailTrans.Find ("left_up");
			leftWingDownTrans = wingTrailTrans.Find ("left_down");
			rightWingUpTrans = wingTrailTrans.Find ("right_up");
			rightWingDownTrans = wingTrailTrans.Find ("right_down");
			leftWingTrail = wingTrailTrans.Find ("trail_left").GetComponent<TrailRenderer>();
			rightWingTrail = wingTrailTrans.Find ("trail_right").GetComponent<TrailRenderer>();
		}


		damagePSMountTrans = cachedTrans.Find ("damage_root");
		laserFeedbackPS = cachedTrans.Find ("laser_feedback").GetComponent<ParticleSystem>();

		if (damagePSObj != null) {
			damagePS = ((GameObject)Instantiate(damagePSObj)).GetComponent<ParticleSystem>();
			damagePS.transform.parent = damagePSMountTrans;
			damagePS.transform.localPosition = Vector3.zero;
			ParticleSystem[] damageSystems = damagePS.GetComponentsInChildren<ParticleSystem>();
			foreach (ParticleSystem p in damageSystems) {
				p.simulationSpace = ParticleSystemSimulationSpace.Local; // default to local
			}
		}

		magnetCollider = (CircleCollider2D)cachedTrans.Find ("magnet").GetComponent<Collider2D>();
		permMagnetCollider = (CircleCollider2D)cachedTrans.Find ("perm_magnet").GetComponent<Collider2D>();
		mainColliders = cachedTrans.GetComponents<Collider2D>();

		warpdriveTrail = GetComponentInChildren<TrailRenderer>();

		currentHealth = 1f;

		spriteRenderer = GetComponent<SpriteRenderer>();

		laserTrans = cachedTrans.Find ("laser");
		Transform enginePSTrans = cachedTrans.Find ("engine");
		if (enginePSTrans != null) {
			enginePS = enginePSTrans.GetComponent<ParticleSystem>();
		}

		collisionTangent = cachedTrans.right;

		// start the invincibility shield
		StartCoroutine (makeInvincible());
	}

	public void Prepare(float speed, float durability, float magnetDuration, float magnetStrength, float warpdriveDuration, 
	                    float warpdriveSpeed, float repairEffectiveness, float speedInterpTime) {

		this.durability = durability;
		this.speed = speed;

		this.magnetDuration = magnetDuration;
		this.magnetStrength = magnetStrength;

		this.warpdriveDuration = warpdriveDuration;
		this.warpdriveSpeed = warpdriveSpeed;

		this.repairEffectiveness = repairEffectiveness;

		this.speedInterpTime = speedInterpTime;

		if (magnetCollider != null) {
			magnetCollider.radius = magnetStrength;
		}
	}

	public bool IsAlive {
		get {
			return currentHealth > 0f;
		}
	}

	// Update is called once per frame
	void FixedUpdate () {

		switch (lastGameMode) {
		case GamePhaseMode.Menu:
			fixedUpdateMenuMode();
			break;

		case GamePhaseMode.Gameplay:
			fixedUpdateGameplayMode();
			break;

		case GamePhaseMode.Lose:
			fixedUpdateLostMode();
			break;
		}

	}

	private void fixedUpdateGameplayMode() {

		if (difficulty == null)
			return;

		if (IsAlive) {
			if (warpdriveTimeLeft <= 0)  {

				float d = Vector2.Dot (collisionTangent, cachedTrans.right);

				float angle = Mathf.Acos (d) * Mathf.Sign(collisionTangent.y);

				float sn = Mathf.Sin (angle);
				float cs = Mathf.Cos (angle);

				Vector2 desiredVelocity = new Vector2(difficulty.Speed, inputDeltaY * speed);

				Vector2 targetVelocity = new Vector2(desiredVelocity.x * cs - desiredVelocity.y * sn, 
				                                     desiredVelocity.x * sn + desiredVelocity.y * cs);


				r2d.velocity = targetVelocity;

				Velocity = r2d.velocity;
			}
			else {
				inputDeltaY = 0f;
				r2d.velocity = Vector2.zero;
			}
		}
		else {
			r2d.velocity = Vector2.zero;
		}
	}

	private void fixedUpdateMenuMode() {

	}

	private void fixedUpdateLostMode() {

	}

	private float addSpeedVel;
	void Update() {

		//
		//currentHealth = 1f; // uncomment for invincibility
		//

		if (Input.GetKeyDown (KeyCode.A)) {
			postEvent (new CEWarpdriveCollect (WarpdrivePhase.Start, warpdriveDuration));
		}

		if (Input.GetKeyDown (KeyCode.B)) {
			postEvent (new CEMagnetCollect (MagnetPhase.Start, magnetDuration));
		}

		switch (lastGameMode) {
		case GamePhaseMode.Menu:
			updateMenuMode();
			break;

		case GamePhaseMode.Gameplay:
			updateGameplayMode();
			break;

		case GamePhaseMode.Lose:
			updateLostMode();
			break;
		}

		postEvent (new CESpaceshipStatus(
			cachedTrans, Velocity, currentHealth, spriteRenderer.sprite.bounds, isInvincible
			));

		if (laserFeedbackPS.isPlaying) {
			if (Time.realtimeSinceStartup - timeLastHitLaser > 0.5f) {
				laserFeedbackPS.Stop();
			}
		}
	}

	private float currentAnimVel;
	private float currentAnim;

	private float interpolation;
	private Vector3[] points;

	public bool InWarpdrive {
		get {
			return warpdriveTimeLeft > 0;
		}
	}

	public bool InMagnet {
		get {
			return (magnetTimeLeft > 0) && (magnetCollider.enabled);
		}
	}

	public bool IsInvincible {
		get {
			return isInvincible;
		}
	}

	private void stopWarpdrive() {
		warpdriveTimeLeft = 0f;
		r2d.isKinematic = false;
		interpolation = 0f;
		points = null;

		GetComponent<Collider2D>().enabled = true;

		postEvent (new CEWarpdriveCollect(WarpdrivePhase.End));
	}

	private void stopFires(bool stop=true) {
		if (damagePS != null) {
			ParticleSystem[] systems = damagePS.transform.GetComponentsInChildren<ParticleSystem>();
			foreach (ParticleSystem p in systems) {
				p.simulationSpace = ParticleSystemSimulationSpace.Local;
			}

			if (stop) {
				damagePS.Stop();
				damagePS.Clear();
			}

			/*
			CEDoSound sndStop = new CEDoSound(DoSoundAction.Stop, "DamageFireLoop");
			sndStop.SoundInstanceID = damageFireInstanceID;
			postEvent (sndStop);
			*/
		}
		isFireEnabled = false;
	}

	private bool isFireEnabled;
	private void updateGameplayMode() {

		timeAlive += Time.deltaTime;

		hullSfxElapsed += Time.deltaTime;
		sparksElapsed += Time.deltaTime;

		// apply dampening to the collision tangent
		collisionTangent = Vector2.MoveTowards (collisionTangent, cachedTrans.right, 0.05f);

		// if the energy is too low, start playing the fire effect
		if (currentHealth < 0.35f && !isFireEnabled) {

			if (damagePS != null) {
				ParticleSystem[] systems = damagePS.transform.GetComponentsInChildren<ParticleSystem>();
				foreach (ParticleSystem p in systems) {
					p.simulationSpace = ParticleSystemSimulationSpace.World;
				}

				/*
				CEDoSound snd = new CEDoSound (DoSoundAction.Play, "DamageFireLoop");
				postEvent (snd);
				damageFireInstanceID = snd.SoundInstanceID;
				*/

				damagePS.Clear();
				damagePS.Simulate (0.001f, true);
				damagePS.Play();
			}
			isFireEnabled = true;
		}

		if (currentHealth > 0.35f && isFireEnabled) {
			stopFires(false);
		}

		// check for active magnet
		if (magnetTimeLeft > 0) {
			magnetTimeLeft -= Time.deltaTime;
			
			if (magnetTimeLeft <= 0) {
				magnetTimeLeft = 0f;
				postEvent (new CEMagnetCollect(MagnetPhase.End));
			}
		}
		
		// check for warpdrive
		if (warpdriveTimeLeft > 0) {
			warpdriveTimeLeft -= Time.deltaTime;
			if (warpdriveTimeLeft <= 0) {
				stopWarpdrive();
			}
			else {

				ConvexResponse[] responses = null;
				postEvent (new CEGetWarpdrivePath(), out responses);
				if (responses != null) {
					CRWarpdrivePath wp = (CRWarpdrivePath)responses[0];

					// get new points
					if (points == null && wp.Points.Count > 2) {
						points = new Vector3[4];
						points[0] = wp.Points[0] - (wp.Points[1] - wp.Points[0]);
						points[1] = wp.Points[0];
						points[2] = wp.Points[1];
						points[3] = wp.Points[2];

						p = points;
					}

					if (points != null) {
						//r2d.isKinematic = true;
						GetComponent<Collider2D>().enabled = false;

						if (interpolation < 1f) {
							float adjustedSpeed = difficulty.Speed;

							if (warpdriveTimeLeft >= warpdriveDuration - warpdriveBuildupSeconds) {
								adjustedSpeed = Mathf.Lerp (difficulty.Speed, difficulty.Speed + warpdriveSpeed, 
								                            Mathf.InverseLerp(warpdriveDuration, warpdriveDuration-warpdriveBuildupSeconds, warpdriveTimeLeft));
							}
							else if (warpdriveTimeLeft <= warpdriveBuildupSeconds) {
								adjustedSpeed = Mathf.Lerp (difficulty.Speed + warpdriveSpeed, difficulty.Speed, 
								                            Mathf.InverseLerp(warpdriveBuildupSeconds, 0, warpdriveTimeLeft));
							}
							else {
								adjustedSpeed = difficulty.Speed + warpdriveSpeed;
							}

							interpolation += Time.deltaTime * adjustedSpeed;

							axis = Mathf.MoveTowards(axis, (points[2].y - cachedTrans.position.y) * warpdriveAxisFactor, warpdriveAxisSpeed );
							axis = Mathf.Clamp (axis, -1f, 1f);

							Vector3 pos = cachedTrans.position;

							pos = Mathf2.CatmulRomInterpolate (points[0], points[1], points[2], points[3], interpolation);

							cachedTrans.position = Vector3.MoveTowards (cachedTrans.position, pos, Time.deltaTime * adjustedSpeed);

							if (interpolation >= 1f) {
								interpolation = 0f;
								points = null;
							}

						}

					}
					else {
						// quit it here. this should never happen, as points must be continous.
						stopWarpdrive();
					}

				}

				// update velocity manually here
				Vector3 deltaPos = ((cachedTrans.position - lastPosition) / Time.deltaTime);				
				Velocity = new Vector2(deltaPos.x, deltaPos.y);

				lastPosition = cachedTrans.position;
			}
		}

		// here we check if the velocity of the spaceship is less than a threshold.
		// this means that the spaceship could be stuck somewhere, let's kill it.
		// TODO: fix, doesn't work
		if (Velocity.magnitude < 10f && timeAlive > 0.5f) {
			if (!isInvincible) {
			//	currentHealth = 0f;
			}
		}

		// if we have lasers and we are not warpdriving, activate them
		if (lasersActive && (warpdriveTimeLeft <= 0) ) {
			/*
			laserElapsed += Time.deltaTime;

			if (laserElapsed >= 0.2f) {

				laserElapsed = 0f;

				PooledInstance vfxGlow = ObjectPoolManager.Instance.Spawn ("GenericPool", "glow");
				if (vfxGlow != null) {
					vfxGlow.CachedTransform.parent = laserTrans;
					vfxGlow.CachedTransform.localPosition = Vector3.zero;

					vfxGlow.CachedParticleSystem.Play();
				}

			}
			*/
		}

		collisionAxis = Mathf.Lerp (collisionAxis, collisionTangent.y, 0.2f);

		currentAnim = Mathf.SmoothDamp (currentAnim, axis, ref currentAnimVel, 0.1f);

		float totalAxis = currentAnim + collisionAxis;

		if (Mathf.Abs(totalAxis) < 0.05f) {
			idleAxis = Mathf.MoveTowards(idleAxis, Mathf.Sin(Time.time*2) * 0.2f, 0.05f);
		}
		else {
			idleAxis = Mathf.MoveTowards(idleAxis, 0f, 0.05f);
		}

		float animAxis = totalAxis*0.5f + idleAxis;
		float wingAxis = Mathf.Clamp(animAxis, -1f, 1f)*0.5f+0.5f;

		Vector3 leftPosition = Vector3.Lerp (leftWingUpTrans.position, leftWingDownTrans.position, wingAxis);
		Vector3 rightPosition = Vector3.Lerp (rightWingUpTrans.position, rightWingDownTrans.position, wingAxis);

		leftWingTrail.transform.position = leftPosition;
		rightWingTrail.transform.position = rightPosition;

		anim.updateMode = AnimatorUpdateMode.UnscaledTime;
		anim.SetFloat ("Current", animAxis);
		anim.Update (Time.deltaTime);
		
		axis *= (1 - Time.deltaTime * verticalSpeedDampen);

		checkForDeath();
		restoreWarningHealth();
	}

	// wait a bit before being able to restore health
	// this delay is defined by the durability of the ship
	private IEnumerator waitForRestoreHealth() {
		canRestoreHealth = false;

		yield return new WaitForSeconds(currentRestoreHealthDelay);

		canRestoreHealth = true;
	}

	private bool canRestoreHealth;

	private float currentRestoreHealthDelay;
	private float currentRestoreHealthTarget;
	private float currentRestoreHealthMaxDelta;

	// this slowly restores health to the warning level / 2 if it's below it.
	private void restoreWarningHealth() {

		currentRestoreHealthDelay = Mathf.Lerp (restoreHealthDelay.x, restoreHealthDelay.y, durability);
		currentRestoreHealthMaxDelta = Mathf.Lerp (restoreHealthMaxDelta.x, restoreHealthMaxDelta.y, durability);
		currentRestoreHealthTarget = Mathf.Lerp (restoreHealthTarget.x, restoreHealthTarget.y, durability);

		if (currentHealth <= currentRestoreHealthTarget && canRestoreHealth) {
			currentHealth = Mathf.MoveTowards (currentHealth, currentRestoreHealthTarget, currentRestoreHealthMaxDelta);
		}
	}

	// flickers the spaceship and its components to make it invincible
	private IEnumerator makeInvincible() {
		isInvincible = true;

		float elapsed=0;
		const float wait = 0.05f;
		while (elapsed < 1.75f) {

			yield return new WaitForSeconds(wait);
			elapsed += wait;

			spriteRenderer.enabled = !spriteRenderer.enabled;
			enginePS.gameObject.SetActive (!enginePS.gameObject.activeSelf);
		}

		// now enable the asteroid collisions
		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer("Asteroid"), LayerMask.NameToLayer("Spaceship"), false);
		
		enginePS.gameObject.SetActive (true);
		spriteRenderer.enabled = true;

		isInvincible = false;
	}

	Vector3[] p;
	void OnDrawGizmos() {

		if (cachedTrans == null) cachedTrans = transform;

		if (p != null) {
			for (int i=0; i<p.Length; i++) {
				Gizmos.DrawSphere (new Vector3(p[i].x, p[i].y, 0f), 0.1f);
			}
		}

		Gizmos.color = Color.red;
		Gizmos.DrawLine (cachedTrans.position, cachedTrans.position + new Vector3(collisionTangent.x, collisionTangent.y, 0f) * 5f);

		if (r2d != null) {
			Vector2 vel = r2d.velocity.normalized * 5f;
			Gizmos.color = Color.green;
			Gizmos.DrawLine (cachedTrans.position, cachedTrans.position + new Vector3(vel.x, vel.y, 0f));
		}
	}

	private void updateMenuMode() {

	}

	private void updateLostMode() {

	}

	private void checkForDeath() {

		// here we die, play the effect and destroy the spaceship
		if (!IsAlive && !isHandlingDeath) {

			isHandlingDeath = true;
			r2d.velocity = Vector2.zero;
			if (enginePS != null) {
				enginePS.Stop (true);
				enginePS.Clear();
			}

			if (laserFeedbackPS != null) {
				laserFeedbackPS.Stop (true);
				laserFeedbackPS.Clear();
			}

			foreach (Collider2D c in mainColliders) {
				c.enabled = false;
			}

			magnetCollider.enabled = false;

			dieEffect();
			spriteRenderer.enabled = false;

			postEvent (new CECameraShake (Random.Range(0.6f, 0.8f), Random.Range(-3f, 3f), Random.Range(0.7f, 1f)));
			postEvent (new CEDoSound (DoSoundAction.Play, "ShipExplode"));

			if (lastMagnetFxInstance != null) {
				lastMagnetFxInstance.CachedParticleSystem.Stop();
			}

			if (lastWarpdriveFxInstance != null) {
				lastWarpdriveFxInstance.CachedTransform.Find ("Trail").GetComponent<ParticleSystem>().simulationSpace = ParticleSystemSimulationSpace.Local;
				lastWarpdriveFxInstance.CachedParticleSystem.Stop();
			}

			enableCollisions();

			StartCoroutine (waitForRetryPopup());
		}
	}

	private IEnumerator waitForRetryPopup() {
		yield return new WaitForSeconds(0.65f);

		postEvent (new CEPushPopup (typeof(UIRetryPopup)));
	}

	private void dieEffect() {
		PooledInstance inst = ObjectPoolManager.Instance.Spawn ("GenericPool", "died_explosion");
		if (inst != null) {
			inst.CachedTransform.position = cachedTrans.position;
		}

		postEvent (new CEDied());
	}

	public Vector2 Velocity {
		get; private set;
	}

	// collision with geometries
	void OnCollisionEnter2D (Collision2D other) {

		handleCollisions (other);
	}

	void OnCollisionStay2D (Collision2D other) {
		handleCollisions (other);
	}

	// if we are in the spaceship tileset then we completely ignore head-on collisions.
	// if we are in the asteroid level, we use the collisionTolerance to detect collisions with asteroids.
	// to check, we check tha tag of the "other" collider.
	private void handleCollisions (Collision2D other, bool immediate=false) {

		// ignore collision response with the main camera colliders
		// this is to avoid the spaceship go out of screen bounds.
		if ((other.collider != null) && (other.collider.tag == TagStringManager.MainCamera)) return;
		//

		ContactPoint2D p = other.contacts[0];

		float d = Vector2.Dot (p.normal, cachedTrans.right);

		// push asteroids away if we are in warpdrive or invincible mode!
		if (other.collider.tag == TagStringManager.AsteroidField) {
			Rigidbody2D body2d = other.collider.GetComponent<Rigidbody2D>();

			Vector2 dir = -p.normal;
			if (Vector2.Dot (dir, Vector2.right) > 0.5f) {
				dir += Vector2.up * 0.75f;
				dir.Normalize();
			}

			body2d.AddForce (dir * Velocity.magnitude);
		}

		// critical collision, die.
		if ((d <= collisionTolerance) && (other.collider.tag == TagStringManager.AsteroidField)) {

			if (!saveData.NeedsTutorial && !isInvincible && (warpdriveTimeLeft <= 0)) {

				currentHealth = 0f;
				checkForDeath();
			}
		}
		// non critical collision, but reduce health and show sparks from normal
		else {

			// calculate collision tangent
			// we will rotate the velocity based on this.
			if ( (p.normal.y > 0f && p.normal.x < 0f) || (p.normal.y < 0f && p.normal.x < 0f)) {

				float angleOffset = 0;
				float angle = Mathf.Deg2Rad * (-90f+angleOffset) * Mathf.Sign(p.normal.y);
				float cs = Mathf.Cos(angle);
				float sn = Mathf.Sin(angle);

				collisionTangent = new Vector2(p.normal.x * cs - p.normal.y * sn, p.normal.x * sn + p.normal.y * cs);
				//
			}

			if (warpdriveTimeLeft <= 0 && currentHealth > 0) {
				if (hullSfxElapsed >= hullSfxInterval || immediate) {
				    postEvent (new CEDoSound(DoSoundAction.Play, "HullHit"));

					hullSfxElapsed = 0f;
				}
			}

			postEvent (new CECameraShake (Random.Range(0.15f, 0.3f), Random.Range(-2f, 2f), Random.Range(0.3f, 0.45f)));

			//
			// TODO: engine bug here, point and normal not updated during Stay2D !!
			if ((sparksElapsed >= sparksInterval) || immediate) {

				if (!saveData.NeedsTutorial && !isInvincible && (warpdriveTimeLeft <= 0)) {

					StopCoroutine ("waitForRestoreHealth");
					StartCoroutine ("waitForRestoreHealth");

					currentHealth -= ((1f/ (durability)) * durabilityFactor);
					currentHealth = Mathf.Clamp01(currentHealth);
				}

				PooledInstance inst = ObjectPoolManager.Instance.Spawn ("GenericPool", "collision_sparks");
				if (inst != null) {
					GameObject go = inst.CachedGameObject;


					Quaternion rot = Quaternion.LookRotation (p.normal) * Quaternion.Euler (0, -90, 0);

					inst.CachedTransform.parent = cachedTrans;
					inst.CachedTransform.localPosition = cachedTrans.InverseTransformPoint(new Vector3(p.point.x - 1f, p.point.y, 0f));

					//inst.CachedTransform.rotation = rot;

					inst.CachedParticleSystem.Play(true);
				}

				sparksElapsed = 0f;
			}

		}

	}

	// triggers for powerups and so on.
	void OnTriggerEnter2D (Collider2D other) {

	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();
	}
}
