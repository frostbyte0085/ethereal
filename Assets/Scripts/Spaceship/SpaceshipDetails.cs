using System.Collections;
using UnityEngine;

public enum SpaceshipType {
	Tier1,
	Tier2,
	Tier3,
	MaxTier
}

public enum UnlockType {
	Unlocked,
	Locked,
	Unavailable
}

public class SpaceshipDetails : ScriptableObject {

	public SpaceshipType type;

	// general
	public string shipName = "Destroyer";
	public string shipDescription = "trololo";

	// stats
	public float agility = 0.5f;
	public float durability = 0.5f;
	public float powerup = 0.5f;

	// unlock mode
	public UnlockType unlock = UnlockType.Unlocked;
	public int crystalsNeeded = 5000;

	// upgrade prices
	public int[] statUpgradePrices;

	// Identifier for GA
	public float ga_id = 0f;
}