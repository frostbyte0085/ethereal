﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// makes the obejcts found under this hierarchy to follow the camera X while scrolling them with a fixed speed.
public class LayerScroll : MessageBehaviour {

	public bool isExtraLayer = false;

	public float relativeScrollSpeed=-0.2f;
	public float constantScrollSpeed=0;

	private SpriteRenderer[] layerRenderers;

	private Transform cachedTrans;

	private CESpaceshipStatus lastSpaceshipStatus;
	private CECameraStatus lastCameraStatus;

	#region event listeners
	private void spaceshipStatusEventListener(ConvexEvent evt) {
		CESpaceshipStatus e = (CESpaceshipStatus)evt;

		lastSpaceshipStatus = e;
	}

	private void gamePhaseEventListener(ConvexEvent evt) {
		CEGamePhase e = (CEGamePhase)evt;

		enabled = (e.Mode == GamePhaseMode.Gameplay);
	}

	private void cameraStatusEventListener(ConvexEvent evt) {
		CECameraStatus e = (CECameraStatus)evt;

		lastCameraStatus = e;
	}
	#endregion

	void Awake() {
		cachedTrans = transform;

		layerRenderers = GetComponentsInChildren<SpriteRenderer>(true);

		registerEventListener (typeof(CESpaceshipStatus), spaceshipStatusEventListener);
		registerEventListener (typeof(CEGamePhase), gamePhaseEventListener);
		registerEventListener (typeof(CECameraStatus), cameraStatusEventListener);

		if (!QualityControlManager.CurrentSetting.extraStarLayer && isExtraLayer) {
			Destroy (gameObject);
		}
	}

	// Update is called once per frame
	void LateUpdate () {
		//if (lastCameraStatus == null)
		//	return;

		Vector3 camPos = Camera.main.transform.position;

		Vector3 pos = cachedTrans.position;
		pos.x = camPos.x;
		cachedTrans.position = pos;

		// scroll the layers
		for (int i=0; i<layerRenderers.Length; i++) {
			Transform t = layerRenderers[i].transform;

			if (lastCameraStatus != null) {
				t.localPosition += Vector3.right * (relativeScrollSpeed * lastCameraStatus.DeltaPos.x + constantScrollSpeed) * Time.deltaTime;
			}

			if (t.localPosition.x <= -Constants.tileScreenSize*2f) {

				t.localPosition += Vector3.right * Constants.tileScreenSize * layerRenderers.Length;
			}
		}
	}
}
