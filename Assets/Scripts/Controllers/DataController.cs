using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

[System.Serializable]
public class ShipSaveData {

	public ShipSaveData() {
		statBonus = new int[(int)StatType.MaxStat];
	}

	public bool isUnlocked;
	public int[] statBonus; // max stat up to 3.
}

[System.Serializable]
public class SaveData {

	public SaveData() {
	
		isFirstRun = true;
		needsTutorial = true;
		needsSocialTutorial = false;
		music = sfx = menuSfx = true;
		numRuns = 0;
		controlScheme = 0;


		shipData = new ShipSaveData[(int)SpaceshipType.MaxTier];
		for (int i=0; i<shipData.Length; i++) {
			shipData[i] = new ShipSaveData();
		}
	}

	public ShipSaveData[] shipData;
	public bool isFirstRun;
	public int numRuns; // gameplay runs
	public bool needsTutorial;
	public bool needsSocialTutorial;

	public bool music;
	public bool sfx;
	public bool menuSfx;
	public int controlScheme; // 0: touch, 1: controls
}

public class DataController : MessageBehaviour {

	private const string saveKey="savedata";

	#region event listeners
	private void doDataEventListener(ConvexEvent evt) {
		CEDoData e = (CEDoData)evt;

		if (e.Type == DoDataType.Save) {
			save ();
		}
		else if (e.Type == DoDataType.Load) {
			load ();
		}

	}

	private void socialTutorialCompleteEventListener (ConvexEvent evt) {
		data.needsSocialTutorial = false;
	}

	private void finishedRunEventListener (ConvexEvent evt) {
		CEFinishedRun e = (CEFinishedRun)evt;

		data.numRuns++;
		if (data.numRuns == 1) {
			data.needsSocialTutorial = true;
		}

	}

	private void crystalCollectEventListener(ConvexEvent evt) {
		CECrystalCollect e = (CECrystalCollect)evt;

		int multiplier = (Unibiller.GetPurchaseCount("double_crystals")==1) ? 2 : 1;

		uint delta=0;
		if (e.Type == CrystalType.Normal)
			delta = Constants.normalCrystal;
		else if (e.Type == CrystalType.Large)
			delta = Constants.bigCrystal;

		RunInstance.CrystalsCollected += (delta * (uint)multiplier);
	}

	private void changeControlSchemeEventListener (ConvexEvent evt) {
		CEControlSchemeChanged e = (CEControlSchemeChanged)evt;

		data.controlScheme = (int)e.Scheme;
	}

	private void soundsEventListener (ConvexEvent evt) {
		CEDoSound e = (CEDoSound)evt;

		bool muted = true;
		if (e.Action == DoSoundAction.Mute) muted = true;
		else if (e.Action == DoSoundAction.Unmute) muted = false;
		else return;

		if (e.Name == "Music") {
			data.music = !muted;

			Debug.Log (data.music);
		}
		else if (e.Name == "Sfx") {
			data.sfx = !muted;
		}
		else if (e.Name == "MenuSfx") {
			data.menuSfx = !muted;
		}
	}

	private void spaceshipUnlockEventListener(ConvexEvent evt) {
		CESpaceshipUnlock e = (CESpaceshipUnlock)evt;

		if (e.Type != SpaceshipType.MaxTier) {

			data.shipData[(int)e.Type].isUnlocked = true;
		}

		save ();
	}
	
	private void upgradeStatEventListener(ConvexEvent evt) {
		CEUpgradeStat e = (CEUpgradeStat)evt;

		if ((e.ShipType != SpaceshipType.MaxTier) && (e.Stat != StatType.MaxStat)) {

			ShipSaveData ship = data.shipData[(int)e.ShipType];
			int bonus = ship.statBonus[(int)e.Stat];
			if (bonus < 3) {
				bonus++;

				ship.statBonus[(int)e.Stat] = bonus;
			}
		}

		save ();
	}

	private void saveEventListener(ConvexEvent evt) {

		save ();
	}

	private void needsTutorialEventListener (ConvexEvent evt) {
		CENeedsTutorial e = (CENeedsTutorial)evt;

		data.needsTutorial = e.Tutorial;

		save ();
	}
	#endregion
	
	private SaveData data;

	private void initSoundStates(string name, bool state) {
		if (state) {
			postEvent (new CEDoSound(DoSoundAction.Unmute, name));
		}
		else {
			postEvent (new CEDoSound(DoSoundAction.Mute, name));
		}
	}

	void Awake() {
		data = new SaveData();

		registerEventListener (typeof(CEDoData), doDataEventListener);
		registerEventListener (typeof(CECrystalCollect), crystalCollectEventListener);
		registerEventListener (typeof(CESpaceshipUnlock), spaceshipUnlockEventListener);
		registerEventListener (typeof(CESave), saveEventListener);
		registerEventListener (typeof(CEUpgradeStat), upgradeStatEventListener);
		registerEventListener (typeof(CENeedsTutorial), needsTutorialEventListener);
		registerEventListener (typeof(CEFinishedRun), finishedRunEventListener);
		registerEventListener (typeof(CESocialTutorialComplete), socialTutorialCompleteEventListener);
		registerEventListener (typeof(CEDoSound), soundsEventListener);
		registerEventListener (typeof(CEControlSchemeChanged), changeControlSchemeEventListener);
	}

	private void save() {
		Debug.Log ("Saving...");
		
		string encrypted = serialize(data).Encrypt();
		PlayerPrefs.SetString (saveKey, encrypted);

		Debug.Log ("Saved");
	}

	// serialize a Data structure to xml formatted string
	private string serialize(SaveData data) {
		
		StringWriter sw = new StringWriter();
		XmlWriter writer = XmlWriter.Create (sw);
		
		XmlSerializer xml = new XmlSerializer(data.GetType());
		xml.Serialize(writer, data);
		
		return sw.ToString();
	}

	// load all data
	private void load() {
		Debug.Log ("Loading...");
		// read from controller
		string s = PlayerPrefs.GetString (saveKey, "");

		data = new SaveData();
		
		// deserialize data, always check if our string is valid
		if (!string.IsNullOrEmpty(s)) {
						
			s = s.Decrypt();

			XmlSerializer xml = new XmlSerializer(data.GetType());
			StringReader sr = new StringReader(s);
			
			XmlReader reader = XmlReader.Create (sr);
			
			data = (SaveData)xml.Deserialize(reader);

			Debug.Log ("Data loaded");
		}
		else {
			Debug.Log ("Loaded defaults");
		}

		postSaveData();

		initSoundStates ("Music", data.music);
		initSoundStates ("Sfx", data.sfx);
		initSoundStates ("MenuSfx", data.menuSfx);

		postEvent (new CEControlSchemeChanged((ControlScheme)data.controlScheme));

		save ();
	}

	// every frame, inform of the listeners for the current save data
	void Update() {
		postSaveData();
	}

	private void postSaveData() {
		postEvent (new CECurrentSaveData (data.shipData, data.needsTutorial, data.needsSocialTutorial, data.numRuns, data.isFirstRun,
		                                  data.music, data.sfx, data.menuSfx, data.controlScheme) );
	}

	private void reset() {
		PlayerPrefs.DeleteAll();
		data = new SaveData();

		// debug only - have an ifdef?
		Unibiller.clearTransactions();
	}
	
	void OnDestroy() {
	
	}

	void OnApplicationPause (bool paused) {
		// save on paused
		if (paused) {
			data.isFirstRun = false;

			save();
		}
	}

	void OnApplicationQuit() {
		data.isFirstRun = false;

		save ();
	}

}