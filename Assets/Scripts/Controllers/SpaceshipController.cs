﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpaceshipSpec {

	public SpaceshipSpec() {
	}


}

// handles the spaceships...
public class SpaceshipController : MessageBehaviour {

	// these curves define the spaceship behaviour for powerup and its stats.
	// they are interpolated based on the normalised spaceship stats.
	public AnimationCurve durabilityCurve;

	// magnet
	public AnimationCurve magnetDuration;
	public AnimationCurve magnetStrength;
	// warpdrive
	public AnimationCurve warpdriveDuration;
	public AnimationCurve warpdriveSpeed;
	// repair
	public AnimationCurve repairEffectiveness;
	// agility
	public AnimationCurve speedCurve; // how fast the ship can move up and down, e.g how delta touch translates to speed
	public AnimationCurve speedInterpolationTimeCurve; // how fast the smoothdamp will be

	private Object[] availableSpaceships;

	private Spaceship currentShip;

	private float currentDistance;
	private float currentTime;

	private const float lostWaitTime = 2f;
	private float lostWaitElapsed = 0f;

	private GamePhaseMode lastGameMode = GamePhaseMode.Invalid;
	private CECurrentSaveData saveData;

	private int currentIndex;
	#region spaceship selection
	private void nextSpaceship() {
		currentIndex++;
		currentIndex = currentIndex % availableSpaceships.Length;
	}
	
	private void previousSpaceship() {
		currentIndex--;
		if (currentIndex < 0)
			currentIndex = availableSpaceships.Length-1;
	}

	// enumerate all spaceships and set the index to default.
	private void defaultSpaceship() {
		availableSpaceships = Resources.LoadAll (ResourceStringManager.ShipsPath);

		currentIndex = 0;
	}

	private void loadCurrentShip() {
		// destroy the old
		if (currentShip != null) {
			Destroy (currentShip.gameObject);
		}

		// bring in the new!
		currentShip = (Instantiate (availableSpaceships[currentIndex]) as GameObject).GetComponent<Spaceship>();

		// setup the current ship variables
		SpaceshipDetails details = currentShip.details;

		// get the bonus from the stat upgrades
		float agilityBonus = saveData.GetStatBonus (details.type, StatType.Agility);
		float durabilityBonus = saveData.GetStatBonus (details.type, StatType.Durability);
		float powerupBonus = saveData.GetStatBonus (details.type, StatType.Powerup);

		float durability = durabilityCurve.Evaluate (Mathf.Clamp01(details.durability + durabilityBonus));

		float magnet_duration = magnetDuration.Evaluate (Mathf.Clamp01(details.powerup + powerupBonus));
		float magnet_strength = magnetStrength.Evaluate (Mathf.Clamp01(details.powerup + powerupBonus));

		float warpdrive_duration = warpdriveDuration.Evaluate (Mathf.Clamp01(details.powerup + powerupBonus));
		float warpdrive_speed = warpdriveSpeed.Evaluate (Mathf.Clamp01(details.powerup + powerupBonus));

		float repair_effectiveness = repairEffectiveness.Evaluate (Mathf.Clamp01(details.powerup + powerupBonus));

		float speed = speedCurve.Evaluate (Mathf.Clamp01(details.agility + agilityBonus));
		float speedInterp = speedInterpolationTimeCurve.Evaluate (Mathf.Clamp01(details.agility + agilityBonus));
		currentShip.Prepare (speed, durability, magnet_duration, magnet_strength, warpdrive_duration, warpdrive_speed, repair_effectiveness, speedInterp);
		//


		// fire the gameplay event only for this ship again
		// this is because this ship didn't receive it originally, since there was no ship in the first place
		postEvent (new CEGamePhase(GamePhaseMode.PreGameplay), currentShip);

		// and the newly selection
		postEvent (new CESpaceshipSelected(currentShip));
		
		currentDistance = 0f;
		currentTime = 0f;
	}
	#endregion

	#region event listeners

	private void saveDataEventListener(ConvexEvent evt) {
		saveData = (CECurrentSaveData)evt;
	}

	private void gamePhaseEventListener(ConvexEvent evt) {
		CEGamePhase e = (CEGamePhase)evt;

		if (lastGameMode != e.Mode) {
			switch (e.Mode) {
			case GamePhaseMode.PreGameplay:
				loadCurrentShip ();
				break;

			case GamePhaseMode.Lose:
				lostWaitElapsed = 0f;
				break;
			}
		}

		lastGameMode = e.Mode;
	}

	private void diedEventListener(ConvexEvent evt) {
		CEDied e = (CEDied)evt;

		//postEvent (new CEDifficultyUpdate(0, 0));
		postEvent (new CEGamePhase (GamePhaseMode.Lose));
	}

	private void changeShipEventListener(ConvexEvent evt) {
		CEChangeShip e = (CEChangeShip)evt;
	
		if (!string.IsNullOrEmpty(e.ShipName)) {

			// simply update the index
			for (int i=0; i<availableSpaceships.Length; i++) {

				if (e.ShipName == (availableSpaceships[i].name + "_menu")) {
					currentIndex = i;
					break;
				}
			}
		}
		else {
			if (e.Direction == ChangeShipDirection.Next) {
				nextSpaceship();
			}
			else if (e.Direction == ChangeShipDirection.Previous) {
				previousSpaceship();
			}
			else if (e.Direction == ChangeShipDirection.Default) {
				defaultSpaceship();
			}

			string menuShipName = availableSpaceships[currentIndex].name + "_menu";

			e.AddResponse (new CRChangedShip(menuShipName, new SpaceshipSpec()));
		}
	}
	
	#endregion

	void Awake() {
		registerEventListener (typeof(CEGamePhase), gamePhaseEventListener);
		registerEventListener (typeof(CEDied), diedEventListener);
		registerEventListener (typeof(CEChangeShip), changeShipEventListener);
		registerEventListener (typeof(CECurrentSaveData), saveDataEventListener);
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		switch (lastGameMode) {
		case GamePhaseMode.Gameplay:
			updateGameplayMode();
			break;

		case GamePhaseMode.Lose:
			updateLostMode();
			break;
		}
	}

	private void updateGameplayMode() {

		// if we are in the tutorial mode, ignore this
		if (saveData != null && saveData.NeedsTutorial) {
			return;
		}

		float delta = (currentShip.Velocity.x/1000f) * Time.timeScale;
		currentDistance += delta;
		currentTime += Time.deltaTime;

		RunInstance.DistanceTravelled = (uint)currentDistance;
	}

	// here we wait a bit until the explosion is finished, or until the player taps on the screen
	// after that, we show the run stats and allow the player to restart the game in an elegant way.
	private void updateLostMode() {
		lostWaitElapsed += Time.deltaTime;

		if (lostWaitElapsed >= lostWaitTime || Input.GetMouseButtonDown(0)) {

			//postEvent (new CEGameRestart(GameRestartPhase.Start));
			//postEvent (new CEFade (FadeMode.OutStarted));

			lostWaitElapsed = 0;
		}
	}

}
