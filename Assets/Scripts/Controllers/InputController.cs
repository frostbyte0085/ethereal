﻿using UnityEngine;
using System.Collections;

public static class InputState {
	public static ControlScheme currentScheme;
}

public class InputData {
	public float axis;
	public Vector3 targetPosition;
	public bool translating;
	public bool touching;
}

public enum ControlScheme {
	Direct,
	Tilt
}

public class InputController : MessageBehaviour {

	private bool inputEnabled;

	private CESpaceshipStatus lastSpaceshipStatus;

	private const float screenBound = 50f;
	private const float tiltSensitivity = 11f;
	private InputData id;

	private Vector3 prevPos;
	private Vector3 pos;

	private InputMode currentInputMode;

	private Vector3 prevTouchPos;
	private Vector3 currentTouchPos;
	private Vector3 deltaTouchPos;
	private float totalTutorialMovement;

	private ControlScheme scheme;

	private float startTilt;

	#region event listeners
	private void warpdriveCollectEventListener(ConvexEvent evt) {
		CEWarpdriveCollect e = (CEWarpdriveCollect)evt;

		if (e.Phase == WarpdrivePhase.Start) {
			inputEnabled = false;
		}
		else if (e.Phase == WarpdrivePhase.End) {
			inputEnabled = true;
			reset ();
		}
	}

	private void calibrateEventListener (ConvexEvent evt) {
		startTilt = Input.acceleration.y;
	}

	private void inputModeEventListener(ConvexEvent evt) {
		CEInputMode e = (CEInputMode)evt;

		currentInputMode = e.Mode;
	}

	private void inputStateEventListener(ConvexEvent evt) {
		CEInputState e = (CEInputState)evt;

		inputEnabled = e.IsEnabled;
	}

	private void spaceshipStatusEventListener(ConvexEvent evt) {
		CESpaceshipStatus e = (CESpaceshipStatus)evt;

		lastSpaceshipStatus = e;
	}

	private void gamePhaseEventListener(ConvexEvent evt) {
		CEGamePhase e = (CEGamePhase)evt;

		inputEnabled = (e.Mode == GamePhaseMode.Gameplay);
	}

	private void controlSchemeChangedEventListener (ConvexEvent evt) {
		CEControlSchemeChanged e = (CEControlSchemeChanged)evt;

		scheme = e.Scheme;

		InputState.currentScheme = scheme;
	}
	#endregion

	void Awake() {
		inputEnabled = false;

		registerEventListener (typeof(CECalibrateAccelerometer), calibrateEventListener);
		registerEventListener (typeof(CEWarpdriveCollect), warpdriveCollectEventListener);
		registerEventListener (typeof(CEGamePhase), gamePhaseEventListener);
		registerEventListener (typeof(CESpaceshipStatus), spaceshipStatusEventListener);
		registerEventListener (typeof(CEInputState), inputStateEventListener);
		registerEventListener (typeof(CEInputMode), inputModeEventListener);
		registerEventListener (typeof(CEControlSchemeChanged), controlSchemeChangedEventListener);

		id = new InputData();
	}
	
	// Update is called once per frame
	void Update () {
		if (lastSpaceshipStatus == null) {
			return;
		}

		if (currentInputMode != InputMode.Disabled) {

			if (inputEnabled) {
		#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
				update_Touch();
		#else
				update_Editor();
		#endif
			}
			else {
				reset();
			}

			// if we're in the tutorial mode, wait until the player has moved the spaceship around enough.
			if (currentInputMode == InputMode.Tutorial) {
				// check the delta on Y to see how much we moved overall
				totalTutorialMovement += Mathf.Abs(deltaTouchPos.y);// * ((float)Screen.height / 768f);

				if (totalTutorialMovement >= 650) {
					// force the ship back to the centre of the screen
					id.targetPosition = Camera.main.ScreenToWorldPoint (new Vector3 (0f, Screen.height/2f, 0f));
					id.translating = true;
					id.axis = 0f;

					postEvent (new CETutorialInputSuccess());
				}
			}
			else {
				id.translating = false;
			}
		}

		if (!updatingAxis) {
			id.axis = 0f;//Mathf.Lerp(id.axis, 0f, Time.deltaTime*3);
		}

		CESpaceshipInput e = new CESpaceshipInput (id);

		if (e != null) {
			postEvent (e);
		}
	}

	private bool canDrag = false;
	private bool checkedFrame = false;

#if (UNITY_IPHONE || UNITY_ANDROID)
	private Vector3 touchCenter;

	private void update_Touch() {

		if (scheme == ControlScheme.Tilt) {
			updateTilt();
		}
		else if (scheme == ControlScheme.Direct) {
			updateDirectTouch();
		}

	}

	private void updateTilt() {

		id.axis = (Input.acceleration.y - startTilt) * tiltSensitivity;

	}

	private void updateDirectTouch() {

		if (Input.touchCount > 0) {
			Touch t0 = Input.GetTouch(0);

			if (t0.phase == TouchPhase.Began) {
				id.touching = true;
				checkedFrame = false;
			}
			else if (t0.phase == TouchPhase.Moved || t0.phase == TouchPhase.Moved || t0.phase == TouchPhase.Stationary) {
				id.targetPosition = Camera.main.ScreenToWorldPoint (new Vector3 (t0.position.x, t0.position.y, 0f));

				doDragCheck();
				
				id.targetPosition.z = getSpeedAndPosition();
				id.axis = getAxis (id.axis, t0.deltaPosition.y);
				
				deltaTouchPos = t0.deltaPosition;
			}
			else if (t0.phase == TouchPhase.Ended || t0.phase == TouchPhase.Canceled) {
				reset ();
			}
			
		}

	}
#endif


	private void updateButtonsEditor() {
		if (Input.GetMouseButton (0)) {
			// sign 1 means moving up, 0 means moving down
			int sign = (Input.mousePosition.x < Screen.width/2) ? 1 : 0;

			id.targetPosition = Camera.main.ScreenToWorldPoint (new Vector3(0, Screen.height * sign, 0));
			id.targetPosition.z = getSpeedAndPosition();
		}
		else if (Input.GetMouseButtonUp(0)) {
			reset ();
		}
	}

	private bool updatingAxis;
	private void updateDirectEditor() {
		if (Input.GetMouseButtonDown(0)) {
			checkedFrame = false;
			currentTouchPos = prevTouchPos = Input.mousePosition;
			id.touching = true;
		}
		else if (Input.GetMouseButton (0)) {
			
			prevTouchPos = currentTouchPos;
			currentTouchPos = Input.mousePosition;
			id.targetPosition = Camera.main.ScreenToWorldPoint (new Vector3 (currentTouchPos.x, currentTouchPos.y, 0f));
			id.axis = getAxis (id.axis, (currentTouchPos - prevTouchPos).y);

			doDragCheck();
			
			deltaTouchPos = (currentTouchPos - prevTouchPos);
			
			id.targetPosition.z = getSpeedAndPosition();
		}
		else if (Input.GetMouseButtonUp(0)) {
			reset();

			deltaTouchPos = Vector3.zero;
		}
	}

	private void update_Editor() {

		if (scheme == ControlScheme.Tilt) {
			updateButtonsEditor();
		}
		else if (scheme == ControlScheme.Direct) {
			updateDirectEditor();
		}
	}

	private float v;
	private float getAxis(float currentAxis, float delta) {
		// relative to 6/6s
		float ratio = 750.0f / (float)Screen.height;

		delta = (delta * 0.5f) * ratio;

		updatingAxis = true;
		//return Mathf.SmoothDamp (currentAxis, delta, ref v, 0.1f);
		return delta;
	}

	private float getSpeedAndPosition() {

		float speed = 0f;
		if (scheme == ControlScheme.Tilt) {
			speed = 0.5f;
		}
		else if (scheme == ControlScheme.Direct) {
			if (canDrag) return 1f;

			speed = Mathf.Lerp (0.35f, 1f, 1-Mathf2.SmoothStep(0, Constants.tileScreenSize/4, 
			                                                   Mathf.Abs(lastSpaceshipStatus.ShipTransform.position.y - id.targetPosition.y)));
		}

		return speed;
	}

	private void doDragCheck() {
		if (!checkedFrame) {
			canDrag = Mathf.Abs (lastSpaceshipStatus.ShipTransform.position.y - id.targetPosition.y) < 1f;
			if (canDrag) {
				checkedFrame = true;
			}
		}
	}
	
	private void reset() {
		id.touching = false;
		id.targetPosition = lastSpaceshipStatus.ShipTransform.position;
		id.targetPosition.z = 1;
		updatingAxis = false;
	}
}
