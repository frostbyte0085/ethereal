﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public static class LocalNotificationState {
	public static bool mustShowRewardPopup;
	public static int crystals;
	public static int repairKits;

	public static void Clear() {
		crystals = 0;
		repairKits = 0;
		mustShowRewardPopup = false;
	}
}

public class LocalNotificationController : MessageBehaviour {

	private LocalNotifications_Backend backend;
	private List<LocalNotificationDescription> notifications;
	private List<HolidayCalculator.Holiday> holidays;

	// Use this for initialization
	void Awake () {

		populateNotifications();

		#if UNITY_EDITOR
		backend = new LocalNotifications_Dummy();
		#else
		#if UNITY_IPHONE
		backend = new LocalNotifications_IOS();
		#elif UNITY_ANDROID
		backend = new LocalNotifications_Android();
		#endif
		#endif

		// this is called here because OnApplicationPause is not triggered
		// when the app starts fresh.
		enterGame();
	}

	// TODO: test when there are multiple notifications.
	private void enterGame() {
		// check for rewards here
		// if there are notifications available,
		// get the first one and check the userData dictionary for rewards and other things.
		if (backend.NotificationCount > 0) {
			IDictionary dict = backend.GetUserData (backend.NotificationCount-1);

			if (dict != null) {
				
				if (dict.Contains ("crystals") && dict.Contains("repair_kits")) {
					int crystals = int.Parse(dict["crystals"].ToString());
					int repairKits = int.Parse(dict["repair_kits"].ToString());

					LocalNotificationState.mustShowRewardPopup = true;
					LocalNotificationState.crystals = crystals;
					LocalNotificationState.repairKits = repairKits;
				}
			}
		}
		
		backend.CancelAll();
	}

	// Update is called once per frame
	void Update () {
		backend.Update();
	}

	// handle the app pause/resume functionality.
	// 1) when we pause, we re-schedule all the notifications
	// 2) when we resume, we check for rewards
	void OnApplicationPause(bool paused) {
		if (paused) {			
			scheduleAll();
		}
		else {
			// entering the game
			enterGame();
		}
	}

	void OnApplicationQuit() {
		rememberLastPlayedDate();
		scheduleAll();
	}

	private DateTime? getLastPlayedDate() {
		
		string lastDateStr = PlayerPrefs.GetString ("LastDate");
		
		if (!string.IsNullOrEmpty(lastDateStr)) {
			DateTime lastDate = DateTime.Parse (lastDateStr);
			return lastDate;
		}
		
		return null;
	}
	
	private void rememberLastPlayedDate() {
		PlayerPrefs.SetString ("LastDate", DateTime.Now.ToString());
	}

	private void populateNotifications() {
		notifications = new List<LocalNotificationDescription>();

		// setup the holidays
		TextAsset holidayXml = Resources.Load<TextAsset>("Holidays");
		HolidayCalculator calc = new HolidayCalculator(DateTime.Now, holidayXml.text);
		holidays = new List<HolidayCalculator.Holiday>();
		for (int i=0; i<calc.OrderedHolidays.Count; i++) {
			holidays.Add ((HolidayCalculator.Holiday)calc.OrderedHolidays[i]);
		}

		// absolute, mostly holidays
		for (int i=0; i<holidays.Count; i++) {
			HolidayCalculator.Holiday h = holidays[i];
			notifications.Add (new LocalNotificationDescription (new string[] {h.Message}, getRandomCrystals(), getRandomRepairKits(), h.Date, 0, true));
		}

		//

		// relative
		//notifications.Add (new LocalNotificationDescription (makeMessages("CLICK ME!!!"), getRandomCrystals(), getRandomRepairKits(), makeDate(0,0,0,0,15), 0, false));

		string str1 = "Hey, it's time to do some interstellar travel!";
		string str2 = "We've discovered some crystals, come and claim them!";
		string str3 = "Your spaceship awaits, come and enjoy the ride!";
		string str4 = "Autopilot will only get us so far! Come take the wheel!";

		notifications.Add (new LocalNotificationDescription (makeMessages(str1, str2, str3, str4), getRandomCrystals(), getRandomRepairKits(), makeDate(0,2,0,0,0), 2, false));
		notifications.Add (new LocalNotificationDescription (makeMessages(str1, str2, str3, str4), getRandomCrystals(), getRandomRepairKits(), makeDate(0,5,0,0,0), 2, false));
		notifications.Add (new LocalNotificationDescription (makeMessages(str1, str2, str3, str4), getRandomCrystals(), getRandomRepairKits(), makeDate(0,7,0,0,0), 2, false));
		notifications.Add (new LocalNotificationDescription (makeMessages(str1, str2, str3, str4), getRandomCrystals(), getRandomRepairKits(), makeDate(0,15,0,0,0), 2, false));
		notifications.Add (new LocalNotificationDescription (makeMessages(str1, str2, str3, str4), getRandomCrystals(), getRandomRepairKits(), makeDate(1,0,0,0,0), 2, false));
		//
	}

	private int getRandomCrystals() {
		return UnityEngine.Random.Range (30, 101);
	}

	private int getRandomRepairKits() {
		return UnityEngine.Random.Range (1, 3);
	}

	private DateTime makeDate (int months, double days, double hours, double minutes, double seconds) {
		DateTime d = new DateTime ();

		d = d.AddMonths (months);
		d = d.AddDays (days);
		d = d.AddHours (hours);
		d = d.AddMinutes (minutes);
		d = d.AddSeconds (seconds);

		return d;
	}

	private string[] makeMessages (string msg, params string[] p) {
		List<string> ret = new List<string>();
		ret.Add (msg);

		if (p != null) {
			ret.AddRange (p);
		}

		return ret.ToArray();
	}

	// reschedule all the notifications defined in the manifest
	private void scheduleAll() {

		for (int i=0; i<notifications.Count; i++) {
			LocalNotificationDescription n = notifications[i];
			
			// check if there is a reward, and add it to the user dictionary for
			// the notification.
			Dictionary<string,string> userDict = null;
			
			if (n.crystals > 0 || n.repairKits > 0) {
				userDict = new Dictionary<string,string>();
				userDict["crystals"] = n.crystals.ToString();
				userDict["repair_kits"] = n.repairKits.ToString();
			}
			
			// TODO: handle repeats.
			
			// pick a random message
			int randomMsgIdx = UnityEngine.Random.Range (0, n.messages.Length);
			
			
			DateTime targetDate = n.date;
			if (n.isAbsolute) {
				DateTime now = DateTime.Now;
				targetDate = targetDate.AddHours (now.Hour);
				targetDate = targetDate.AddMinutes (now.Minute);
				targetDate = targetDate.AddSeconds (now.Second);

				//if (n.messages[0].StartsWith ("Testing")) {
				//	Debug.Log (targetDate.ToString());
				//}
			}
			else {
				targetDate = DateTime.Now;
				targetDate = targetDate.AddSeconds (n.date.Second);
				targetDate = targetDate.AddMinutes (n.date.Minute);
				targetDate = targetDate.AddHours (n.date.Hour);
				targetDate = targetDate.AddDays (n.date.Day-1); // hack to fix the "min value" bug. could've used TimeSpan instead.
				targetDate = targetDate.AddMonths (n.date.Month-1); // same as above
			}
			
			// schedule
			backend.Schedule (n.messages[randomMsgIdx], targetDate, userDict);
		}
	}
}
