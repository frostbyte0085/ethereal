﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PaymentController : MessageBehaviour {

	private bool mustCreditLives;

	void Start() {

		Unibiller.onBillerReady += onInitialised;

		// setup the events for purchasing status
		Unibiller.onPurchaseCancelled += delegate(PurchasableItem obj) {
			postEvent (new CEIAPResult(IAPResult.Cancelled));
				};

		Unibiller.onPurchaseCompleteEvent += delegate(PurchaseEvent obj) {
			postEvent (new CEIAPResult(IAPResult.Success));
				};

		Unibiller.onPurchaseFailed += delegate(PurchasableItem obj) {
			postEvent (new CEIAPResult(IAPResult.Failed));
				};

		Unibiller.onTransactionsRestored += delegate(bool obj) {
			postEvent (new CEIAPResult(IAPResult.RestoredPurchases));
				};

		Unibiller.Initialise();

		registerEventListener (typeof(CECurrentSaveData), saveDataEventListener);
		registerEventListener (typeof(CEDoIAP), doIAPEventListener);
	}

	#region event listeners
	private void saveDataEventListener (ConvexEvent evt) {
		CECurrentSaveData e = (CECurrentSaveData)evt;

		mustCreditLives = e.IsFirstRun;
	}

	private void doIAPEventListener (ConvexEvent evt) {
		CEDoIAP e = (CEDoIAP)evt;

		PurchasableItem item = Unibiller.GetPurchasableItemById (e.ID);
		if (item != null) {
			Unibiller.initiatePurchase (item);
		}
	}
	#endregion
	private void onInitialised(UnibillState result) {
		StartCoroutine (waitForCredit());
	}

	private IEnumerator waitForCredit() {
		yield return null;

		if (mustCreditLives) {
			Unibiller.CreditBalance ("RepairKits", Constants.defaultRepairKits);
			Unibiller.CreditBalance ("Crystals", Constants.defaultCrystals);
			
			mustCreditLives = false;
		}
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();
	}
}
