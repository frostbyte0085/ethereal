﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MessageBehaviour {

	private TileController tiles;
	private SpaceshipController spaceship;
	private InputController input;
	private DataController data;
	private UIController ui;
	private SocialController social;
	private AdController ads;
	private SoundController sound;
	private ScoringController scoring;
	private PaymentController payments;
	private LocalNotificationController localNotifications;

	private Transform cachedTrans;

	private static bool isFirstLoad = true;


	private CECurrentSaveData saveData;

	private GamePhaseMode lastGameMode = GamePhaseMode.Invalid;

	#region event listeners
	private void gamePhaseEventListener(ConvexEvent evt) {
		CEGamePhase e = (CEGamePhase)evt;

		switch (e.Mode) {
		case GamePhaseMode.Menu:
			postEvent (new CEChangeScreen (typeof(UIMenuScreen)));

			postEvent (new CEDoSound(DoSoundAction.StopAll));
			postEvent (new CEDoSound(DoSoundAction.Play, "Menu"));
			break;

		case GamePhaseMode.Gameplay:

			// if we need the tutorial, then disable input until the tutorial enables it back again
			if (saveData.NeedsTutorial) {
				postEvent (new CEInputMode (InputMode.Disabled));
			}
			else {
				// otherwise, enable it
				postEvent (new CEInputMode (InputMode.Free));
			}
			break;

		case GamePhaseMode.PreGameplay:
			UIRetryPopup.Reset();
			mustShowAd = true;
			StartCoroutine (waitForGameplayChange());

			postEvent (new CEChangeScreen (typeof(UIGameScreen)));

			postEvent (new CEDoSound(DoSoundAction.StopAll));
			postEvent (new CEDoSound(DoSoundAction.Play, "Ingame"));
			break;

		case GamePhaseMode.Lose:
			break;

		case GamePhaseMode.Results:
			postEvent (new CEFinishedRun());
			break;
		}

		lastGameMode = e.Mode;

		// on state change, instantly save
		postEvent (new CESave());
	}

	private void saveDataEventListener(ConvexEvent evt) {
		saveData = (CECurrentSaveData)evt;
	}

	private IEnumerator waitForGameplayChange() {
		yield return new WaitForEndOfFrame();

		postEvent (new CEGamePhase(GamePhaseMode.Gameplay));
	}

	private void continueEventListener(ConvexEvent evt) {
		CEContinue e = (CEContinue)evt;

		postEvent (new CEGamePhase (GamePhaseMode.Gameplay));
	}

	private void gameRestartEventListener(ConvexEvent evt) {
		CEGameRestart e = (CEGameRestart)evt;

		Application.LoadLevel ("Game");
	}
	#endregion

	void Awake() {

		TileElement.tileElementOrdinal = 0;

		// if we haven't ran the game from the Main level, start it
		if (!Loading.loadingStarted) {
			Application.LoadLevel ("Main");
		}

		// clear the sprite animation cache
		SpriteAnimationCache.Clear();

		// reset the per-run stats
		RunInstance.Reset();

		cachedTrans = transform;

		tiles = cachedTrans.Find ("TileController").GetComponent<TileController>();
		spaceship = cachedTrans.Find ("SpaceshipController").GetComponent<SpaceshipController>();
		input = cachedTrans.Find ("InputController").GetComponent<InputController>();
		data = cachedTrans.Find ("DataController").GetComponent<DataController>();
		ui = cachedTrans.Find ("UIController").GetComponent<UIController>();
		social = cachedTrans.Find ("SocialController").GetComponent<SocialController>();
		ads = cachedTrans.Find ("AdController").GetComponent<AdController>();
		sound = cachedTrans.Find ("SoundController").GetComponent<SoundController>();
		scoring = cachedTrans.Find ("ScoringController").GetComponent<ScoringController>();
		payments = cachedTrans.Find ("PaymentController").GetComponent<PaymentController>();
		localNotifications = cachedTrans.Find ("LocalNotificationController").GetComponent<LocalNotificationController>();

		postEvent (new CEDoData (DoDataType.Load));

		// event listeners
		registerEventListener (typeof(CEGamePhase), gamePhaseEventListener);
		registerEventListener (typeof(CEGameRestart), gameRestartEventListener);
		registerEventListener (typeof(CEContinue), continueEventListener);
		registerEventListener (typeof(CECurrentSaveData), saveDataEventListener);
	}

	private void fb_InitComplete() {
	
	}

	private void fb_HideUnity() {

	}

	// Use this for initialization
	void Start () {

		postEvent (new CEGamePhase(GamePhaseMode.Menu));

		// at game start, reload everything!
		tiles.Load();

		Instantiate (Resources.Load ("SpaceshipCamera"));

		// if it's not the first load, flicker
		// otherwise the flicker is started when the fade ends from the loading scene.
		//if (!isFirstLoad) {
			
		//}

		StartCoroutine (distort());

		isFirstLoad = false;
	}

	private static bool mustShowAd = false;

	// wait a frame to ensure that the screen distortion postprocess is initialised.
	// quite hacky but works 
	private IEnumerator distort() {

		yield return new WaitForEndOfFrame();

		postEvent (new CEScreenDistortion());

		yield return new WaitForSeconds (0.3f);

		if (mustShowAd) 
		{
			// show an ad based on chance
			float r = UnityEngine.Random.Range(0f, 1f);
			if (r <= 0.65f) {
				postEvent (new CEDoAd (AdActionType.ShowVideo));
			}
		}
	}

	// Update is called once per frame
	void Update () {

		if (lastGameMode == GamePhaseMode.Menu) {
			updateMenu();
		}
		else if (lastGameMode == GamePhaseMode.Gameplay) {

			updateGameplay();
		}
	}

	private void updateMenu() {

	}
	
	private void updateGameplay() {



	}
	
	void LateUpdate() {

	}
}
