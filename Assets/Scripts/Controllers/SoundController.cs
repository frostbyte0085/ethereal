﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// TODO: refactor using dictionary instead of list
public class SoundController : MessageBehaviour {

	private Sound[] sounds;
	private SoundComponent[] components;

	private List<uint> soundInstanceIds;
	private uint soundIdOrdinal;

	#region event listeners
	private void doSoundEventListener (ConvexEvent evt) {
		CEDoSound e = (CEDoSound)evt;

		if (sounds == null || sounds.Length == 0) return;

		switch (e.Action) {
		case DoSoundAction.Play:
			foreach (Sound snd in sounds) {
				if (snd.name == e.Name) {

					uint id = ++soundIdOrdinal;
					soundInstanceIds.Add (id);

					snd.Play(e.Param, id);

					e.SoundInstanceID = id;

					break;
				}
			}
			break;

		case DoSoundAction.SetVolume:
			foreach (SoundComponent cp in components) {
				if (cp.name == e.Name) {
					cp.volume = Mathf.Clamp01(e.Param);
				}
			}
			break;

		case DoSoundAction.Mute:
			foreach (SoundComponent cp in components) {
				if (cp.name == e.Name) {
					cp.mute = true;
				}
			}
			break;

		case DoSoundAction.Unmute:
			foreach (SoundComponent cp in components) {
				if (cp.name == e.Name) {
					cp.mute = false;
				}
			}
			break;

		case DoSoundAction.PauseComponent:
			foreach (SoundComponent cp in components) {
				if (cp.name == e.Name) {
					Sound[] componentSounds = cp.GetComponentsInChildren<Sound>();
					if (componentSounds != null) {
						foreach (Sound snd in componentSounds) {
							snd.Pause (0, true);
						}
					}
				}
			}
			break;

		case DoSoundAction.ResumeComponent:
			foreach (SoundComponent cp in components) {
				if (cp.name == e.Name) {
					Sound[] componentSounds = cp.GetComponentsInChildren<Sound>();
					if (componentSounds != null) {
						foreach (Sound snd in componentSounds) {
							snd.Resume (0, true);
						}
					}
				}
			}
			break;
		
		case DoSoundAction.PauseAll:
			foreach (Sound snd in sounds) {
				snd.PauseAll();
			}
			break;

		case DoSoundAction.ResumeAll:
			foreach (Sound snd in sounds) {
				snd.ResumeAll();
			}
			break;

		case DoSoundAction.StopAll:
			foreach (Sound snd in sounds) {
				snd.StopAll();
			}
			soundInstanceIds.Clear();
			break;

		case DoSoundAction.Stop:
			foreach (Sound snd in sounds) {
				if (snd.name == e.Name) {
					if (snd.Stop (e.SoundInstanceID)) {
						// remove the instanceid from the list
						soundInstanceIds.Remove (e.SoundInstanceID);
						break;
					}
				}
			}
			break;

		case DoSoundAction.Resume:
			foreach (Sound snd in sounds) {
				if (snd.name == e.Name) {
					snd.Resume (e.SoundInstanceID);
				}
			}
			break;

		case DoSoundAction.Pause:
			foreach (Sound snd in sounds) {
				if (snd.name == e.Name) {
					snd.Pause (e.SoundInstanceID);
				}
			}
			break;

		default:break;
		}
	}
	#endregion

	// Use this for initialization
	void Awake () {

		sounds = GetComponentsInChildren<Sound>();
		components = GetComponentsInChildren<SoundComponent>(true);

		registerEventListener (typeof(CEDoSound), doSoundEventListener);

		soundInstanceIds = new List<uint>();
	}

}
