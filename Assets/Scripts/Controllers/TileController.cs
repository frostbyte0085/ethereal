﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public class TileController : MessageBehaviour {

	public FieldType forceField = FieldType.None;
	public bool enableTiles = false;
	
	public float difficultyStep = 0.05f; // how much we increment difficulty by
	public float maxDifficultyStepPerSecond = 0.0002f;
	public int numFamiliesForDifficulty = 2; // increment difficulty every family changes
	public AnimationCurve spaceshipSpeedCurve; // speed based on difficulty

	public AnimationCurve mineProbabilityForDifficulty;
	public AnimationCurve minefieldProbabilityForDifficulty;
	public AnimationCurve minefieldTilesForDifficulty;

	public AnimationCurve sawbotProbabilityForDifficulty;
	public AnimationCurve sawbotfieldProbabilityForDifficulty;
	public AnimationCurve sawbotfieldTilesForDifficulty;

	public AnimationCurve zapProbabilityForDifficulty;
	public AnimationCurve zapfieldProbabilityForDifficulty;
	public AnimationCurve zapfieldTilesForDifficulty;
	
	public AnimationCurve laserfieldProbabilityForDifficulty;
	public AnimationCurve lasersForDifficulty;
	public AnimationCurve lasersTimingForDifficulty;

	public float easyEnd = 0.5f;
	public float mediumStart = 0.25f;
	public float mediumEnd = 0.75f;
	public float hardStart = 0.5f;

	public int minRewardTiles = 3;
	public int maxRewardTiles = 5;

	public int minTiles = 3;
	public int maxTiles = 5;

	public float warpdrivePointLeaveBehind = 0.85f;

	private List<Tile> currentTiles;
	private Tile lastTile;

	private List<Vector2> warpdrivePath;

	private GamePhaseMode lastGameMode = GamePhaseMode.Invalid;

	private CESpaceshipStatus lastSpaceshipStatus;

	private int totalFamilyChanges;
	private int totalTilesSpawned;

	private int currentTilesInFamily;
	private int targetTilesInFamily;

	private int currentTilesForMineFieldSequence;
	private int currentTilesForZapFieldSequence;
	private int currentTilesForSawbotFieldSequence;
	
	private float currentMineProbability_Difficulty;
	private float currentMineFieldProbability_Difficulty;
	private int currentTilesForMineFieldSequence_Difficulty;

	private float currentSawbotProbability_Difficulty;
	private float currentSawbotFieldProbability_Difficulty;
	private int currentTilesForSawbotFieldSequence_Difficulty;

	private float currentZapProbability_Difficulty;
	private float currentZapFieldProbability_Difficulty;
	private int currentTilesForZapFieldSequence_Difficulty;

	private float currentLaserfieldProbability_Difficulty;
	private int currentLasersForDifficulty;
	private float currentLasersTimingForDifficulty;
	private int numLasersSpawned; // lasers spawned this field.
	private int numLaserTiles;
	private int numLaserWaitTiles; // wait until we exit the laser area. 2 should be enough.

	private bool isInRewardArea;
	private int targetRewardTiles;
	private int currentRewardTiles;

	private CECurrentSaveData saveData;
	private bool inTutorial;
	private bool isInWarpdrive;
	private FieldType inFieldType;

	private float currentDifficulty;
	private float targetDifficulty;
	private int numFamiliesChangedForDifficultyStep;

	#region event listeners
	private void gamePhaseEventListener(ConvexEvent evt) {
		CEGamePhase e = (CEGamePhase)evt;

		if (e.Mode != lastGameMode) {
			switch (e.Mode) {
			case GamePhaseMode.Gameplay:
				break;

			case GamePhaseMode.Lose:
				break;
			}
		}

		lastGameMode = e.Mode;
	}

	private void spaceshipStatusEventListener(ConvexEvent evt) {
		CESpaceshipStatus e = (CESpaceshipStatus)evt;

		lastSpaceshipStatus = e;
	}

	private void warpdriveEventListener (ConvexEvent evt) {
		CEWarpdriveCollect e = (CEWarpdriveCollect)evt;

		isInWarpdrive = (e.Phase == WarpdrivePhase.Start);
	}

	private void getWarpdrivePathEventListener(ConvexEvent evt) {
		CEGetWarpdrivePath e = (CEGetWarpdrivePath)evt;

		if (warpdrivePath != null) {
			// sort the path based on x
			warpdrivePath.Sort (delegate (Vector2 v1, Vector2 v2) {
				return v1.x.CompareTo (v2.x);
			}
			);

			e.AddResponse (new CRWarpdrivePath(warpdrivePath));
		}
	}

	private void saveDataEventListener (ConvexEvent evt) {
		saveData = (CECurrentSaveData)evt;

		inTutorial = saveData.NeedsTutorial;
	}

	private void needsTutorialEventListener (ConvexEvent evt) {
		CENeedsTutorial e = (CENeedsTutorial)evt;

		// this is typically called when the player re-enables tutorial from the settings menu.
		// this will make the tile controller destroy the tiles 1 ... tileCount and respawn the tutorial ones instead.
		// the tile 0 is the initial adaptor, so we still want it.
		if (e.Tutorial) {
			isInRewardArea = false;

			// similar to how we recycle the tiles.
			for (int i=currentTiles.Count-1; i>=1; i--) {
				Tile t = currentTiles[i];

				Tile parent = t.ParentTile;
				parent.Recycle (t);
				
				currentTiles.RemoveAt(i);
			}

			lastTile = currentTiles[0]; // set the last tile back to the initial adaptor tile
			// reset tile family variables, we don't want any accidental family switches
			totalTilesSpawned = 1;
			currentTilesInFamily = 0;
			currentRewardTiles = 0;

			// tell the system that we are in tutorial mode
			inTutorial = true;

			// now spawn some more. these tiles will be tutorial ones
			for (int i=0; i<5; i++) {
				getNextTile(false); // do not allow fields spawning so early
			}
		}
		// if tutorial has ended, reset the reward area, we don't want it right after the tutorial
		else {
			isInRewardArea = false;
		}
	}

	#endregion

	void Awake() {

		warpdrivePath = new List<Vector2>();

		registerEventListener (typeof(CEGamePhase), gamePhaseEventListener);
		registerEventListener (typeof(CESpaceshipStatus), spaceshipStatusEventListener);
		registerEventListener (typeof(CEGetWarpdrivePath), getWarpdrivePathEventListener);
		registerEventListener (typeof(CECurrentSaveData), saveDataEventListener);
		registerEventListener (typeof(CENeedsTutorial), needsTutorialEventListener);
		registerEventListener (typeof(CEWarpdriveCollect), warpdriveEventListener);

		currentTilesForMineFieldSequence = 0;
		currentTilesForSawbotFieldSequence = 0;
		currentTilesForZapFieldSequence = 0;

		inFieldType = FieldType.None;
		spawnedFieldThisFamily = true;
	}

	public void Load() {

		TileLoader.EnumerateAndLoad();

		if (enableTiles) {
			StartCoroutine (InitTiles());
		}
	}

	protected override void OnDestroy() {
		base.OnDestroy();

		if (currentTiles != null) {
			for (int i=0; i<currentTiles.Count; i++) {
				if (currentTiles[i] != null){
					Destroy (currentTiles[i].gameObject);
				}
			}

			currentTiles.Clear();
		}

		lastTile = null;
	}

	public IEnumerator InitTiles() {

		currentTiles = new List<Tile>();

		lastTile = null;

		// get the initial tile, but wait a frame to get the save data...
		// lame way, we need to refactor the event system for these cases.
		getNextTile (false);

		yield return null; // wait a frame

		// if the game needs to run the tutorial again, constrain the tiles to empty spacestation tiles.
		// spawn all the rest
		for (int i=0; i<5; i++) {
			getNextTile(false); // do not allow minefield spawning so early
		}
	}

	private Tile spawnNextRandomNormalTile(RewardType reward = RewardType.None) {
		float[] weights = new float[3];
		float difficulty = currentDifficulty;

		if (difficulty < easyEnd) {
			weights[0] = Mathf.Cos((Mathf.PI/2f) * difficulty / easyEnd);
		}

		if (difficulty > mediumStart && difficulty < mediumEnd) {
			weights[1] = Mathf.Sin(Mathf.PI * (difficulty - mediumStart) / (mediumEnd-mediumStart));
		}
		
		if (difficulty > hardStart) {
			weights[2] = Mathf.Sin((Mathf.PI/2f) * (difficulty - hardStart) / (1.0f - hardStart));
		}

		float totalWeight = 0.0f;
		foreach (float f in weights) {
			totalWeight += f;
		}
		
		float r = Random.Range(0.0f, totalWeight);
		TileDifficulty td = TileDifficulty.Easy;
		for (int i=0; i<weights.Length; i++) {
			if (r <= weights[i]) {
				td = (TileDifficulty)i;
				break;
			}
			r -= weights[i];
		}

		List<Tile> tilesForFamily = TileLoader.GetTiles(td)[lastTile.to];
		
		int idx = Random.Range (0, tilesForFamily.Count);
		return spawn(tilesForFamily[idx], reward);
	}
	

	private Tile spawnNextMineFieldTile() {
		return spawn (TileLoader.MinefieldTiles[Random.Range(0, TileLoader.MinefieldTiles.Count)]);
	}

	private Tile spawnNextSawbotFieldTile() {
		return spawn (TileLoader.SawbotFieldTiles[Random.Range(0, TileLoader.SawbotFieldTiles.Count)]);
	}

	private Tile spawnNextZapFieldTile() {
		return spawn (TileLoader.ZapFieldTiles[Random.Range(0, TileLoader.ZapFieldTiles.Count)]);
	}

	private Tile spawnNextLaserFieldTile() {
		numLaserTiles++;
		return spawn (TileLoader.LaserFieldTiles[Random.Range(0, TileLoader.LaserFieldTiles.Count)]);
	}

	// a reward tile can be a tutorial tile as well...
	// I will not forgive myself for this.
	private bool foundPowerup = false;
	private uint numRewardRegions = 0;

	private Tile spawnNextRewardTile() {
		if (inTutorial) {
			return spawn (TileLoader.TutorialTile);
		}else {

			RewardType reward = RewardType.None;

			// ignore the first reward region for powerups.
			// also ignore if we're in warpdrive mode
			if (!isInWarpdrive) {

				// crystal group
				if ((currentRewardTiles % 2 == 0) || (currentRewardTiles == 0)) {
					if (!foundPowerup) {
						//float r = Random.Range(0f, 1f);
						//if (r <= 0.5f) {
							reward = RewardType.Powerup;
							foundPowerup = true;
						//}
					}

				}
				else {
					reward = RewardType.CrystalGroup;
				}

			}

			currentRewardTiles++;
			if (currentRewardTiles == targetRewardTiles)
				isInRewardArea = false;

			// here we assume that reward tiles consist of only one element, this way we can do the previous calculation
			Tile t = spawn (TileLoader.RewardTiles[Random.Range(0, TileLoader.RewardTiles.Count)], reward);
			return t;
		}
		return null;
	}

	private Tile spawnNextAdaptorTile(TileFamily fromFamily, TileFamily nextFamily) {
		totalFamilyChanges++;
		numFamiliesChangedForDifficultyStep++;
		// if we reached the target family count, then set a new target difficulty
		if (currentDifficulty < 1f) {
			if (numFamiliesChangedForDifficultyStep == numFamiliesForDifficulty) {
				numFamiliesChangedForDifficultyStep = 0;
				targetDifficulty += difficultyStep;
			}
		}

		spawnedFieldThisFamily = false; // we changed family, allow spawning of fields again.
		return spawn(TileLoader.AdaptorTiles[fromFamily][nextFamily]);
	}
	
	private Tile spawn(Tile t, RewardType reward=RewardType.None) {
		totalTilesSpawned++;
		return t.GetPooled(currentMineProbability_Difficulty, currentSawbotProbability_Difficulty, currentZapProbability_Difficulty, reward);
	}

	private void resetFieldTileCount() {
		inFieldType = FieldType.None;
		currentTilesForMineFieldSequence = 0;
		currentTilesForSawbotFieldSequence = 0;
		currentTilesForZapFieldSequence = 0;
		numLasersSpawned = 0;
		numLaserTiles = 0;
		numLaserWaitTiles = 0;
	}

	private bool spawnedFieldThisFamily=false;
	// load a new tile and anchor it to the previous one
	private void getNextTile(bool allowField=true) {

		Tile t = null;

		int prevTileCount = currentTiles.Count;
		// adaptor
		if ( (inFieldType == FieldType.None) && ((prevTileCount == 0) || (currentTilesInFamily >= targetTilesInFamily))) {

			// reward section detection
			// this is the exit adaptor (when we leave the station)
			if (lastTile != null){
				if (!isInRewardArea && lastTile.from == TileFamily.SpaceStation && lastTile.type != TileType.Reward) {
					targetRewardTiles = Random.Range(minRewardTiles, maxRewardTiles+1);
					foundPowerup = false;
					currentRewardTiles = 0;
					numRewardRegions++;
					isInRewardArea = true;
				}
			}

			if (isInRewardArea) {
				t = spawnNextRewardTile();
			}
			else {
				currentTilesInFamily = 0;
				targetTilesInFamily = Random.Range(minTiles, maxTiles);

				TileFamily nextFamily = TileFamily.SpaceStation; // this is the default, when we haven't spawned any tiles yet.
				TileFamily fromFamily = TileFamily.AsteroidField;// this is the default, when we haven't spawned any tiles yet.

				if (lastTile != null) {
					fromFamily = lastTile.from;
					nextFamily = lastTile.to;
					nextFamily++;

					if (nextFamily == TileFamily.Invalid) {
						nextFamily = 0;
					}
				}

				t = spawnNextAdaptorTile(fromFamily, nextFamily);
				// if we are entering the spacestation, enter the reward area first
				// check for totalFamilyChanges, cause we don't want to spawn rewards in the beginning
				if (t.to == TileFamily.SpaceStation && (totalFamilyChanges > 1)) {
					targetRewardTiles = Random.Range(minRewardTiles, maxRewardTiles+1);
					foundPowerup = false;
					numRewardRegions++;
					currentRewardTiles = 0;
					isInRewardArea = true;
				}
			}
		}
		// normal tile
		else {

			if (isInRewardArea || inTutorial) {
				t = spawnNextRewardTile();
			}
			else {
				if (inFieldType == FieldType.MineField) {
					if (currentTilesForMineFieldSequence < currentTilesForMineFieldSequence_Difficulty) {
						t = spawnNextMineFieldTile();
						currentTilesForMineFieldSequence++;
					}
					else {
						resetFieldTileCount();
						t = spawnNormalTile(allowField);
					}
				}
				else if (inFieldType == FieldType.SawbotField) {
					if (currentTilesForSawbotFieldSequence < currentTilesForSawbotFieldSequence_Difficulty) {
						t = spawnNextSawbotFieldTile();
						currentTilesForSawbotFieldSequence++;
					}
					else {
						resetFieldTileCount();
						t = spawnNormalTile(allowField);
					}
				}
				else if (inFieldType == FieldType.ZapField) {
					if (currentTilesForZapFieldSequence < currentTilesForZapFieldSequence_Difficulty) {
						t = spawnNextZapFieldTile();
						currentTilesForZapFieldSequence++;
					}
					else {
						resetFieldTileCount();
						t = spawnNormalTile(allowField);
					}
				}
				else if (inFieldType == FieldType.LaserField) {
					if (numLasersSpawned < currentLasersForDifficulty) {
						t = spawnNextLaserFieldTile();
					}
					else {
						if (numLaserWaitTiles < 2) {
							numLaserWaitTiles++;
							t = spawnNextLaserFieldTile();
						}
						else {
							resetFieldTileCount();
							t = spawnNormalTile(allowField);
						}
					}
				}
				else {
					t = spawnNormalTile(allowField);
				}
			}
		}

		// snapping
		if (lastTile != null) {
			t.transform.position = lastTile.transform.position + Vector3.right * lastTile.TileSize;
		}
		else {
			t.transform.position = Vector3.zero;
		}

		currentTiles.Add (t);
		lastTile = t;
	}

	private FieldType lastFieldType = FieldType.None;
	private bool rewardFinished;
	private Tile spawnNormalTile(bool allowField) {
		Tile t = spawnNextRandomNormalTile(RewardType.None);
		
		currentTilesInFamily++;

		bool forceFirstRun = (forceField != FieldType.None);
		allowField = (allowField) && ((totalFamilyChanges > 1) || forceFirstRun);
		//allowField = allowField && (!isInWarpdrive);

		if ((lastTile.to == TileFamily.SpaceStation) && allowField && (!spawnedFieldThisFamily)) {

			// select a random field type to spawn
			FieldType type = FieldType.None;

			do {
				type = (FieldType)Random.Range(0, (int)FieldType.None);

			}while (type == lastFieldType);

			float p = Random.Range (0f, 1f);

			if (forceField != FieldType.None) {
				type = forceField;
				p = 0f;
			}


			if (type == FieldType.MineField) {
				if (p < currentMineFieldProbability_Difficulty) {
					inFieldType = FieldType.MineField;

					// don't allow spawning of another field sequence without exiting the space station family
					spawnedFieldThisFamily = true;

					postEvent (new CEShowWarning("APPROACHING MINEFIELD!", "ApproachingMinefield"));
				}
			}
			else if (type == FieldType.SawbotField) {
				if (p < currentSawbotFieldProbability_Difficulty) {
					inFieldType = FieldType.SawbotField;
					
					// don't allow spawning of another field sequence without exiting the space station family
					spawnedFieldThisFamily = true;
					
					postEvent (new CEShowWarning("APPROACHING SAWBOT FIELD!", "ApproachingSawbotfield"));
				}
			}
			else if (type == FieldType.ZapField) {
				if (p < currentZapFieldProbability_Difficulty) {
					inFieldType = FieldType.ZapField;
					
					// don't allow spawning of another field sequence without exiting the space station family
					spawnedFieldThisFamily = true;
					
					postEvent (new CEShowWarning("APPROACHING ELECTRO FIELD!", "ApproachingElectrofield"));
				}
			}
			else if (type == FieldType.LaserField) {
				if (p < currentLaserfieldProbability_Difficulty) {
					inFieldType = FieldType.LaserField;

					// don't allow spawning of another field sequence without exiting the space station family
					spawnedFieldThisFamily = true;

					postEvent (new CEShowWarning("APPROACHING LASER FIELD!", "ApproachingLaserfield"));
				
				
				}
			}

			lastFieldType = type;
		}

		return t;
	}

	int currentFrames=0;

	// Update is called once per frame
	void Update () {

		if (!enableTiles) return;

		// split computation of curves over frames. this seems to to greatly improve performance on lower end devices
		currentFrames++;
		// evaluate curves every n frames
		if (currentFrames == 40) {
			currentMineProbability_Difficulty = Mathf.Clamp01(mineProbabilityForDifficulty.Evaluate (currentDifficulty));
			currentMineFieldProbability_Difficulty = Mathf.Clamp01(minefieldProbabilityForDifficulty.Evaluate(currentDifficulty));
			currentTilesForMineFieldSequence_Difficulty = Mathf.CeilToInt(minefieldTilesForDifficulty.Evaluate(currentDifficulty));
		}

		if (currentFrames == 80) {
			currentSawbotProbability_Difficulty = Mathf.Clamp01(sawbotProbabilityForDifficulty.Evaluate (currentDifficulty));
			currentSawbotFieldProbability_Difficulty = Mathf.Clamp01(sawbotfieldProbabilityForDifficulty.Evaluate(currentDifficulty));
			currentTilesForSawbotFieldSequence_Difficulty = Mathf.CeilToInt(sawbotfieldTilesForDifficulty.Evaluate(currentDifficulty));
		}

		if (currentFrames == 120) {
			currentZapProbability_Difficulty = Mathf.Clamp01(mineProbabilityForDifficulty.Evaluate (currentDifficulty));
			currentZapFieldProbability_Difficulty = Mathf.Clamp01(zapfieldProbabilityForDifficulty.Evaluate(currentDifficulty));
			currentTilesForZapFieldSequence_Difficulty = Mathf.CeilToInt(zapfieldTilesForDifficulty.Evaluate(currentDifficulty));
		}

		if (currentFrames == 160) {
			currentLaserfieldProbability_Difficulty = Mathf.Clamp01(laserfieldProbabilityForDifficulty.Evaluate (currentDifficulty));
			currentLasersForDifficulty = Mathf.CeilToInt (lasersForDifficulty.Evaluate(currentDifficulty));
			currentLasersTimingForDifficulty = lasersTimingForDifficulty.Evaluate(currentDifficulty);
		}

		if (currentFrames > 160) {
			currentFrames = 0;
		}

		if (currentTiles != null && currentTiles.Count > 0) {

			if (lastTile != null) {

				float dist = Mathf.Abs(Camera.main.transform.position.x - lastTile.transform.position.x);

				if (dist < Constants.tileScreenSize) {
					getNextTile();
				}
			}

			for (int i=currentTiles.Count-1; i>=0; i--) {
				Tile t = currentTiles[i];
				if (t == null)
					continue;

				if (t.transform.position.x <= Camera.main.transform.position.x - (t.TileSize+Constants.tileScreenSize)) {
					Tile parent = t.ParentTile;
					parent.Recycle (t);

					currentTiles.RemoveAt(i);
				}

			}

			// update the path that the ship will follow while in warpdrive mode
			updateWarpdrivePath();

			//
			if (inFieldType == FieldType.LaserField && numLaserWaitTiles == 0) {
				// spawn laser beams
				updateLaserBeams();
			}
			//
		}


		// uodate if the spaceship is alive
		if (lastSpaceshipStatus != null && lastSpaceshipStatus.IsAlive) {
			updateDifficulty();
		}

	}

	private void updateDifficulty() {
		// update the difficulty setting. simply move towards the target difficulty over time
		currentDifficulty = Mathf.MoveTowards (currentDifficulty, targetDifficulty, Time.deltaTime * maxDifficultyStepPerSecond);
		currentDifficulty = Mathf.Clamp01(currentDifficulty);

		RunInstance.Difficulty = currentDifficulty;

		float speed = spaceshipSpeedCurve.Evaluate(currentDifficulty);
		postEvent (new CEDifficultyUpdate(speed, currentDifficulty));
	}

	private float laserWaitElapsed=0f;
	private float laserInterval=1f;

	// we spawn lasers only within the asteroid field, ignoring warpdrive mode, tutorial, reward areas
	private void updateLaserBeams() {
		if (lastSpaceshipStatus == null) return;

		if (isInWarpdrive) {
			numLasersSpawned = currentLasersForDifficulty;
			return;
		}

		if (inTutorial) return;
		if (!lastSpaceshipStatus.IsAlive) return;

		// try spawning a new one
		laserWaitElapsed += Time.deltaTime;
		if (laserWaitElapsed >= laserInterval && numLaserTiles > 4) {
			laserWaitElapsed = 0f;
			laserInterval = Random.Range(currentLasersTimingForDifficulty*0.8f, currentLasersTimingForDifficulty*1.2f);

			PooledInstance laserInstance = ObjectPoolManager.Instance.Spawn ("GenericPool", "laser_beam");
			if (laserInstance != null) {
				Laser laser = laserInstance.CachedGameObject.GetComponent<Laser>();
				laser.Instance = laserInstance;

				float shipScreenPos = Camera.main.WorldToViewportPoint (lastSpaceshipStatus.ShipTransform.position).y;

				laser.Spawn (shipScreenPos + Random.Range(-0.1f, 0.1f));

			}
			numLasersSpawned++;
		}
	}

	private void updateWarpdrivePath() {

		if (lastSpaceshipStatus == null || lastSpaceshipStatus.ShipTransform == null)
			return;

		warpdrivePath = new List<Vector2>();

		for (int i=0; i<currentTiles.Count; i++) {
			Tile t = currentTiles[i];
			if (t == null)
				continue;

			Vector2[] points = t.WarpdrivePoints;

			if (points != null) {
				for (int p=0; p<points.Length; p++) {
					Vector3 point = t.transform.TransformPoint( points[p] );

					if (point.x < lastSpaceshipStatus.ShipTransform.position.x-warpdrivePointLeaveBehind)
						continue;

					warpdrivePath.Add (point);
				}
			}

		}
	}
}
