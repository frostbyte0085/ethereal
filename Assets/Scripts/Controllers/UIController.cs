﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class UIState {
	public static int popupCount;
}

public class UIController : MessageBehaviour {

	public dfPanel vignettes;
	public dfTiledSprite scanlines;
	public Camera mainCamera;
	public dfTextureSprite blockerBG;

	private UIScreen currentScreen;
	private List<UIPopup> activePopups;

	private Transform cachedTrans;
	private dfGUIManager uiMgr;

	private dfPanel popupPanel;
	private dfPanel scanlinePanel;

	#region event listeners
	private void doScanlinesEventListener (ConvexEvent evt) {
		CEDoScanlines e = (CEDoScanlines)evt;

		scanlines.gameObject.SetActive (e.Enabled);
	}

	private void changeScreenEventListener (ConvexEvent evt) {
		CEChangeScreen e = (CEChangeScreen)evt;

		changeCurrentScreen (e.ClassType);
	}

	private void pushPopupEventListener (ConvexEvent evt) {
		CEPushPopup e = (CEPushPopup)evt;

		pushPopup (e.ClassType);
	}

	private void popPopupEventListener (ConvexEvent evt) {
		CEPopPopup e = (CEPopPopup)evt;

		if (activePopups.Count > 0) {
			activePopups[activePopups.Count-1].Dismiss();
		}
	}

	private void screenDistortionEventListener (ConvexEvent evt) {
		CEScreenDistortion e = (CEScreenDistortion)evt;

		if (mainCamera != null) {
			FlickerPostProcess pp = mainCamera.GetComponent <FlickerPostProcess>();
			if (pp != null) {
				pp.enabled = true;
			}

		}
	}
	#endregion

	void Awake() {
		activePopups = new List<UIPopup>();

		cachedTrans = transform;
		uiMgr = GetComponent<dfGUIManager>();

		popupPanel = cachedTrans.Find ("popupPanel").GetComponent<dfPanel>();
		popupPanel.gameObject.SetActive (true);

		scanlinePanel = scanlines.Parent.GetComponent<dfPanel>();

		registerEventListener (typeof(CEChangeScreen), changeScreenEventListener);
		registerEventListener (typeof(CEPopPopup), popPopupEventListener);
		registerEventListener (typeof(CEPushPopup), pushPopupEventListener);
		registerEventListener (typeof(CEScreenDistortion), screenDistortionEventListener);
		registerEventListener (typeof(CEDoScanlines), doScanlinesEventListener);
	}

	// Update is called once per frame
	void Update () {

		popupPanel.ZOrder = 1000; // set it to something very high, so it goes over all the rest of the UI
		vignettes.ZOrder = 1001; // always on top of everything

		blockerBG.enabled = (activePopups.Count > 0);
		blockerBG.ZOrder = Mathf.Max(0, activePopups.Count - 1);

		// go through all the popups and see which ones are to be dismissed, and dismiss them
		for (int i=activePopups.Count-1; i>=0 ;i--) {
			UIPopup popup = activePopups[i];

			if (popup.DismissPending) {
				destroy (popup);
				activePopups.RemoveAt(i);

				if (activePopups.Count == 0) {
					currentScreen.OnGainFocus();
				}
				else {
					if (i > 0) {
						StartCoroutine( popupGainFocus(i) );
					}
				}
			}
		}

		// scroll the scanlines
		if (scanlines.gameObject.activeInHierarchy) {
			scanlines.TileScroll += new Vector2(0,1) * Time.unscaledDeltaTime;
			scanlinePanel.ZOrder = Mathf.Max(1, activePopups.Count+1);
		}

		if (activePopups != null) {
			UIState.popupCount = activePopups.Count;
		}

	}

	private IEnumerator popupGainFocus(int lastIdx) {
		yield return null;

		if (activePopups != null) {
			if (activePopups.Count > 0) {

				if (activePopups.Count-1 == lastIdx) {
					if (activePopups[lastIdx] != null) {
						activePopups[lastIdx].OnGainFocus();
					}
				}
				else {
					if (activePopups[lastIdx-1] != null) {
						activePopups[lastIdx-1].OnGainFocus();
					}
				}
			}
		}
	}

	// destroys the selected screen (or popup)
	private void destroy(UIScreen s) {
		if (s != null) {
			s.OnWillHide();
			s.gameObject.SetActive (false);
			s.OnHide();
			
			Destroy (s.gameObject);
		}
	}

	// warning:
	// df-gui screen prefabs must be disabled in the editor before instantiating them!
	private void changeCurrentScreen(Type t) {
		// go through the popups and destroy them first
		for (int i=0; i<activePopups.Count; i++) {
			destroy (activePopups[i]);
		}
		activePopups.Clear();

		// destroy the previous one
		destroy (currentScreen);

		if (t != null) {
			GameObject screenGO = load (t);
			if (screenGO != null) {
				dfControl ctrl = uiMgr.AddPrefab (screenGO);
				currentScreen = ctrl.GetComponent<UIScreen>();

				if (currentScreen != null) {
					currentScreen.OnWillShow();
					currentScreen.gameObject.SetActive (true);
					currentScreen.OnShow();
				}
			}
		}
	}

	private void pushPopup (Type t) {

		GameObject popupGO = load (t);
		if (popupGO != null) {
			dfControl ctrl = popupPanel.AddPrefab (popupGO);
			UIPopup popup = ctrl.GetComponent<UIPopup>();

			if (popup != null) {
				if (activePopups.Count == 0) {
					currentScreen.OnLoseFocus();
				}

				if (activePopups.Count > 0) {
					activePopups[activePopups.Count-1].OnLoseFocus();
				}

				popup.OnWillShow();
				popup.gameObject.SetActive (true);
				popup.OnShow();

				activePopups.Add (popup);
			}
		}
	}

	private GameObject load(Type t) {

		if (t == null)
			return null;

		string rootpath = null;

		if (t.BaseType == typeof(UIScreen)) {
			rootpath = ResourceStringManager.ScreensPath;
		}
		else if (t.BaseType == typeof(UIPopup)) {
			rootpath = ResourceStringManager.PopupsPath;
		}
		else {
			return null;
		}

		string filename = rootpath + "/" + t.ToString();
		return (GameObject)Resources.Load (filename);
	}
}
