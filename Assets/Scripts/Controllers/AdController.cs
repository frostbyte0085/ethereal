﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Advertisements;

public static class AdState {
	public static bool hasAds;
}

public class AdController : MessageBehaviour {

#if UNITY_IPHONE
	private const string appId = "16431";
#elif UNITY_ANDROID
	private const string appId = "16433";
#else
	private const string appId = "";
#endif

	#region event listeners
	private void showAdEventListener(ConvexEvent evt) {
		CEDoAd e = (CEDoAd)evt;

		// ignore this on platforms that don't support ads
		if (!Advertisement.isSupported) {
			Debug.LogError ("Ignoring ad, unsupported platform");
			return;
		}

		Debug.Log ("Trying to show ad...");

		// if the disableads non-consumable is purchased, ignore this
		if (e.CheckForAds && (Unibiller.GetPurchaseCount("disableads")==1)) {
			return;
		}

		StopCoroutine ("doAction");
		StartCoroutine ("doAction", e);
	}

	private void disableAdsEventListener (ConvexEvent evt) {
		CEDisableAds e = (CEDisableAds)evt;

		AdState.hasAds = false;
	}
	#endregion

	private int adReward;
	private void adResult (ShowResult result) {

		if (result == ShowResult.Finished) {
			Unibiller.CreditBalance ("Crystals", adReward);
		}

		adReward = 0;
	}

	private IEnumerator doAction(CEDoAd e) {
		if (!Advertisement.isInitialized) {
			Debug.LogError ("Ad show failed, not initialised!");
			yield break;
		}

		/*
		while (!Advertisement.isReady()) {
			Debug.Log ("Waiting for ad...");
			yield return null;
		}
		*/

		adReward = e.GiveCrystals;
		ShowOptions options = new ShowOptions();
		options.pause = true;
		options.resultCallback = adResult;

		AdActionType type = e.Type;

		if (type == AdActionType.ShowRandom) {
			type = (AdActionType)Random.Range(0, (int)AdActionType.ShowRandom);
		}

		if (type == AdActionType.ShowVideoForReward) {
			Debug.Log ("Showing video ad (reward)");
			Advertisement.Show ("incentivisedVideoZone", options);
		}
		else if (type == AdActionType.ShowVideo) {
			Debug.Log ("Showing picture ad");
			Advertisement.Show ("defaultVideoAndPictureZone", options);
		}
	}

	// Use this for initialization
	void Awake () {
		if (!Advertisement.isInitialized && Advertisement.isSupported) {

			Advertisement.allowPrecache = true;
			Advertisement.Initialize (appId);

			AdState.hasAds = true;
		}

		registerEventListener (typeof(CEDoAd), showAdEventListener);
		registerEventListener (typeof(CEDisableAds), disableAdsEventListener);
	}

}
