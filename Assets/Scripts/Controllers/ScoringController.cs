﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.SocialPlatforms;

#if UNITY_IPHONE
using UnityEngine.SocialPlatforms.GameCenter;
#endif

#if UNITY_ANDROID
using GooglePlayGames;
#endif

// a bad way of working around the event mess.
// refactor in the next game or regret it forever :@
public static class ScoringState {

	public static IScore[] scores;
	public static IUserProfile[] profiles;

	public static bool isWorking;
	public static bool isAuthenticated;

	public static long currentMaxScore;
}

public class ScoringController : MessageBehaviour {
	
	private string socialName;
	private bool isAuthenticating;
	private float elapsedSinceLastTry;
	
	private bool canTryAuthentication;
	

	#region event listeners
	private void reportScoreEventListener (ConvexEvent evt) {
		CEReportScore e = (CEReportScore)evt;

		reportScore (e.LeaderboardID, e.Score);
	}

	private void openScoringEventListener (ConvexEvent evt) {
		Social.ShowLeaderboardUI();
	}

	private void loadScoresEventListener (ConvexEvent evt) {
		CELoadScores e = (CELoadScores)evt;

		loadScores(e.ID, e.UserScope, e.TimeScope, e.Range);
	}
	#endregion


	void Awake() {
		
		// handle more platforms here
		#if !UNITY_EDITOR
		#if UNITY_IPHONE
		Social.Active = new GameCenterPlatform();
		#elif UNITY_ANDROID
		Social.Active = PlayGamesPlatform.Instance;
		#endif

		#else
		Social.Active = new Local();
		#endif

		socialName = Social.Active.ToString();
		socialName = socialName.Substring (socialName.LastIndexOf('.')+1);
		Debug.Log ("Using Social Platform: " + socialName);
		
		canTryAuthentication = true;
		authenticate();

		registerEventListener (typeof(CEReportScore), reportScoreEventListener);
		registerEventListener (typeof(CEOpenScoring), openScoringEventListener);
		registerEventListener (typeof(CELoadScores), loadScoresEventListener);

		ScoringState.currentMaxScore = long.Parse (PlayerPrefs.GetString ("current_max_score", "0"));
	}
	
	// returns the name of the social platform used.
	public string SocialPlatformName {
		get {
			return socialName;
		}
	}
	
	// here we authenticate the user.
	// useful if connection drops during gameplay, this is called when the connection is stable again.
	private void authenticate() {
		if (!isAuthenticated) {
			isAuthenticating = true;
			Social.localUser.Authenticate (authenticateCallback);
		}
	}
	
	private void authenticateCallback(bool success) {
		if (success) {
			postEvent (new CELoadScores (Constants.leaderboardID, UserScope.FriendsOnly, TimeScope.AllTime, new Range(1, 100)));
		}
		else {
			Debug.LogWarning ("Authentication failed, will retry later.");
			
			// if there was an internet connection and the authentication failed, then there is something wrong,
			// strop retying.
			canTryAuthentication = false;
		}
		
		isAuthenticating = false;
	}

	// loading all scores
	private ILeaderboard currentLeaderboard;
	private void loadScores(string id, UserScope userScope, TimeScope timeScope, Range range) {
		if (!isAuthenticated) {
			return;
		}

		ScoringState.isWorking = true;
		ScoringState.scores = null;

		currentLeaderboard = Social.CreateLeaderboard();
		currentLeaderboard.id = id;
		currentLeaderboard.range = range;
		currentLeaderboard.userScope = userScope;
		currentLeaderboard.timeScope = timeScope;

		currentLeaderboard.LoadScores (loadScoresCallback);
	}
	
	private void loadScoresCallback(bool result) {

		if (currentLeaderboard == null || !result) {
			ScoringState.scores = null;
			ScoringState.isWorking = false;
			return;
		}

		IScore[] scores = currentLeaderboard.scores;

		if (scores.Length > 0) {

			Debug.Log ("Loaded scores for leaderboard: " + currentLeaderboard.id);

			List<string> userIds = new List<string>();
			foreach (IScore k in scores) {
				userIds.Add (k.userID);

				if (k.userID == Social.localUser.id) {

					long currentScore = long.Parse(PlayerPrefs.GetString ("current_max_score", "0"));

					if (k.value > currentScore) {
						ScoringState.currentMaxScore = k.value;
						PlayerPrefs.SetString ("current_max_score", k.value.ToString());
						PlayerPrefs.Save();
					}
				}
			}

			ScoringState.scores = scores;
			Social.LoadUsers (userIds.ToArray(), loadUsersCallback);
		}
		else {
			ScoringState.scores = null;
			ScoringState.isWorking = false;
		}

		currentLeaderboard = null;
	}

	private void loadUsersCallback (IUserProfile[] users) {
		ScoringState.profiles = users;
		ScoringState.isWorking = false;
	}

	// updates the score for this user and leaderboard
	private bool reportScore (string leaderboardId, long score) {
		if (!isAuthenticated) {
			Debug.LogWarning ("Cannot report score, the user is not authenticated!");
			return false;
		}
		
		bool success = false;

		Social.ReportScore(score, leaderboardId, result => 
		                   {
			if (result)
			{
				success = true;
				Debug.Log ("Successfully reported score for leaderboard " + leaderboardId);
			}
			else
			{
				success = false;
				Debug.LogWarning ("Failed to report score for leaderboard " + leaderboardId);
			}
		});
		
		return success;
	}
		
	// checks if the user has been authenticated.
	private bool isAuthenticated {
		get {
			return Social.localUser.authenticated;
		}
	}
	
	void Update() {
		if (canTryAuthentication) {
			elapsedSinceLastTry += Time.unscaledDeltaTime;
			
			// retry after 30 seconds
			if (elapsedSinceLastTry > 30f) {
				elapsedSinceLastTry = 0f;
				
				if (!isAuthenticated && !isAuthenticating) {
					// try to authenticate again if there is an internet connection.
					if (Application.internetReachability != NetworkReachability.NotReachable) {
						authenticate();
					}
				}
			}
		}

		ScoringState.isAuthenticated = isAuthenticated;
	}
}
