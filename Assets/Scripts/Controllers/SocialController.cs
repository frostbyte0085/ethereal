﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SocialPlatforms;
using Facebook.Unity;

public enum SocialMedia {
	Facebook,
	Twitter
}

public enum SocialResult {
	Success,
	Fail
}


public class FBScore {
	public long score;
	public int rank;
	public string username;
	public string id;
}

public static class FacebookState {
	public static bool isWorking;
	public static FBScore[] scores;
}

public class SocialController : MessageBehaviour {

	#region event listeners
	private void socialShareEventListener(ConvexEvent evt) {
		CESocialShare e = (CESocialShare)evt;

		if (e.Media == SocialMedia.Facebook) {
			shareFacebook();
		}
		else if (e.Media == SocialMedia.Twitter) {

		}
	}

	private void facebookLoginEventListener (ConvexEvent evt) {
		CEFacebookLogin e = (CEFacebookLogin)evt;

		fbLogin ("onLoginSuccess_FB");
	}

	private void reportScoreEventListener (ConvexEvent evt) {
		CEReportScore e = (CEReportScore)evt;

		reportScore (e.LeaderboardID, e.Score);
	}

	private void loadFacebookScoresEventListener (ConvexEvent evt) {
		CELoadFacebookScores e = (CELoadFacebookScores)evt;

		loadScores();
	}
	#endregion

	// Use this for initialization
	void Start() {
		facebookInit();
		twitterInit();

		registerEventListener (typeof(CESocialShare), socialShareEventListener);
		registerEventListener (typeof(CEFacebookLogin), facebookLoginEventListener);
		registerEventListener (typeof(CEReportScore), reportScoreEventListener);
		registerEventListener (typeof(CELoadFacebookScores), loadFacebookScoresEventListener);
	}
	
	#region Facebook
	private long desiredScore;
	private void reportScore(string id, long score) {
		if (FB.IsLoggedIn)
		{
			desiredScore = score;
			var query = new Dictionary<string, string>();
			FB.API("/me/scores", HttpMethod.GET, postScoreAfterValidation, query);
		}
	}

	private void loadScores() {
		if (FB.IsLoggedIn) {

			var query = new Dictionary<string, string>();
			FB.API("/" + FB.AppId + "/scores", HttpMethod.GET, loadScoresCallback, query);
			FacebookState.isWorking = true;
			FacebookState.scores = null;
		}
	}

	private void loadScoresCallback(IResult r) {
		FacebookState.isWorking = false;

		if (!string.IsNullOrEmpty(r.Error)) {
			Debug.Log ("FB Graph: Cannot load friend scores, server returned error");
			return;
		}
		
		IDictionary<string, object> data = r.ResultDictionary;

		List<FBScore> scores = new List<FBScore>();

		List<object> results = (List<object>)data["data"];
		if (results != null) {
			for (int i=0; i<results.Count; i++) {
				Dictionary<string, object> resultData = (Dictionary<string,object>)results[i];
				Dictionary<string, object> pd = (Dictionary<string, object>)resultData["user"];

				long score = long.Parse(resultData["score"].ToString());
				string username = pd["name"].ToString();
				string id = pd["id"].ToString();

				FBScore s = new FBScore();
				s.score = score;
				s.rank = (i+1);
				s.username = username;
				s.id = id;

				if (id == AccessToken.CurrentAccessToken.UserId) {
					long currentScore = long.Parse( PlayerPrefs.GetString ("current_max_score", "0") );
					if (score > currentScore) {
						ScoringState.currentMaxScore = score;
						PlayerPrefs.SetString ("current_max_score", score.ToString());
						PlayerPrefs.Save();
					}
				}

				scores.Add (s);
			}
		}

		FacebookState.scores = scores.ToArray();
	}

	private void postScoreAfterValidation(IResult r) {
		if (!string.IsNullOrEmpty(r.Error)) { 
			Debug.Log ("FB Graph: Cannot report score, server returned error");
			return;
		}

		IDictionary<string, object> data = r.ResultDictionary;

		List<object> results = (List<object>)data["data"];
		if (results != null && results.Count > 0) {
			Dictionary<string, object> myData = (Dictionary<string,object>)results[0];
			long score = long.Parse(myData["score"].ToString());

			Debug.Log ("FB Graph: wants to post score: " + desiredScore.ToString() + ", server score is: " + score.ToString());

			if (desiredScore > score) {
				reportNewScore (desiredScore);
			}
		}
		else {
			reportNewScore (desiredScore);
		}

	}

	private void reportNewScore (long score) {
		var query = new Dictionary<string, string>();
		query["score"] = score.ToString();
		FB.API("/me/scores", HttpMethod.POST, delegate(IGraphResult result) {Debug.Log ("FB Graph: " + result.RawResult);}, query);
	}

	private void onLoginSuccess_FB() {
		int alreadyGiven = PlayerPrefs.GetInt ("gave_fb", 0);

		if (alreadyGiven == 0) {
			Unibiller.CreditBalance ("Crystals", 250);

			postEvent (new CEPushPopup(typeof(UIFacebookLoginSuccessPopup)));

			PlayerPrefs.SetInt ("gave_fb", 1);
			PlayerPrefs.Save();
		}
	}

	private void facebookInit() {
		FB.Init (fbInitCallback, fbInitHideUnity);
	}
	
	private void fbInitCallback() {
		Debug.Log ("fb init success");

		postEvent (new CELoadFacebookScores());
	}

	private float prevTimeScale;
	private void fbInitHideUnity(bool isGameShown) {
			// why is like that i nthe tutorial?

		if (isGameShown)
			Time.timeScale = prevTimeScale;
		else {
			prevTimeScale = Time.timeScale;
			Time.timeScale = 0f;
		}
			
	}

	private void shareFacebook() {

		if (FB.IsLoggedIn) {
						/*
			FB.Shareli(
				toId: "",
				link: "",
				linkName: "name",
				linkCaption: "caption",
				linkDescription: "desc",
				picture: "",
				mediaSource: "",
				actionName: "",
				actionLink: "",
				reference: "",
				properties: null,
				callback: fbShareCallback
				
				);
				*/

				//FB.ShareLink (null,
		}
		else {
			fbLogin ("ShareFacebook");
		}
	}
	
		private void fbShareCallback (IResult res) {
		SocialResult result = SocialResult.Fail;
		if (res.Error == null) {
			result = SocialResult.Success;
		}

		// send event here
		postEvent (new CESocialShareResult (result, SocialMedia.Facebook));
	}
	
	private string currentOnLoginSuccess;
	private IResult lastLoginResult;
	private void fbLogin(string onSuccess) {
		if (!FB.IsLoggedIn) {
			currentOnLoginSuccess = onSuccess;
						FB.LogInWithPublishPermissions (new List<string>() { "publish_actions"}, fbLoginCallback);
		}
	}
	
	private void fbLoginCallback(IResult res) {
		lastLoginResult = res;
		if (res.Error == null && FB.IsLoggedIn) {
			if (!string.IsNullOrEmpty(currentOnLoginSuccess)) {
				SendMessage (currentOnLoginSuccess);

				postEvent (new CELoadFacebookScores());
			}
		}
		currentOnLoginSuccess = "";
	}
	#endregion
	
	
	#region Twitter
	private void twitterInit() {
		
	}
	
	public void ShareTwitter() {
		
	}
	#endregion
}
