﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum LocalNotificationType {
	Dummy,
	iOS,
	Android,
	WindowsPhone,
	Invalid
}

public class LocalNotifications_Backend {

	public LocalNotifications_Backend() {

	}

	public virtual LocalNotificationType Type {
		get {
			return LocalNotificationType.Invalid;
		}
	}
	
	public virtual void Schedule(string message, DateTime date, IDictionary userInfo=null) {
		
	}
	
	public virtual void CancelAll() {
		
	}
	
	public virtual int NotificationCount {
		get {
			return 0;
		}
	}
	
	public virtual IDictionary GetUserData (int notification) {
		return null;
	}
	
	public void Update() {
		
	}

}
