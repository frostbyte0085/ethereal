﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

#if UNITY_IPHONE
public class LocalNotifications_IOS: LocalNotifications_Backend {
	
	public LocalNotifications_IOS() {

	}

	public override LocalNotificationType Type {
		get {
			return LocalNotificationType.iOS;
		}
	}

	public override void Schedule(string message, DateTime date, IDictionary userInfo=null)
	{
		base.Schedule(message, date, userInfo);
		
		UnityEngine.iOS.LocalNotification ln = new UnityEngine.iOS.LocalNotification();
		
		ln.alertBody = message;
		ln.fireDate = date;
		ln.userInfo = userInfo;
		ln.soundName = "notification.wav";
		
		UnityEngine.iOS.NotificationServices.ScheduleLocalNotification (ln);
	}
	
	public override int NotificationCount
	{
		get
		{
			return UnityEngine.iOS.NotificationServices.localNotificationCount;
		}
	}
	
	public override IDictionary GetUserData(int notification)
	{
		if (notification < UnityEngine.iOS.NotificationServices.localNotificationCount) {
			return UnityEngine.iOS.NotificationServices.GetLocalNotification(notification).userInfo;
		}
		
		return null;
	}
	
	public override void CancelAll()
	{
		base.CancelAll();
		
		UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications();
	}
}
#endif