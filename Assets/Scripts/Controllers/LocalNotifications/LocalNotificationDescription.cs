﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class LocalNotificationDescription {
	
	public LocalNotificationDescription(string[] messages, int crystals, int repairKits, DateTime date, int hourVariation, bool isAbsolute) {

		// make sure the date is pointing in the future.
		if (isAbsolute) {
			if (DateTime.Now > date) {
				date = date.AddYears (1);
			}
		}			
		
		int randomHour = UnityEngine.Random.Range (0, hourVariation+1);
		date = date.AddHours ((double)randomHour);

		// 
		this.messages = messages;
		this.isAbsolute = isAbsolute;
		this.date = date;
		this.crystals = crystals;
		this.repairKits = repairKits;
	}

	public readonly string[] messages;
	public readonly DateTime date;
	public readonly bool isAbsolute;
	public readonly int crystals;
	public readonly int repairKits;
	
}
