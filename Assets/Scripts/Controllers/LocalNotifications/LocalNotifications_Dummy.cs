﻿using UnityEngine;
using System.Collections;

public class LocalNotifications_Dummy: LocalNotifications_Backend {

	public LocalNotifications_Dummy() {

	}

	public override LocalNotificationType Type {
		get {
			return LocalNotificationType.Dummy;
		}
	}
}
