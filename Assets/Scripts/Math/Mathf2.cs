using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
	
	
/**
 * Defines a world plane to use.
 */
public enum WorldPlane {
	XZ,
	XY,
	YZ
}

/**
 * \brief Extended mathematics class.
 */
public static class Mathf2 {
	
	private static Plane[] frustum;
		
	/**
	 * Gets the distance to travel in order to have an object of specific size to fill the screen with a specific camera vertical fov.
	 * @param fov the camera's vertical fov.
	 * @param aspect the camera's aspect ratio.
	 * @param size the object's size.
	 * @return the distance to travel.
	 */
	public static float DistanceForFOV (float fov, float aspect, Vector3 size) {

		float maxSize = Mathf.Max(size.x, Mathf.Max(size.y, size.z));
		float halfSize = maxSize * 0.5f;
		
		float horizontalFOV = (fov * Mathf.Deg2Rad) * aspect;//2 * Mathf.Atan(Mathf.Tan(fov * 0.5f) * aspect);
		
		float distance = halfSize / (horizontalFOV * 0.5f);
		
		return distance;
	}
	
	/**
	 * Gets the camera field of view to encompass the entire object's size, for a given distance.
	 * @param distance the distance between the camera and the target object.
	 * @param aspect the camera's aspect ratio.
	 * @param size the object's size.
	 * @return the camera field of view.
	 */
	public static float FOVForDistance(float distance, float aspect, Vector3 size) {
		float maxSize = Mathf.Max(size.x, Mathf.Max(size.y, size.z));
		
		float halfHorizontalFOV = Mathf.Atan ( (maxSize * 0.5f) / distance );
		
		float horizontalFOV = halfHorizontalFOV * 2.0f * Mathf.Rad2Deg;
		return horizontalFOV * 1.0f / aspect;
	}
	
	/**
	 * Scales a value to 0..1 range between min and max values.
	 * @param val the value to scale.
	 * @param min the minimum margin.
	 * @param max the maximum margin.
	 * @return the scaled value.
	 */
	public static float ScaleRange (float val, float min, float max) {
		return (val - min) / (max - min);	
	}
	
	/**
	 * Projects a vector on a plane.
	 * @param point the vector to project.
	 * @param p the plane
	 * @return the projected vector.
	 */
	
	public static Vector3 ProjectToPlane(Vector3 point, Plane p) {
		float distanceFromPlane = p.GetDistanceToPoint(point);
		return point - p.normal * distanceFromPlane;
	}
	
	
	/**
	 * Calculate the frustum planes based on the main camera.
	 */
	public static void CalculateFrustumPlanes() {
		if (Camera.main != null) {
			frustum = GeometryUtility.CalculateFrustumPlanes(Camera.main);
		}
	}
	
	/**
	 * Checks if a point is in the frustum. Additionally, it checks if the point is within the minimum and maximum distance from the camera, and it can expand the point
	 * to resemble a box for larger area testing.
	 * @param point the point to be checked.
	 * @param cameraPosition camera position.
	 * @param minDistance the minimum distance from the camera.
	 * @param maxDistance the maximum distance from the camera.
	 * @param expandPoint how much the point should be expanded.
	 * @return True if the point is in the frustum, false otherwise.
	 */
	public static bool IsPointInFrustum(Vector3 point, Vector3 cameraPosition, float minDistance, float maxDistance, Vector3 expandPoint) {
		float min = minDistance * minDistance;
		float max = maxDistance * maxDistance;
		
		Vector3 delta = point - cameraPosition;
		float dist = delta.sqrMagnitude;
		
		if (dist < min || dist > max) return false;

		Bounds bounds = new Bounds(point, expandPoint);
		return GeometryUtility.TestPlanesAABB(FrustumPlanes, bounds);
	}
	
	/**
	 * Checks if a point is in the frustum. Additionally, it checks if the point is within the minimum and maximum distance from the camera.
	 * @param point the point to be checked.
	 * @param cameraPosition camera position.
	 * @param minDistance the minimum distance from the camera.
	 * @param maxDistance the maximum distance from the camera.
	 * @return True if the point is in the frustum, false otherwise.
	 */
	public static bool IsPointInFrustum(Vector3 point, Vector3 cameraPosition, float minDistance, float maxDistance) {
		float min = minDistance * minDistance;
		float max = maxDistance * maxDistance;
		
		Vector3 delta = point - cameraPosition;
		float dist = delta.sqrMagnitude;
		
		if (dist < min || dist > max) return false;

		foreach (Plane f in frustum) {
			if (point.x * f.normal.x + point.y * f.normal.y + point.z * f.normal.z + f.distance <= 0)
				return false;
		}
		return true;
	}
	
	/**
	 * Checks if a point is in the frustum.
	 * @param point the point to be checked.
	 * @return True if it is in the frustum, false otherwise.
	 */
	public static bool IsPointInFrustum(Vector3 point) {
		
		foreach (Plane f in frustum) {
			if (point.x * f.normal.x + point.y * f.normal.y + point.z * f.normal.z + f.distance <= 0)
				return false;
		}
		return true;
	}

	public static bool IsSphereInFrustum(Vector3 center, float radius) {

		foreach (Plane f in frustum) {
			if (center.x * f.normal.x + center.y * f.normal.y + center.z * f.normal.z + f.distance <= -radius)
				return false;
		}
		return true;
	}
	
	/**
	 * Gets the previously calculated frustum planes.
	 * @return the array of planes.
	 */
	public static Plane[] FrustumPlanes {
		get {
			return frustum;
		}
	}
	
	/**
	 * Checks if any of the components in a quaternion is NaN.
	 * True if NaN, False otherwise.
	 */
	public static bool IsNaN(Quaternion q) {
		bool nx = float.IsNaN(q.x);
		bool ny = float.IsNaN(q.y);
		bool nz = float.IsNaN(q.z);
		bool nw = float.IsNaN(q.w);
		
		return nw || nz || ny || nx;
	}
	
	/**
	 * Checks if any of the components in a vector is NaN.
	 * True if NaN, False otherwise.
	 */
	public static bool IsNaN(Vector3 v) {
		bool nx = float.IsNaN(v.x);
		bool ny = float.IsNaN(v.y);
		bool nz = float.IsNaN(v.z);
		
		return nx || ny || nz;
	}
	
	/**
	 * Checks if any of the components in a quaternion is Infinity.
	 * True if Infinity, False otherwise.
	 */
	public static bool IsInfinity(Quaternion q) {
		bool nx = float.IsInfinity(q.x);
		bool ny = float.IsInfinity(q.y);
		bool nz = float.IsInfinity(q.z);
		bool nw = float.IsInfinity(q.w);
		
		return nw || nz || ny || nx;
	}
	
	public static bool IsZero(Quaternion q) {
		bool nx = Mathf.Approximately(q.x, 0.0f);
		bool ny = Mathf.Approximately(q.y, 0.0f);
		bool nz = Mathf.Approximately(q.z, 0.0f);
		bool nw = Mathf.Approximately(q.w, 0.0f);
		
		return nw || nz || ny || nx;
	}
	
	/**
	 * Clamps a colour between a minimum and maximum colour value.
	 * @param c the input colour.
	 * @param min the minimum colour value.
	 * @param max the maximum colour value.
	 * @see Mathf2
	 * @return the clamped colour.
	 */
	public static Color ClampColor(Color c, Color min, Color max) {
		Color ret = c;
		
		ret.r = Mathf.Clamp(ret.r, min.r, max.r);
		ret.g = Mathf.Clamp(ret.g, min.g, max.g);
		ret.b = Mathf.Clamp(ret.b, min.b, max.b);
		ret.a = Mathf.Clamp(ret.a, min.a, max.a);
		
		return ret;
	}
	
	/**
	 * Returns the signed angle between two quaternions relative to a plane.
	 * @param q1 The first quaternion.
	 * @param q2 The second quaternion.
	 * @param plane The plane that the test is done against.
	 * @see WorldPlane
	 * @return The signed angle.
	 */
	public static float SignedAngle(Quaternion q1, Quaternion q2, WorldPlane plane) {
		Vector3 forwardA = q1 * Vector3.forward;
		Vector3 forwardB = q2 * Vector3.forward;
		
		// get a numeric angle for each vector, on the X-Z plane (relative to world forward)
		float angleA=0.0f;
		float angleB=0.0f;
		
		if (plane == WorldPlane.XZ) {
			angleA = Mathf.Atan2(forwardA.x, forwardA.z) * Mathf.Rad2Deg;
			angleB = Mathf.Atan2(forwardB.x, forwardB.z) * Mathf.Rad2Deg;
		}
		else if (plane == WorldPlane.XY) {
			angleA = Mathf.Atan2(forwardA.x, forwardA.y) * Mathf.Rad2Deg;
			angleB = Mathf.Atan2(forwardB.x, forwardB.y) * Mathf.Rad2Deg;
		}
		else if (plane == WorldPlane.YZ) {
			angleA = Mathf.Atan2(forwardA.y, forwardA.z) * Mathf.Rad2Deg;
			angleB = Mathf.Atan2(forwardB.y, forwardB.z) * Mathf.Rad2Deg;
		}
		
		// get the signed difference in these angles
		return Mathf.DeltaAngle( angleA, angleB );
	}
	
	/**
	 * Finds the shortest angle between two angles to rotate.
	 * @param angle1 first angle.
	 * @param angle2 second angle.
	 * @param sign the direction to rotate.
	 * @return the angle.
	 */
	public static float ShortestAngle(float angle1, float angle2, out int sign) {
		float difference = Mathf.Abs(angle2 - angle1) % 360;
		
		sign = -1;
		
		if (difference > 180) {
			difference = 360 - difference;
			sign = 1;
		}
		
		return difference;
	}
	
	public static float ShortestAngle(float angle1, float angle2) {
		// Another way: :)
		/*
		angle1 = Mathf.Deg2Rad * angle1;
		angle2 = Mathf.Deg2Rad * angle2;
		
		float diffAngle = Mathf.Atan2(Mathf.Sin(angle2 - angle1), Mathf.Cos(angle2 - angle1));
		
		return diffAngle * Mathf.Rad2Deg;
		*/
		float difference = angle2 - angle1;
        while (difference < -180) difference += 360;
        while (difference > 180) difference -= 360;
        return difference;
	}
	
	public static bool CatmullRom(List<Vector3> path, out List<Vector3> transformedPath, int samples) {
		// catmull rom requires minimum of 4 points
		if (path.Count < 4) {
			transformedPath = null;
			return false;
		}
		
		transformedPath = new List<Vector3>();
		
		for (int n=1; n<path.Count-2; n++) {
			for (int i=0; i<samples; i++) {
				transformedPath.Add(PointOnCurve(path[n - 1], path[n], path[n + 1], path[n + 2], (1f / samples) * i ));
			}
		}
		transformedPath.Add(path[path.Count - 2]);
		
		return true;
	}
	
	public static Vector3 PointOnCurve (Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t) {
		Vector3 result = new Vector3();
 
		float t0 = ((-t + 2f) * t - 1f) * t * 0.5f;
		float t1 = (((3f * t - 5f) * t) * t + 2f) * 0.5f;
		float t2 = ((-3f * t + 4f) * t + 1f) * t * 0.5f;
		float t3 = ((t - 1f) * t * t) * 0.5f;
	 
		result.x = p0.x * t0 + p1.x * t1 + p2.x * t2 + p3.x * t3;
		result.y = p0.y * t0 + p1.y * t1 + p2.y * t2 + p3.y * t3;
		result.z = p0.z * t0 + p1.z * t1 + p2.z * t2 + p3.z * t3;
	 
		return result;	
	}

	public static Vector3 CubicInterpolate (Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4, float t) {

		float t2 = t * t;
		float t3 = t2 * t;

		Vector3 cp0 = p4 - p3 - p1 + p2;
		Vector3 cp1 = p1 - p2 - cp0;
		Vector3 cp2 = p3 - p1;
		Vector3 cp3 = p2;

		return cp0 * t3 + cp1 * t2 + cp2 * t + cp3;
	}
	
	public static Vector3 CatmulRomInterpolate (Vector3 previous, Vector3 start, Vector3 end, Vector3 next, float t) {

		float percentComplete = t;
		float percentCompleteSquared = t*t;
		float percentCompleteCubed = percentCompleteSquared * t;
		
		return previous * (-0.5f * percentCompleteCubed +
		                   percentCompleteSquared -
		                   0.5f * percentComplete) +
				start   * ( 1.5f * percentCompleteCubed +
			       		    -2.5f * percentCompleteSquared + 1.0f) +
				end     * (-1.5f * percentCompleteCubed +
				            2.0f * percentCompleteSquared +
				           0.5f * percentComplete) +
				next    * ( 0.5f * percentCompleteCubed -
				            0.5f * percentCompleteSquared);

	}
	
	/**
	 * Clamps an angle between min and max.
	 * @return the clamped angle.
	 */
	public static float ClampAngle(float angle, float min, float max) {
		if (angle < -360)
		    angle += 360;
		if (angle > 360)
		    angle -= 360;
		return Mathf.Clamp (angle, min, max);
	}
	
	
	public static float SmoothStep (float min, float max, float val) {
		
		float x = Mathf.Clamp01((val - min)/(max - min)); 
	    // Evaluate polynomial
	    return x*x*(3.0f - 2.0f*x);
	}
	
	public static float Hermite(float start, float end, float value)
    {
        return Mathf.Lerp(start, end, value * value * (3.0f - 2.0f * value));
    }
 
    public static float Sinerp(float start, float end, float value)
    {
        return Mathf.Lerp(start, end, Mathf.Sin(value * Mathf.PI * 0.5f));
    }
 
    public static float Coserp(float start, float end, float value)
    {
        return Mathf.Lerp(start, end, 1.0f - Mathf.Cos(value * Mathf.PI * 0.5f));
    }
 
    public static float Berp(float start, float end, float value)
    {
        value = Mathf.Clamp01(value);
        value = (Mathf.Sin(value * Mathf.PI * (0.2f + 2.5f * value * value * value)) * Mathf.Pow(1f - value, 2.2f) + value) * (1f + (1.2f * (1f - value)));
        return start + (end - start) * value;
    }
	
	public static float Lerp(float start, float end, float value)
    {
        return ((1.0f - value) * start) + (value * end);
    }
 
    public static Vector3 NearestPoint(Vector3 lineStart, Vector3 lineEnd, Vector3 point)
    {
        Vector3 lineDirection = Vector3.Normalize(lineEnd-lineStart);
        float closestPoint = Vector3.Dot((point-lineStart),lineDirection)/Vector3.Dot(lineDirection,lineDirection);
        return lineStart+(closestPoint*lineDirection);
    }
 
    public static Vector3 NearestPointStrict(Vector3 lineStart, Vector3 lineEnd, Vector3 point)
    {
        Vector3 fullDirection = lineEnd-lineStart;
        Vector3 lineDirection = Vector3.Normalize(fullDirection);
        float closestPoint = Vector3.Dot((point-lineStart),lineDirection)/Vector3.Dot(lineDirection,lineDirection);
        return lineStart+(Mathf.Clamp(closestPoint,0.0f,Vector3.Magnitude(fullDirection))*lineDirection);
    }
    public static float Bounce(float x) {
        return Mathf.Abs(Mathf.Sin(6.28f*(x+1f)*(x+1f)) * (1f-x));
    }
 
    // test for value that is near specified float (due to floating point inprecision)
    // all thanks to Opless for this!
    public static bool Approx(float val, float about, float range) {
        return ( ( Mathf.Abs(val - about) < range) );
    }
 
    // test if a Vector3 is close to another Vector3 (due to floating point inprecision)
    // compares the square of the distance to the square of the range as this 
    // avoids calculating a square root which is much slower than squaring the range
    public static bool Approx(Vector3 val, Vector3 about, float range) {
        return ( (val - about).sqrMagnitude < range*range);
    }
 
   /*
     * CLerp - Circular Lerp - is like lerp but handles the wraparound from 0 to 360.
     * This is useful when interpolating eulerAngles and the object
     * crosses the 0/360 boundary.  The standard Lerp function causes the object
     * to rotate in the wrong direction and looks stupid. Clerp fixes that.
     */
    public static float Clerp(float start , float end, float value) {
        float min = 0.0f;
        float max = 360.0f;
        float half = Mathf.Abs((max - min)/2.0f);//half the distance between min and max
        float retval = 0.0f;
        float diff = 0.0f;
 
        if((end - start) < -half){
            diff = ((max - start)+end)*value;
			retval =  start+diff;
        }
        else if((end - start) > half){
            diff = -((max - end)+start)*value;
            retval =  start+diff;
        }
        else retval =  start+(end-start)*value;

    return retval;
    }
	
	/*
     * FeedbackLerpFactor
	 * This is a framerate independent lerp factor calculator
     */
	public static float FeedbackLerpFactor(float lerpVal, float targetFPS, float currentFPS) {
		return 1.0f - Mathf.Pow(1.0f - lerpVal, targetFPS / currentFPS);
	 }
}
