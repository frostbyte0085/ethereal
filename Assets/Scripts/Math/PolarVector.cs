using UnityEngine;
using System.Collections;

public class PolarVector {

	public float rho = 0.0f;
	public float theta = 0.0f;

	public PolarVector(Vector3 v) {
		rho = v.magnitude;
		theta = (float)Mathf.Atan2(v.z, v.x);
	}
	
	public Vector3 ToVector3() {
		return new Vector3(rho * (float)Mathf.Cos(theta), 0.0f, 
			rho * (float)Mathf.Sin(theta));
	}
}