﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Loading : MessageBehaviour {
	
	public static bool loadingStarted = false;

	public dfPanel vignettes;
	public dfTextureSprite logoTexture;
	public dfTextureSprite blackTexture;

	public dfPanel mainPanel;

	public dfLabel statusLabel;
	public dfProgressBar[] loadingbars;


	// GameAnalytics
	private const string GA_GameKey = "b338de86298c517b6d2254c52727326b";
	private const string GA_SecretKey = "2ec938f00d88aaa589774b5769720d5bdf0d82cc";
	//

	private void updateStatus (float percent, string label) {
		statusLabel.Text = label;
		foreach (dfProgressBar bar in loadingbars) {
			bar.Value = percent;
		}
	}


	// Use this for initialization
	IEnumerator Start () {

		vignettes.enabled = false;

		PlatformQuery.Initialize();

		loadingStarted = true;

		blackTexture.gameObject.SetActive (true);
		
		yield return new WaitForSeconds (4.0f);

		logoTexture.gameObject.SetActive (true);

		// do some logo fading out
		float elapsed = 0f;
		while (elapsed < 1f) {
			elapsed += Time.unscaledDeltaTime;

			logoTexture.Opacity = Mathf.Lerp (0f, 1f, elapsed);
			yield return null;
		}

		yield return new WaitForSeconds(2f);

		// do some logo fading out
		elapsed = 0f;
		while (elapsed < 1f) {
			elapsed += Time.unscaledDeltaTime;

			logoTexture.Opacity = Mathf.Lerp (1f, 0f, elapsed);
			yield return null;
		}

		//

		yield return new WaitForSeconds (1f);

		vignettes.enabled = true;

		logoTexture.gameObject.SetActive (false);
		blackTexture.gameObject.SetActive (false);

		yield return null;

		CrystalGroupCache.Initialise();

		updateStatus (0.2f, "MAPPING SPACE STATION");
		Application.LoadLevelAdditive ("TilesStation");

		yield return null;

		updateStatus (0.4f, "DETECTING ASTEROIDS");
		Application.LoadLevelAdditive ("TilesAsteroid");

		yield return null;

		updateStatus (0.65f, "REGISTERING DANGER ZONES");
		Application.LoadLevelAdditive ("TilesFields");

		updateStatus (0.8f, "CALCULATING INTERSTELLAR ROUTES");
		Application.LoadLevelAdditive ("TilesCommon");

		yield return null;

		updateStatus (1f, "INITIALISING MAINFRAME");
		Application.LoadLevelAdditive ("Game");

		// wait a frame to send the event
		yield return null;

		yield return new WaitForSeconds(1f);

		postEvent (new CEFade (FadeMode.Out, 2f, null, fadeFinished));
	}

	void fadeFinished(FadeMode mode) {
		postEvent (new CEScreenDistortion());
		Destroy (gameObject);
	}

}
