using System.Collections;
using UnityEngine;

public class Repair : Collectable {

	protected override void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == TagStringManager.Spaceship) {
			RunInstance.NumPowerups++;

			PooledInstance fx = ObjectPoolManager.Instance.Spawn ("GenericPool", "repair_collect");
			if (fx != null) {
				fx.CachedTransform.parent = other.transform;
				fx.CachedTransform.localPosition = Vector3.zero;
			}

			Messenger.instance.postEvent (new CERepairCollect());
		}

		base.OnTriggerEnter2D (other);
	}

}