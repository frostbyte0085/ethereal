using System.Collections;
using UnityEngine;

public class Magnet : Collectable {

	protected override void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == TagStringManager.Spaceship) {
			Spaceship ship = other.gameObject.GetComponent<Spaceship>();
			if (ship.InWarpdrive) return; // avoid combining warpdrive and magnet, warpdrive gets priority

			RunInstance.NumPowerups++;

			PooledInstance fx = ObjectPoolManager.Instance.Spawn ("GenericPool", "magnet_collect");
			if (fx != null) {
				fx.CachedTransform.parent = other.transform;
				fx.CachedTransform.localPosition = Vector3.zero;
			}

			Messenger.instance.postEvent (new CEMagnetCollect(MagnetPhase.Start, ship.MagnetDuration));
		}

		base.OnTriggerEnter2D (other);
	}

}