using System.Collections;
using UnityEngine;

public class Warpdrive : Collectable {

	protected override void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == TagStringManager.Spaceship) {
			Spaceship ship = other.gameObject.GetComponent <Spaceship>();
		
			RunInstance.NumPowerups++;

			PooledInstance fx = ObjectPoolManager.Instance.Spawn ("GenericPool", "warpdrive_collect");
			if (fx != null) {
				fx.CachedTransform.parent = other.transform;
				fx.CachedTransform.localPosition = Vector3.zero;
			}

			Messenger.instance.postEvent (new CEWarpdriveCollect(WarpdrivePhase.Start, ship.WarpdriveDuration));
		}

		base.OnTriggerEnter2D (other);
	}

}