﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Collectable : MessageBehaviour {
	
	protected Transform cachedTrans;

	protected SpriteRenderer[] renderers;

	protected Collider2D c2d;

	protected virtual void Awake() {
		cachedTrans = transform;

		renderers = GetComponentsInChildren<SpriteRenderer>(true);

		c2d = GetComponentInChildren<Collider2D>();
	}

	// Use this for initialization
	protected virtual void Start () {
		
	}
	
	// Update is called once per frame
	protected virtual void Update () {
	
	}

	protected virtual void OnEnable() {
		if (c2d != null) {
			c2d.enabled = true;
		}

		foreach (SpriteRenderer r in renderers) {
			r.enabled = true;
		}
	}

	protected virtual void OnTriggerEnter2D (Collider2D other) {

		if (other.tag == TagStringManager.Spaceship) {

			if (c2d != null) {
				c2d.enabled = false;
			}

			foreach (SpriteRenderer r in renderers) {
				r.enabled = false;
			}
		}
	}

	protected virtual void OnTriggerStay2D (Collider2D other) {

	}
}
