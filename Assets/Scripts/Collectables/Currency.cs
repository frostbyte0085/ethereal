using System.Collections;
using UnityEngine;

public enum CrystalType {
	Large,
	Normal
}

public class Currency : Collectable {

	public CrystalType type = CrystalType.Large;

	private CEDifficultyUpdate difficulty;
	private CESpaceshipStatus lastStatus;
	
	private const float maxZ=45f;
	private const float rotationSpeed = 3.5f;
	private float rotationPhase;

	private Transform spriteTrans;

	#region event listeners
	private void difficultyUpdateEventListener(ConvexEvent evt) {
		CEDifficultyUpdate e = (CEDifficultyUpdate)evt;

		difficulty = e;
	}

	private void spaceshipStatusEventListener(ConvexEvent evt) {
		CESpaceshipStatus e = (CESpaceshipStatus)evt;
		lastStatus = e;
	}
	#endregion

	protected override void Awake ()
	{
		base.Awake ();

		registerEventListener (typeof(CEDifficultyUpdate), difficultyUpdateEventListener);
		registerEventListener (typeof(CESpaceshipStatus), spaceshipStatusEventListener);

		spriteTrans = transform.GetChild (0);

		rotationPhase = Random.Range(0f, 10f);
	}

	void OnDisable() {
		isCollecting = false;
	}

	private float elapsed;
	protected override void Update ()
	{
		base.Update ();

		if (isCollecting) {
			spriteTrans.Rotate (new Vector3(0,0,1), Time.deltaTime * 650, Space.Self);
		}
		else {
			elapsed += Time.deltaTime * rotationSpeed;

			Vector3 euler = spriteTrans.localEulerAngles;
			euler.z = maxZ * Mathf.Sin (elapsed + rotationPhase);
			spriteTrans.localEulerAngles = euler;
		}
	}

	protected override void OnEnable ()
	{
		base.OnEnable ();

		elapsed = 0f;
	}

	protected override void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == TagStringManager.Spaceship) {

			bool doubleCrystals = (Unibiller.GetPurchaseCount("double_crystals")==1);

			Messenger.instance.postEvent (new CECrystalCollect(type));

			PooledInstance fx = null;
			if (type == CrystalType.Large) {
				postEvent (new CEDoSound(DoSoundAction.Play, "CrystalLarge"));

				if (doubleCrystals) {
					fx = ObjectPoolManager.Instance.Spawn ("GenericPool", "crystal_large_collect_double");
				}
				else {
					fx = ObjectPoolManager.Instance.Spawn ("GenericPool", "crystal_large_collect");
				}
			}
			else {
				postEvent (new CEDoSound(DoSoundAction.Play, "CrystalSmall"));

				if (doubleCrystals) {
					fx = ObjectPoolManager.Instance.Spawn ("GenericPool", "crystal_small_collect_double");
				}
				else {
					fx = ObjectPoolManager.Instance.Spawn ("GenericPool", "crystal_small_collect");
				}
			}

			if (fx != null) {
				fx.CachedTransform.parent = other.transform;
				fx.CachedTransform.localPosition = Vector3.zero;
			}
		}
		else if (other.tag == TagStringManager.Magnet) {

			if (!isCollecting) {
				StartCoroutine ("moveToTarget", other.transform);
			}
		}

		base.OnTriggerEnter2D (other);
	}

	private bool isCollecting;
	private IEnumerator moveToTarget(Transform target) {
		isCollecting = true;

		float elapsed=0f;
		float speedIncrease=1;

		while (true) {
			if (target == null)
				yield break;

			// if it hasn't reached on time, slowly increase the speed till it does.
			elapsed += Time.deltaTime;
			if (elapsed >= 1f) {
				speedIncrease += Time.deltaTime*4;
			}

			// hack to wait if the timescale is 0. if not, MoveTowards throws warnings which could slow down/crash the device.
			while (Time.timeScale < 0.001f) {
				yield return null;
			}

			// safety measure, this is required if the currency is moving to target while the player forfeits through the pause menu!
			if (target == null || cachedTrans == null)
				yield break;

			cachedTrans.position = Vector2.MoveTowards (cachedTrans.position, target.position, 0.03f * lastStatus.Velocity.x * speedIncrease);

			yield return null;
		}

		isCollecting = false;
	}


}