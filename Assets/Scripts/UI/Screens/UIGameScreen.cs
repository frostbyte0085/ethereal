﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIGameScreen : UIScreen {

	public dfPanel crystalsPanel; // used to punch the scale
	public dfPanel highscorePanel; // shown when the player reaches a new highscore

	public GameplayCountdown countdown;
	public dfPanel tutorialQueuePanel;
	public dfSprite warningSprite;
	public dfPanel warningPanel;

	public dfPanel topPanel;
	public dfPanel bottomPanel;
	public dfProgressBar[] infoBars;
	public dfLabel infoLabel;

	public dfLabel warningLabel;

	private CESpaceshipStatus lastSpaceshipStatus;

	private float healthVel;

	private bool hasShownHealthWarning;

	// these are exposed to the property binding
	[HideInInspector]
	public uint currentDistance;

	[HideInInspector]
	public uint currentCrystals;

	[HideInInspector]
	public float currentHealth;
	
	private TutorialQueue tutorialQueue;

	private CECurrentSaveData saveData;
	private dfTweenFloat tween;

	private bool hasFoundHigherScore;

	#region event listeners

	private void continueEventListener (ConvexEvent evt) {
		CEContinue e = (CEContinue)evt;

		focusFromContinue = true;
		warningPanel.gameObject.SetActive (false);
		StopCoroutine ("showWarning");

		StartCoroutine (enableHealthWarnings());
	}

	private void saveDataEventListener (ConvexEvent evt) {
		saveData = (CECurrentSaveData)evt;

		if (saveData.NeedsTutorial) {
			if (!tutorialQueuePanel.IsVisible) {
				tutorialQueuePanel.Show ();
			}

			if (topPanel.IsVisible) {
				topPanel.Hide();
			}
		}
		else {
			if (tutorialQueuePanel.IsVisible) {
				tutorialQueuePanel.Hide();
			}

			if (!topPanel.IsVisible) {
				topPanel.Show();
			}
		}
	}

	// hack:
	// wait a bit to allow for the healthbar to fill up. otherwise, the showWarning event is fired!
	private IEnumerator enableHealthWarnings() {
		yield return new WaitForSeconds (2f);

		hasShownHealthWarning = false;
	}

	private void spaceshipStatusEventListener (ConvexEvent evt) {
		lastSpaceshipStatus = (CESpaceshipStatus)evt;

		dfTweenFloat tween = warningSprite.GetComponent<dfTweenFloat>();
	}

	private void crystalCollectEventListener (ConvexEvent evt) {
		CECrystalCollect e = (CECrystalCollect)evt;

		if (e.Type == CrystalType.Large) {
			crystalsPanel.transform.localScale = Vector3.one * 1.75f;
		}
		else if (e.Type == CrystalType.Normal) {
			crystalsPanel.transform.localScale = Vector3.one * 1.35f;
		}
	}

	private void showWarningEventListener (ConvexEvent evt) {
		CEShowWarning e = (CEShowWarning)evt;

		warningPanel.gameObject.SetActive (false);
		warningLabel.Text = e.Title;

		postEvent (new CEDoSound(DoSoundAction.Play, e.Sound));

		StopCoroutine ("showWarning");
		StartCoroutine ("showWarning");
	}

	private IEnumerator showWarning() {

		// disable the highscore one
		StopCoroutine ("showHighscore");
		highscorePanel.gameObject.SetActive (false);

		const float warningFadeDuration = 0.3f;

		warningPanel.Opacity = 0f;
		warningPanel.gameObject.SetActive (true);

		float elapsed = 0f;
		while (elapsed < 1f) {
			elapsed += Time.deltaTime * 1f / warningFadeDuration;
			warningPanel.Opacity = Mathf.Lerp (0f, 1f, elapsed);
			yield return null;
		}

		elapsed = 0;

		yield return new WaitForSeconds (3f);

		while (elapsed < 1f) {
			elapsed += Time.deltaTime * 1f / warningFadeDuration;
			warningPanel.Opacity = Mathf.Lerp (1f, 0f, elapsed);
			yield return null;
		}

		warningPanel.gameObject.SetActive (false);
	}

	private void warpdriveInfoEventListener(ConvexEvent evt) {
		CEWarpdriveInfo e = (CEWarpdriveInfo)evt;

		if (e.Show) {
			infoLabel.Text = "WARPDRIVE ENABLED";

			StartCoroutine (fadeInfoPanelAlpha(true));
			StartCoroutine ("updateInfoBars", e.Duration);
		}
		else { 
			if (e.Force) {
				StopCoroutine ("updateInfoBars");
				bottomPanel.gameObject.SetActive (false);
			}
			else {
				if (bottomPanel.gameObject.activeSelf) {
					StartCoroutine (fadeInfoPanelAlpha(false));
					StopCoroutine ("updateInfoBars");
				}
			}
		}
	}

	private void magnetInfoEventListener(ConvexEvent evt) {
		CEMagnetInfo e = (CEMagnetInfo)evt;
		
		if (e.Show) {
			infoLabel.Text = "MAGNET ENABLED";
			
			StartCoroutine (fadeInfoPanelAlpha(true));
			StartCoroutine ("updateInfoBars", e.Duration);
		}
		else {
			if (e.Force) {
				StopCoroutine ("updateInfoBars");
				bottomPanel.gameObject.SetActive (false);
			}
			else {
				if (bottomPanel.gameObject.activeSelf) {
					StartCoroutine (fadeInfoPanelAlpha(false));
					StopCoroutine ("updateInfoBars");
				}
			}
		}
	}

	private IEnumerator updateInfoBars(float duration) {

		float timeStart = Time.timeSinceLevelLoad;
		float timeEnd = Time.timeSinceLevelLoad + duration;

		while (Time.timeSinceLevelLoad <= timeEnd) {

			foreach (dfProgressBar bar in infoBars) {
				bar.Value = 1f-Mathf.InverseLerp (timeStart, timeEnd, Time.timeSinceLevelLoad);
			}

			yield return null;
		}
	}

	private IEnumerator fadeInfoPanelAlpha (bool fadeIn) {

		if (fadeIn) {
			bottomPanel.gameObject.SetActive (true);
		}

		float elapsed = 0f;

		while (elapsed < 1f) {

			elapsed += Time.deltaTime * 3f;

			if (fadeIn) {
				bottomPanel.Opacity = Mathf.Lerp (0f, 1f, elapsed);
			}
			else {
				bottomPanel.Opacity = Mathf.Lerp (1f, 0f, elapsed);
			}

			yield return null;
		}

		if (!fadeIn) {
			bottomPanel.gameObject.SetActive (false);
		}

	}

	private void diedEventListener(ConvexEvent evt) {
		CEDied e = (CEDied)evt;

		StartCoroutine (fadeInfoPanelAlpha(false));
		StopCoroutine ("updateInfoBars");
	}
	#endregion

	protected override void Awake ()
	{
		base.Awake ();

		countdown.gameObject.SetActive (false);

		currentHealth = 1f;
		currentCrystals = 0;
		currentDistance = 0;

		warningPanel.gameObject.SetActive (false);
		warningSprite.Opacity = 0f;

		bottomPanel.gameObject.SetActive (false);

		tween = warningSprite.GetComponent<dfTweenFloat>();

		registerEventListener (typeof(CESpaceshipStatus), spaceshipStatusEventListener);
		registerEventListener (typeof(CECrystalCollect), crystalCollectEventListener);
		registerEventListener (typeof(CEWarpdriveInfo), warpdriveInfoEventListener);
		registerEventListener (typeof(CEMagnetInfo), magnetInfoEventListener);
		registerEventListener (typeof(CEDied), diedEventListener);
		registerEventListener (typeof(CEShowWarning), showWarningEventListener);
		registerEventListener (typeof(CEContinue), continueEventListener);
		registerEventListener (typeof(CECurrentSaveData), saveDataEventListener);

		// start with the tutorial hidden. we may not need to show this.
		tutorialQueuePanel.Hide();

		tutorialQueue = tutorialQueuePanel.GetComponent<TutorialQueue>();
	}

	void OnApplicationPause(bool paused) {

		return;

		bool spaceshipAlive = true;
		if (lastSpaceshipStatus != null) {
			spaceshipAlive = lastSpaceshipStatus.IsAlive;
		}

		if (!PauseState.isPaused && spaceshipAlive && !saveData.NeedsTutorial && (UIState.popupCount == 0)) {

			// treat this as LoseFocus
			if (paused) {
				if (!countdown.gameObject.activeSelf) {
					lostFocusLogic();
				}
			}
			else {
				StopCoroutine ("waitForSoundsAfterPause");
				StartCoroutine ("waitForSoundsAfterPause");
			}

			countdown.gameObject.SetActive (!paused);
		}
		
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();
	}

	protected override void Start ()
	{
		base.Start ();
	}

	private bool oldTopPanel;
	private bool oldBottomPanel;
	private bool oldWarningPanel;
	private bool oldHighscorePanel;

	private bool focusFromContinue; // this is true if the window gains focus from a continue event. in this case, we hide the warning panel

	private void lostFocusLogic() {
		oldTopPanel = topPanel.gameObject.activeSelf;
		oldBottomPanel = bottomPanel.gameObject.activeSelf;
		oldWarningPanel = warningPanel.gameObject.activeSelf;
		oldHighscorePanel = highscorePanel.gameObject.activeSelf;
		
		topPanel.gameObject.SetActive (false);
		bottomPanel.gameObject.SetActive (false);
		warningPanel.gameObject.SetActive (false);
		highscorePanel.gameObject.SetActive (false);
	}

	public override void OnLoseFocus ()
	{
		base.OnLoseFocus ();

		lostFocusLogic();
	}

	public override void OnGainFocus ()
	{
		base.OnGainFocus ();

		postEvent (new CECalibrateAccelerometer());

		if (PauseState.mustShowCountdown) {
			PauseState.mustShowCountdown = false;

			countdown.gameObject.SetActive (true);

			StopCoroutine ("waitForSoundsAfterPause");
			StartCoroutine("waitForSoundsAfterPause");
		}
		else {
			topPanel.gameObject.SetActive (oldTopPanel);
			bottomPanel.gameObject.SetActive (oldBottomPanel);
			warningPanel.gameObject.SetActive (oldWarningPanel && !focusFromContinue);
			highscorePanel.gameObject.SetActive (oldHighscorePanel);
		}

		focusFromContinue = false;
	}

	private IEnumerator waitForSoundsAfterPause() {
	
		yield return new WaitForSeconds (0.1f);

		while (Time.timeScale < 1f) {
			yield return null;
		}

		topPanel.gameObject.SetActive (oldTopPanel);
		bottomPanel.gameObject.SetActive (oldBottomPanel);
		warningPanel.gameObject.SetActive (oldWarningPanel);
		highscorePanel.gameObject.SetActive (oldHighscorePanel);

		AudioListener.volume = 1f;
		postEvent (new CEDoSound (DoSoundAction.ResumeComponent, "Sfx"));
	}

	public override void OnWillHide ()
	{
		base.OnWillHide ();
	}

	public override void OnWillShow ()
	{
		base.OnWillShow ();
	}

	public override void OnShow ()
	{
		base.OnShow ();
	}

	public override void OnHide ()
	{
		base.OnHide ();
	}

	void Update() {

		if (lastSpaceshipStatus == null)
			return;

		currentHealth = Mathf.SmoothDamp (currentHealth, lastSpaceshipStatus.Health, ref healthVel, 0.2f);
		currentHealth = Mathf.Clamp01 (currentHealth);

		float mag = (crystalsPanel.transform.localScale - Vector3.one).magnitude;
		if (mag > 0.01f) {
			crystalsPanel.transform.localScale = Vector3.Lerp(crystalsPanel.transform.localScale, Vector3.one, Time.deltaTime * 4f);
		}

		if (lastSpaceshipStatus.Health <= Constants.healthWarningLevel) {
			if (!hasShownHealthWarning && lastSpaceshipStatus.IsAlive) {
				hasShownHealthWarning = true;

				postEvent (new CEDoSound (DoSoundAction.Play, "CriticalEnergy"));
			}

			if (!tween.IsPlaying) {
				tween.Play();
			}
		}
		else {
			if (tween.IsPlaying) {
				tween.Stop();
				tween.Reset();
				if (warningSprite.Opacity > 0.001f) {
					warningSprite.Opacity = 0f;
				}
			}
		}

		currentCrystals = RunInstance.CrystalsCollected;
		currentDistance = RunInstance.DistanceTravelled;

		// calculate the current score. if it's greater than 0 and also greater than our current max score, inform the player
		// do this once per run.
		if (!hasFoundHigherScore && lastSpaceshipStatus.IsAlive ) {
			long score = RunInstance.CalculateScore();
			long currentScore = ScoringState.currentMaxScore;

			if ( (currentScore > 0) && (score > 0) && (score > currentScore) ) {
				Debug.Log ("Higher score found! " + score);

				ScoringState.currentMaxScore = score;
				PlayerPrefs.SetString ("current_max_score", score.ToString());
				PlayerPrefs.Save();

				postEvent (new CEDoSound (DoSoundAction.Play, "HighscoreNotification"));
				postEvent (new CEReportScore(Constants.leaderboardID, score)); // upload it

				StopCoroutine ("showHighscore");
				StartCoroutine ("showHighscore");

				hasFoundHigherScore = true;
			}
		}
	}

	private IEnumerator showHighscore() {

		// disable the warning panel
		StopCoroutine ("showWarning");
		warningPanel.gameObject.SetActive (false);

		highscorePanel.Opacity = 1f;
		highscorePanel.gameObject.SetActive (true);

		yield return new WaitForSeconds (0.5f);

		float elapsed = 0f;
		while (elapsed < 1f) {
			elapsed += Time.deltaTime;
			highscorePanel.Opacity = Mathf.Lerp (1f, 0f, elapsed);

			yield return null;
		}

		highscorePanel.gameObject.SetActive (false);
	}

	public void OnPauseClick( dfControl control, dfMouseEventArgs mouseEvent )
	{
		postEvent (new CEPushPopup (typeof(UIPausePopup)));
	}

}
