﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

public class UIMenuScreen : UIScreen {
	
	public float statUpgradePunchDampen = 1f;
	public Color statUpgradeBarColor = Color.white;

	public dfButton debugMenuButton;
	public Color flashStatColor = Color.green;
	public dfPanel shipRootPanel;
	public dfLabel crystalsLabel;
	public dfPanel unlockPanel;
	public dfLabel unlockCrystalPrice;
	public dfLabel comingSoonLabel;

	public dfButton fbButton;
	public dfButton twButton;
	public dfTextureSprite flickerFG;
	public dfSlicedSprite shipNameUnderline;
	public dfLabel shipName;
	public dfLabel shipDescription;
	public dfProgressBar agilityBar;
	public dfProgressBar durabilityBar;
	public dfProgressBar powerupBar;

	public dfButton storeButton;

	public dfButton agilityUpgradeButton;
	public dfButton durabilityUpgradeButton;
	public dfButton powerupUpgradeButton;

	public StartButton startButton;

	private SpaceshipMenu currentMenuShip;
	private CECurrentSaveData saveData;

	private static bool hasShownFBNotice=false;
	private static bool hasShownNotification=false;

	private static string lastShipName="";
	private static int lastShipIndex=0;

	// Use this for initialization
	protected override void Start () {
		base.Start();

		registerEventListener (typeof(CECurrentSaveData), saveDataEventListener);
		registerEventListener (typeof(CESocialShareResult), socialShareResultEventListener);
		registerEventListener (typeof(CEUpgradeStat), upgradeStatEventListener);

#if ETHEREAL_DEBUG
		debugMenuButton.gameObject.SetActive(true);
#else
		debugMenuButton.gameObject.SetActive(false);
#endif
	}

	public override void OnWillShow ()
	{
		base.OnWillShow ();

		postEvent (new CEDoScanlines(true));

		agilityUpgradeButton.IsEnabled = false;
		durabilityUpgradeButton.IsEnabled = false;
		powerupUpgradeButton.IsEnabled = false;

		doShipSelection (ChangeShipDirection.Default);

		unlockPanel.Opacity = 0f;
	}

	public override void OnShow ()
	{
		base.OnShow ();
	}

	public override void OnHide ()
	{
		base.OnHide ();

		postEvent (new CEDoScanlines(false));
	}

	private Queue<uint> instanceID = new Queue<uint>();
	private ulong frameCount=0;
	// Update is called once per frame
	void Update () {

		// wait until we have a valid save data for this screen
		if (currentMenuShip == null)
			return;

		if (Input.GetKeyDown(KeyCode.A)) {
			CEDoSound snd = new CEDoSound(DoSoundAction.Play, "LaserShoot");
			postEvent (snd);

			instanceID.Enqueue (snd.SoundInstanceID);
		}

		if (Input.GetKeyDown(KeyCode.S)) {
			if (instanceID.Count > 0) {
				CEDoSound snd = new CEDoSound(DoSoundAction.Stop, "LaserShoot");
				snd.SoundInstanceID = instanceID.Dequeue();

				postEvent (snd);
			}
		}

		frameCount++;

		// crystals!
		crystalsLabel.Text = Unibiller.GetCurrencyBalance ("Crystals").ToString();

		// update the sliders
		updateDetails (false);

		// see if we can upgrade stats for this ship. if not, disable the upgrade button
		if (currentMenuShip != null && saveData != null) {

			// horrible timing hack. delay it a bit so we have received both possible outcomes.	
			// when we have, prioritise local notification over FB notice.
			// my god.
			if (frameCount > 0) {
				// a very hacky queue system
				if (LocalNotificationState.mustShowRewardPopup) {

					if (!hasShownNotification) {
						postEvent (new CEPushPopup(typeof(UIWelcomeBackPopup)));
						hasShownNotification = true;
					}
				}
				else {
					if (saveData.IsFirstRun && !hasShownFBNotice) {
						if (!FB.IsLoggedIn) {
							hasShownFBNotice = true;
							postEvent (new CEPushPopup (typeof(UIFacebookPopup)));
						}
					}
				}
			}

			bool isUnlocked = getShipUnlock() == UnlockType.Unlocked;

			agilityUpgradeButton.IsEnabled = saveData.HasMoreUpgrades (currentMenuShip.details.type, StatType.Agility) && isUnlocked;
			durabilityUpgradeButton.IsEnabled = saveData.HasMoreUpgrades (currentMenuShip.details.type, StatType.Durability) && isUnlocked;
			powerupUpgradeButton.IsEnabled = saveData.HasMoreUpgrades (currentMenuShip.details.type, StatType.Powerup) && isUnlocked;

			updateStatButtonColor();
		}

	}

	private void upgradeStatEventListener(ConvexEvent evt) {
		CEUpgradeStat e = (CEUpgradeStat)evt;

		dfProgressBar bar = null;
		if (e.Stat == StatType.Agility) {
			bar = agilityBar;
		}
		else if (e.Stat == StatType.Durability) {
			bar = durabilityBar;
		}
		else if (e.Stat == StatType.Powerup) {
			bar = powerupBar;
		}

		StartCoroutine (punchUpgradedStat (bar));
	}

	private IEnumerator punchUpgradedStat (dfProgressBar statBar) {

		Color32 oldColor = statBar.ProgressColor;
		statBar.ProgressColor = statUpgradeBarColor;

		byte d=0;
		do {

			statBar.ProgressColor = Color32.Lerp (statBar.ProgressColor, oldColor, Time.deltaTime * statUpgradePunchDampen);
			d = (byte)Mathf.Abs((byte)(statBar.ProgressColor.g - oldColor.g));

			yield return null;

		} while (d>0);

		statBar.ProgressColor = oldColor;
	}

	private void updateDetails(bool snap) {

		if (saveData == null) return;

		SpaceshipDetails details = currentMenuShip.details;

		bool available = (currentMenuShip.details.unlock != UnlockType.Unavailable);

		shipName.Text = available ? details.shipName.ToUpper() : "????????";
		shipDescription.Text = available ? details.shipDescription.ToUpper() : "INTEL NOT AVAILABLE YET";

		float bonusAgility = saveData.GetStatBonus (details.type, StatType.Agility);
		float bonusDurability = saveData.GetStatBonus (details.type, StatType.Durability);
		float bonusPowerup = saveData.GetStatBonus (details.type, StatType.Powerup);

		float agility = available ? details.agility : 0.0f;
		float durability = available ? details.durability : 0.0f;
		float powerup = available ? details.powerup : 0.0f;

		if (snap) {
			agilityBar.Value = Mathf.Clamp01(agility + bonusAgility);
			durabilityBar.Value = Mathf.Clamp01(durability + bonusDurability);
			powerupBar.Value = Mathf.Clamp01(powerup + bonusPowerup);

			shipNameUnderline.Width = shipName.Width;

		}
		else {

			agilityBar.Value = Mathf.MoveTowards (agilityBar.Value, Mathf.Clamp01(agility + bonusAgility), 0.03f);
			durabilityBar.Value = Mathf.MoveTowards (durabilityBar.Value, Mathf.Clamp01(durability + bonusDurability), 0.03f);
			powerupBar.Value = Mathf.MoveTowards (powerupBar.Value, Mathf.Clamp01(powerup + bonusPowerup), 0.03f);

			shipNameUnderline.Width = Mathf.Lerp(shipNameUnderline.Width, shipName.Width, 0.06f);
		}

		UnlockType unlock = getShipUnlock();

		// update the state of the start button
		startButton.CurrentShipState = unlock;

		switch (unlock) {
		case UnlockType.Unlocked:
		case UnlockType.Unavailable:
			unlockPanel.Opacity = Mathf.MoveTowards (unlockPanel.Opacity, 0f, 0.1f);
			break;
		case UnlockType.Locked:
			unlockPanel.Opacity = Mathf.MoveTowards (unlockPanel.Opacity, 1f, 0.1f);
			break;
		}

	}

	// returns the ship unlock type if the ship is not unlocked, otherwise "unlocked".
	private UnlockType getShipUnlock() {

		if (saveData != null) {
			if (saveData.ShipData[(int)currentMenuShip.details.type].isUnlocked)
				return UnlockType.Unlocked;
		}

		return currentMenuShip.details.unlock;
	}

	// start new game
	void OnStartClick()
	{
		if (startButton.CurrentShipState == UnlockType.Unlocked) {
			postEvent (new CEGamePhase(GamePhaseMode.PreGameplay));
		}
		else {
			int needed = currentMenuShip.details.crystalsNeeded;
			postEvent (new CEPushPopup (typeof(UIGenericPopup)));
			postEvent (new CEPrepareGenericPopup("Unlock Ship", "Unlock " + currentMenuShip.details.shipName + " for " + needed.ToString() + " crystals?", 
			                                     "Yes!", "No, thanks",
			                                     onUnlockAccept, null));
		}
	}

	private void onUnlockAccept() {
		if (Unibiller.DebitBalance ("Crystals", currentMenuShip.details.crystalsNeeded)) {
			postEvent (new CESpaceshipUnlock (currentMenuShip.details.type));
		}
		else {
			postEvent (new CEPushPopup (typeof(UIGenericPopup)));
			postEvent (new CEPrepareGenericPopup("Crystals needed!", "Not enough crystals! Buy some at the store?", 
			                                     "Yes!", "No, thanks",
			                                     onCrystalsAccept, null));
		}
	}

	private void updateStatButtonColor() {

		if (saveData == null) return;

		int agilityIndex = saveData.NextUpgradeIndex (currentMenuShip.details.type, StatType.Agility);
		int durabilityIndex = saveData.NextUpgradeIndex (currentMenuShip.details.type, StatType.Durability);
		int powerupIndex = saveData.NextUpgradeIndex (currentMenuShip.details.type, StatType.Powerup);

		int balance = (int)Unibiller.GetCurrencyBalance ("Crystals");


		// this could have been much simpler.
		if ( (agilityIndex < currentMenuShip.details.statUpgradePrices.Length)) {
			int priceAgility = currentMenuShip.details.statUpgradePrices[agilityIndex];
			updateSingleStatButton (agilityUpgradeButton, priceAgility, balance, 2.0f);
		}
		else {
			if (agilityUpgradeButton.NormalBackgroundColor != Color.white) {
				setButtonColor (agilityUpgradeButton, Color.white);
			}
		}

		if ( (durabilityIndex < currentMenuShip.details.statUpgradePrices.Length)) {
			int priceDurability = currentMenuShip.details.statUpgradePrices[durabilityIndex];
			updateSingleStatButton (durabilityUpgradeButton, priceDurability, balance, 1f);
		}
		else {
			if (durabilityUpgradeButton.NormalBackgroundColor != Color.white) {
				setButtonColor (durabilityUpgradeButton, Color.white);
			}
		}

		if ( (powerupIndex < currentMenuShip.details.statUpgradePrices.Length)) {
			int pricePowerup = currentMenuShip.details.statUpgradePrices[powerupIndex];
			updateSingleStatButton (powerupUpgradeButton, pricePowerup, balance, 0f);
		}
		else {
			if (powerupUpgradeButton.NormalBackgroundColor != Color.white) {
				setButtonColor (powerupUpgradeButton, Color.white);
			}
		}

		// interpolate scale back to 1
		durabilityUpgradeButton.transform.localScale = Vector3.Lerp (durabilityUpgradeButton.transform.localScale, Vector3.one, Time.deltaTime * 5);
		agilityUpgradeButton.transform.localScale = Vector3.Lerp (agilityUpgradeButton.transform.localScale, Vector3.one, Time.deltaTime * 5);
		powerupUpgradeButton.transform.localScale = Vector3.Lerp (powerupUpgradeButton.transform.localScale, Vector3.one, Time.deltaTime * 5);
	}

	private void updateSingleStatButton (dfButton button, int price, int balance, float phase) {
		if (button.IsEnabled) {
			if (balance >= price) {
				float factor = Mathf.Cos(Time.time*3 + phase)*0.5f+0.5f;
				if (factor >= 0 && factor <= 0.002f) button.transform.localScale = Vector3.one * 1.35f;

				Color color = Color.Lerp(Color.white, flashStatColor, factor);
				setButtonColor (button, color);
			}
			else {
				setButtonColor (button, Color.white);
			}
		}
		else {
			setButtonColor (button, Color.white);
		}
	}

	private void setButtonColor(dfButton button, Color color) {
		button.NormalBackgroundColor = color;
		button.FocusBackgroundColor = color;
		button.HoverBackgroundColor = color;
		button.PressedBackgroundColor = color;
	}

	private void onCrystalsAccept() {
		
		postEvent (new CEPushPopup (typeof(UIStorePopup)));
	}

	// store button
	public void OnStoreClick( dfControl control, dfMouseEventArgs mouseEvent ) {
		postEvent (new CEPushPopup(typeof(UIStorePopup)));
	}

	public void OnSettingsClick( dfControl control, dfMouseEventArgs mouseEvent )
	{
		postEvent (new CEPushPopup(typeof(UISettingsPopup)));
	}

	public void OnLeadboardClick( dfControl control, dfMouseEventArgs mouseEvent )
	{
		postEvent (new CEPushPopup(typeof(UILeaderboardsPopup)));
	}

	// these are the next and previous ship selection buttons
	public void OnNextClick( dfControl control, dfMouseEventArgs mouseEvent )
	{
		doShipSelection (ChangeShipDirection.Next);
		
		postEvent (new CEDoSound(DoSoundAction.Play, "MenuLeftRight"));
	}

	public void OnPreviousClick( dfControl control, dfMouseEventArgs mouseEvent )
	{
		doShipSelection (ChangeShipDirection.Previous);
		
		postEvent (new CEDoSound(DoSoundAction.Play, "MenuLeftRight"));
	}

	private void selectNewShip (string shipName) {
		
		if (currentMenuShip != null) {
			DestroyImmediate (currentMenuShip.gameObject);
		}
		currentMenuShip = null;

		dfMaterialCache.Clear();
		Resources.UnloadUnusedAssets();

		GameObject go = Instantiate (Resources.Load (ResourceStringManager.MenuShipsPath + "/" + shipName)) as GameObject;
		dfSprite spr = go.GetComponent<dfSprite>();

		currentMenuShip = go.GetComponent<SpaceshipMenu>();
		currentMenuShip.transform.parent = shipRootPanel.transform;
		currentMenuShip.transform.localPosition = Vector3.zero;

		bool available = (currentMenuShip.details.unlock != UnlockType.Unavailable);

		if (!available) {
			spr.Color = Color.black;
		}

		unlockCrystalPrice.IsVisible = available;
		unlockPanel.IsVisible = available;
		comingSoonLabel.IsVisible = !available;
		startButton.gameObject.SetActive (available);

		unlockCrystalPrice.Text = currentMenuShip.details.crystalsNeeded.ToString();

		lastShipName = shipName;
	}

	private void doShipSelection(ChangeShipDirection direction) {

		ConvexResponse[] responses = null;

		postEvent (new CEChangeShip(direction), out responses);
		if (responses != null) {
			CRChangedShip e = (CRChangedShip)responses[0];

			if (direction == ChangeShipDirection.Default && !string.IsNullOrEmpty(lastShipName)) {
				postEvent (new CEChangeShip(lastShipName));
				selectNewShip (lastShipName);
			}
			else {
				selectNewShip (e.MenuShipResourceName);
			}

			// if it's "default", snap stats
			if (direction == ChangeShipDirection.Default) {
				StartCoroutine (updateDetails_Delayed());
			}
		}

	}

	private IEnumerator updateDetails_Delayed() {
		yield return new WaitForEndOfFrame();

		updateDetails (true);
	}
	
	// social following
	private bool hasLeft;
	void OnApplicationPause( bool paused) {
		hasLeft = paused;
	}

	public void DebugMenu(dfControl control, dfMouseEventArgs mouseEvent) {
		postEvent (new CEPushPopup(typeof(UIDebugPopup)));
	}

	public void FacebookLike(dfControl control, dfMouseEventArgs mouseEvent) {
		float startTime;
		startTime = Time.timeSinceLevelLoad;
		
		//open the facebook app
		Application.OpenURL(Constants.fbPageApp);
		
		if (Time.timeSinceLevelLoad - startTime <= 1f)
		{
			//fail. Open safari.
			Application.OpenURL(Constants.fbPageURL);
		}
	}
	
	public void TwitterFollow(dfControl control, dfMouseEventArgs mouseEvent) {
		float startTime;
		startTime = Time.timeSinceLevelLoad;
		
		//open the facebook app
		Application.OpenURL(Constants.twApp);
		
		if (Time.timeSinceLevelLoad - startTime <= 1f)
		{
			//fail. Open safari.
			Application.OpenURL(Constants.twURL);
		}
	}

	// stat upgrading callbacks for UI
	public void UpgradeAgility(dfControl control, dfMouseEventArgs mouseEvent) {
		showUpgrade (StatType.Agility);
	}

	public void UpgradeDurability(dfControl control, dfMouseEventArgs mouseEvent) {
		showUpgrade (StatType.Durability);
	}

	public void UpgradePowerup(dfControl control, dfMouseEventArgs mouseEvent) {
		showUpgrade (StatType.Powerup);
	}

	private void showUpgrade(StatType stat) {
		ShipSaveData sd = saveData.ShipData[(int)currentMenuShip.details.type];

		postEvent (new CEPushPopup (typeof(UIUpgradePopup)));
		postEvent (new CEPrepareUpgradePopup (currentMenuShip, stat, sd.statBonus[(int)stat]+1));
	}
	//

	#region event listeners
	private void socialShareResultEventListener(ConvexEvent evt) {
		CESocialShareResult e = (CESocialShareResult)evt;

		if (e.Result == SocialResult.Success) {
			postEvent (new CESpaceshipUnlock (currentMenuShip.details.type));
		}
	}

	private void saveDataEventListener (ConvexEvent evt) {
		saveData = (CECurrentSaveData)evt;
	}
	#endregion

}
