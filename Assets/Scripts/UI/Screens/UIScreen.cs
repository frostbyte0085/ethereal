using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScreen : MessageBehaviour {

	protected Transform cachedTrans;
	protected dfPanel rootPanel;


	protected virtual void Awake() {

		cachedTrans = transform;
		rootPanel = cachedTrans.GetComponent <dfPanel>();
	}

	public bool HasFocus {
		get; private set;
	}

	protected virtual void Start() {

	}

	public virtual void OnWillShow() {
		HasFocus = true;
	}

	public virtual void OnShow() {

	}

	public virtual void OnWillHide() {
		HasFocus = false;
	}

	public virtual void OnHide() {

	}

	public virtual void OnGainFocus() {
		HasFocus = true;
	}

	public virtual void OnLoseFocus() {
		HasFocus = false;
	}

}