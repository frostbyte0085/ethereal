﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIUpgradePopup : UIPopup {

	public dfSprite statIcon;
	public dfSprite underlineSprite;
	public dfLabel titleLabel;
	public dfLabel descriptionLabel;
	public dfLabel priceLabel;

	private readonly string[] titles = {"Agility Upgrade", "Durability Upgrade", "Powerup Upgrade"};
	private readonly string[] postfixDescriptions = {
		"Upgrade to Agility Level {0} to benefit from increased manuverability!",
		"Upgrade to Durability Level {0} to withstand more damage! Also replenishes a fraction of energy when the ship's health is low.",
		"Upgrade to Powerup Level {0} to increase the efficiency of the collected powerups!"};

	#region event listeners
	private void prepareEventListener(ConvexEvent evt) {
		CEPrepareUpgradePopup e = (CEPrepareUpgradePopup)evt;

		nextLevel = e.NextLevel;

		ship = e.Ship;
		stat = e.Stat;
		switch (stat) {
		case StatType.Agility:
			statIcon.SpriteName = "icon_agility";
			break;

		case StatType.Durability:
			statIcon.SpriteName = "icon_shield";
			break;

		case StatType.Powerup:
			statIcon.SpriteName = "icon_special_power";
			break;
		}
		statIcon.PerformLayout();

		int statIdx = (int)stat;

		descriptionLabel.Text = string.Format(postfixDescriptions[statIdx], nextLevel);
		titleLabel.Text = titles[statIdx];
		titleLabel.PerformLayout();

		price = ship.details.statUpgradePrices[nextLevel-1];
		priceLabel.Text = price.ToString();

		underlineSprite.Width = titleLabel.Width;
		underlineSprite.PerformLayout();
	}
	#endregion

	private StatType stat;
	private int nextLevel;
	private int price;
	private SpaceshipMenu ship;

	protected override void Awake ()
	{
		base.Awake ();

		registerEventListener (typeof(CEPrepareUpgradePopup), prepareEventListener);
	}

	void OnCancelClick()
	{
		// Add event handler code here
		Dismiss();
	}

	void OnAcceptClick()
	{
		if (Unibiller.DebitBalance ("Crystals", price)) {

			// Add event handler code here
			postEvent (new CESpendCrystals ((uint)price));
			postEvent (new CEUpgradeStat (ship.details.type, stat));

			Dismiss();
		}
		else {
			postEvent (new CEPushPopup (typeof(UIGenericPopup)));
			postEvent (new CEPrepareGenericPopup("Crystals needed!", "Not enough crystals! Buy some at the store?", 
			                                     "Yes!", "Later",
			                                     onPurchaseAccept, onPurchaseCancel));
		}
	}

	// callbacks for the crystals purchase confirmation box
	private void onPurchaseAccept() {

		postEvent (new CEPushPopup (typeof(UIStorePopup)));
	}

	private void onPurchaseCancel() {

	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();

		ship = null;
	}
}
