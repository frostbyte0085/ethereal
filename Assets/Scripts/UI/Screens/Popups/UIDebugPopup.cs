﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System;
using System.Threading;
using Facebook.Unity;

public class UIDebugPopup : UIPopup {

#if ETHEREAL_DEBUG
	const string fbAppToken = "352010831604522|SYFHhcTCHvkYkgaavD5sdcFmOSs";
#endif

	#region event listeners

	#endregion

	protected override void Awake ()
	{
		base.Awake ();
	}

	void OnMakeMeRich() {
		Unibiller.CreditBalance ("Crystals", 100000);
		Unibiller.CreditBalance ("RepairKits", 1000);
	}

	void OnDeleteFBScores() {

#if ETHEREAL_DEBUG
		if (FB.IsLoggedIn) {
			string url = "https://graph.facebook.com/" + FB.AppId + "/scores?access_token=" + fbAppToken;
			FB.API(url, Facebook.HttpMethod.DELETE, fbDeleteCallback);
		}

#endif
	}

	private void fbDeleteCallback (IResult result) {
		//Debug.Log ("Delete all scores: " + result.Text + ", " + result.Error);
	}

	void OnClearPrefs() {
		PlayerPrefs.DeleteAll();
		postEvent (new CESave());
	}

	void OnMaxOut() {


	}

}
