﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void OnGenericButtonClickedDelegate();

public class UIGenericPopup : UIPopup {

	public dfSprite underlineSprite;
	public dfLabel titleLabel;
	public dfLabel bodyLabel;

	public ComplexButton acceptButton;
	public ComplexButton cancelButton;

	private OnGenericButtonClickedDelegate onAccept;
	private OnGenericButtonClickedDelegate onCancel;

	#region event listeners
	private void prepareEventListener(ConvexEvent evt) {
		CEPrepareGenericPopup e = (CEPrepareGenericPopup)evt;

		titleLabel.Text = e.Title;
		bodyLabel.Text = e.Body;
		acceptButton.Text = e.AcceptText.ToUpper();
		cancelButton.Text = e.CancelText.ToUpper();

		underlineSprite.Width = titleLabel.Width;

		onAccept = e.OnAccept;
		onCancel = e.OnCancel;
	}
	#endregion

	protected override void Awake ()
	{
		base.Awake ();

		registerEventListener (typeof(CEPrepareGenericPopup), prepareEventListener);
	}

	public override void OnShow ()
	{
		base.OnShow ();
	}

	public override void OnHide ()
	{
		base.OnHide ();
	}

	void OnAcceptClick() {
		// Add event handler code here
		if (onAccept != null) {
			onAccept();
		}

		Dismiss();
	}

	void OnCancelClick() {
		// Add event handler code here
		if (onCancel != null) {
			onCancel();
		}

		Dismiss();
	}


}
