﻿using UnityEngine;
using System.Collections;

public class UIPurchaseResultPopup : UIPopup {

	public dfLabel infoLabel;

	public static string text;

	protected override void Awake ()
	{
		base.Awake ();

		infoLabel.Text = text;
		StartCoroutine (fade());
	}

	private IEnumerator fade() {
		yield return new WaitForSeconds(1f);

		float elapsed=0f;

		while (elapsed < 1f) {
			elapsed += Time.deltaTime;

			rootPanel.Opacity = Mathf.Clamp01(1f - elapsed);

			yield return null;
		}

		Dismiss();
	}
}
