﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIFacebookPopup : UIPopup {

	protected override void Awake ()
	{
		base.Awake ();
	}

	public override void OnWillShow ()
	{

		base.OnWillShow ();
	}

	public override void OnWillHide ()
	{

		base.OnWillHide ();
	}

	private void OnLoginClick() {
		postEvent (new CEFacebookLogin());
		Dismiss();
	}

}
