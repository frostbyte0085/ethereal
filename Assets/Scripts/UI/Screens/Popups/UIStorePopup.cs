﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System;
using System.Threading;

public class UIStorePopup : UIPopup {

	public int freeCrystalsAmount = 200;
	public ComplexButton freeCrystalsButton;

	public dfPanel loadingPanel;
	public dfLabel errorLabel;
	public dfPanel productsPanel;

	public ComplexButton disableAdsButton;
	public ComplexButton doubleCrystalsButton;

	public PriceLabel disableAdsPrice;
	public PriceLabel doublePrice;

	private CECurrentSaveData saveData;
	private bool isFirstTest;

	#region event listeners
	private void saveDataEventListener (ConvexEvent evt) {
		saveData = (CECurrentSaveData)evt;
	}

	private void iapResultEventListener (ConvexEvent evt) {
		CEIAPResult e = (CEIAPResult)evt;

		postEvent (new CEPopPopup());

		switch (e.Result) {
		case IAPResult.Cancelled:
			UIPurchaseResultPopup.text = "Purchase cancelled";
			postEvent (new CEPushPopup(typeof(UIPurchaseResultPopup)));
			break;

		case IAPResult.Failed:
			UIPurchaseResultPopup.text = "Purchase failed";
			postEvent (new CEPushPopup(typeof(UIPurchaseResultPopup)));
			break;

		case IAPResult.Success:
			if (lastID == "disableads") {
				postEvent (new CEDisableAds());
			}

			break;

		case IAPResult.RestoredPurchases:
			UIPurchaseResultPopup.text = "Restored purchases";
			postEvent (new CEPushPopup(typeof(UIPurchaseResultPopup)));

			// check if we have the disableads product, if so send the event
			if (Unibiller.GetPurchaseCount ("disableads") == 1) {
				postEvent (new CEDisableAds());
			}

			break;

		default:break;
		}

		lastID = "";
	}

	#endregion

	protected override void Awake ()
	{
		base.Awake ();

		registerEventListener (typeof(CEIAPResult), iapResultEventListener);
		registerEventListener (typeof(CECurrentSaveData), saveDataEventListener);

		//
		checkConnection();

		isFirstTest = true;
	}

	protected override void Start ()
	{
		base.Start ();

		//freeCrystalsButton.Text = freeCrystalsAmount + " FREE CRYSTALS!";
		loadingPanel.gameObject.SetActive (false);
	}

	void OnFreeCrystalsClick() {
		if (Application.internetReachability != NetworkReachability.ReachableViaLocalAreaNetwork) {
			postEvent (new CEPushPopup(typeof(UIRequiresWifiPopup)));
		}
		else {
			postEvent (new CEDoAd (AdActionType.ShowVideoForReward, freeCrystalsAmount, false));
		}
	}

	public void OnCancelClick( dfControl control, dfMouseEventArgs mouseEvent )
	{
		// Add event handler code here
		Dismiss();
	}
	
	private const float pingRetry = 3f;
	private bool isConnected=true;

	private bool isWaitingForInternet;
	private IEnumerator waitForInternet (string url, string key) {
		isWaitingForInternet = true;

		Debug.Log ("Testing connectivity using URL: " + url);
		WWW www = new WWW(url);

		yield return www;

		if (!string.IsNullOrEmpty(www.error)) {
			Debug.Log ("Error: " + www.error);
			isConnected = false;
		}
		else {

			string content = www.text;

			if (string.IsNullOrEmpty(content)) {
				Debug.Log ("Empty result");
				isConnected = false;
			}
			else if (!content.Contains (key)) {
				Debug.Log ("Doesn't contain " + key);
				isConnected = false;
			}
			else {
				Debug.Log ("Connected");
				isConnected = true;
			}

		}

		isWaitingForInternet = false;
	}

	private bool isTestingInternet;
	private IEnumerator testInternet() {
		isTestingInternet = true;

		if (isFirstTest) {
			loadingPanel.gameObject.SetActive (true);
		}

		StartCoroutine (waitForInternet("http://www.apple.com", "Apple"));

		while (isWaitingForInternet) {
			yield return null;
		}

		// try another server
		if (!isConnected) {
			StartCoroutine (waitForInternet("http://www.google.com", "schema.org/WebPage"));

			while (isWaitingForInternet) {
				yield return null;
			}
		}

		if (isFirstTest) {
			loadingPanel.gameObject.SetActive (false);
		}

		isTestingInternet = false;

		isFirstTest = false;
	}

	private void checkConnection() {

		StartCoroutine ("testInternet");
	}

	private float checkElapsed=pingRetry;

	void Update() {

		disableAdsButton.IsEnabled = !disableAdsPrice.IsPurchased;
		doubleCrystalsButton.IsEnabled = !doublePrice.IsPurchased;

		if (disableAdsPrice.IsPurchased) {

			disableAdsButton.SnapDown();
			disableAdsButton.GetComponent<dfButton>().State = dfButton.ButtonState.Pressed;
		}

		if (doublePrice.IsPurchased) {
			doubleCrystalsButton.SnapDown();
			doubleCrystalsButton.GetComponent<dfButton>().State = dfButton.ButtonState.Pressed;
		}

		checkElapsed += Time.deltaTime;
		if (checkElapsed >= pingRetry) {
			if (!isTestingInternet && Application.internetReachability != NetworkReachability.NotReachable) {
				checkConnection();
			}

			checkElapsed = 0f;
		}


		isConnected = isConnected && (Application.internetReachability != NetworkReachability.NotReachable);
		//
		if (Unibiller.Initialised && isConnected) {
			errorLabel.gameObject.SetActive (false);
			productsPanel.gameObject.SetActive (true);
		}
		else {
			errorLabel.gameObject.SetActive (true);
			productsPanel.gameObject.SetActive (false);
		}

		UnibillError[] errors = Unibiller.Errors;
		if (errors != null) {
			for (int i=0; i<errors.Length; i++) {
				Debug.Log ("ERROR: " + errors[i].ToString());
			}
		}
	}

	private IEnumerator delayPurchase(string id) {
		#if UNITY_EDITOR
		yield return new WaitForSeconds (2f);
		#else
		yield return null;
		#endif

		lastID = id;
		postEvent (new CEDoIAP (id));
	}

	private IEnumerator delayRestore() {
		#if UNITY_EDITOR
		yield return new WaitForSeconds (2f);
		#else
		yield return null;
		#endif

		Unibiller.restoreTransactions();
	}

	private string lastID;

	void OnCrystals() {
		postEvent (new CEPushPopup(typeof(UIPurchasingIAPPopup)));
		StartCoroutine (delayPurchase("crystals"));
	}

	void OnDoubleCrystals() {
		if (doublePrice.IsPurchased) {
			postEvent (new CEPushPopup(typeof(UIAlreadyPurchasedPopup)));
		}
		else {
			postEvent (new CEPushPopup(typeof(UIPurchasingIAPPopup)));
			StartCoroutine (delayPurchase("double_crystals"));
		}
	}

	void OnAds() {
		if (disableAdsPrice.IsPurchased) {
			postEvent (new CEPushPopup(typeof(UIAlreadyPurchasedPopup)));
		}
		else {
			postEvent (new CEPushPopup(typeof(UIPurchasingIAPPopup)));
			StartCoroutine (delayPurchase("disableads"));
		}
	}

	void OnLives() {
		postEvent (new CEPushPopup(typeof(UIPurchasingIAPPopup)));
		StartCoroutine (delayPurchase("lives"));
	}

	void OnRestore() {
		UIPurchasingIAPPopup.text = "Restoring purchases...";
		postEvent (new CEPushPopup(typeof(UIPurchasingIAPPopup)));
		StartCoroutine (delayRestore());
	}

}
