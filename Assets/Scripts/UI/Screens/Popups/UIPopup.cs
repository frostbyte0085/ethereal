﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIPopup : UIScreen {

	public bool distort=true;
	protected override void Awake ()
	{
		base.Awake ();
	}

	protected override void Start ()
	{
		base.Start ();
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();
	}

	public override void OnHide ()
	{
		base.OnHide ();

		postEvent (new CEDoSound(DoSoundAction.Play, "MenuOut"));
	}

	public override void OnShow ()
	{
		base.OnShow ();

		postEvent (new CEDoSound(DoSoundAction.Play, "MenuIn"));
		if (distort && QualityControlManager.CurrentSetting.distortionEffects) {
			postEvent (new CEScreenDistortion());
		}
	}

	public override void OnWillShow ()
	{
		base.OnWillShow ();
	}

	public override void OnWillHide ()
	{
		base.OnWillHide ();
	}

	public virtual void Dismiss() {
		DismissPending = true;
	}

	public virtual bool DismissPending {
		get; private set;
	}
}
