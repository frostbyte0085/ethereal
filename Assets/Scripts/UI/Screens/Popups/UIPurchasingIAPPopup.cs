﻿using UnityEngine;
using System.Collections;

public class UIPurchasingIAPPopup : UIPopup {

	public static string text = null;

	public dfLabel label;
	public dfSprite spinner;

	protected override void Awake ()
	{
		base.Awake ();

		if (!string.IsNullOrEmpty (text)) {
			label.Text = text;
		}
	}

	public override void OnWillHide ()
	{
		base.OnWillHide ();

		text = null;
	}

	void Update() {
		spinner.transform.Rotate (new Vector3(0,0,-1) * Time.deltaTime * 50);
	}

}
