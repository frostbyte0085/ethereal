﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIWelcomeBackPopup : UIPopup {

	public dfLabel descLabel;
	public dfLabel crystalsLabel;
	public dfLabel repairKitsLabel;

	private const string single = "WE FOUND SOME CRYSTALS AND A REPAIR KIT WHILE YOU WERE AWAY!";
	private const string multiply = "WE FOUND SOME CRYSTALS AND REPAIR KITs WHILE YOU WERE AWAY!";

	protected override void Awake ()
	{
		base.Awake ();
	}

	public override void OnWillShow ()
	{
		base.OnWillShow ();

		rootPanel.PerformLayout();

		int rk = LocalNotificationState.repairKits;

		crystalsLabel.Text = LocalNotificationState.crystals.ToString();
		repairKitsLabel.Text = LocalNotificationState.repairKits.ToString();

		if (rk > 1) {
			descLabel.Text = multiply;
		}
		else if (rk == 1) {
			descLabel.Text = single;
		}

		Unibiller.CreditBalance ("Crystals", LocalNotificationState.crystals);
		Unibiller.CreditBalance ("RepairKits", LocalNotificationState.repairKits);

		LocalNotificationState.Clear();
	}

	public override void OnWillHide ()
	{
		base.OnWillHide ();
	}
}
