﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIResultsPopup : UIPopup {
	
	public float adHeight=0.12f;

	public AnimationCurve crystalsRewardCurve;
	public AnimationCurve repairKitsRewardCurve;
	public float crystalsVariation=0.2f;
	public float repairKitsVariation=0.3f;

	public float scale = 12f;
	public float scaleDelay=10f;
	public float scaleWait=0.1f;

	public dfButton continueButton;

	public dfLabel distanceLabel;
	public dfLabel crystalsLabel;
	public dfLabel powerupsLabel;
	public dfLabel scoreLabel;
	public dfLabel crystalsRewardLabel;
	public dfLabel repairKitsRewardLabel;
	
	protected override void Awake ()
	{
		base.Awake ();

	}

	public override void OnWillShow ()
	{
		base.OnWillShow ();

		transform.localPosition = Vector3.zero;

		continueButton.IsEnabled = true;
		continueButton.IsVisible = false;

		postEvent (new CEGamePhase(GamePhaseMode.Results));

		StartCoroutine (doAnimation());

		distanceLabel.Text = RunInstance.DistanceTravelled.ToString() + " AU";
		crystalsLabel.Text = RunInstance.CrystalsCollected.ToString();
		powerupsLabel.Text = RunInstance.NumPowerups.ToString();

		long score = calcScore();

		scoreLabel.Text = score.ToString();

		// upload scores
		postEvent (new CEReportScore(Constants.leaderboardID, (long)score));

		// calc the rewards
		int crystalsReward = Mathf.RoundToInt(crystalsRewardCurve.Evaluate(RunInstance.Difficulty + crystalsVariation));
		int repairKitsReward = Mathf.RoundToInt(repairKitsRewardCurve.Evaluate(RunInstance.Difficulty + repairKitsVariation));

		repairKitsRewardLabel.Text = repairKitsReward.ToString();
		crystalsRewardLabel.Text = crystalsReward.ToString();

		// update the player's crystals
		Unibiller.CreditBalance ("Crystals", (decimal)(RunInstance.CrystalsCollected + crystalsReward));
		Unibiller.CreditBalance ("RepairKits", (decimal)repairKitsReward);
	}

	void Update() {

		postEvent (new CEDoScanlines(true)); // hack for showing the scanlines. why do they not appear when I post this in OnShow? Who knows...
	}

	private IEnumerator doAnimation() {

		dfLabel[] labels = {distanceLabel, crystalsLabel, powerupsLabel, scoreLabel, crystalsRewardLabel, repairKitsRewardLabel};

		foreach (dfLabel l in labels) {
			l.enabled = false;
		}

		int idx = 0;
		while (idx < labels.Length) {
			float elapsed = 0;

			dfLabel label = labels[idx];
			label.enabled = true;

			label.transform.localScale = Vector3.one * scale;
			while (elapsed < 0.35f) {

				elapsed += Time.deltaTime;
				label.transform.localScale = Vector3.Lerp (label.transform.localScale, Vector3.one, scaleDelay * Time.deltaTime);

				yield return null;
			}

			idx++;
		}

		continueButton.IsVisible = true;
	}

	private long calcScore() {

		long score = RunInstance.CalculateScore();
	
		long currentScore = long.Parse (PlayerPrefs.GetString ("current_max_score", "0"));
		if (score > currentScore) {
			ScoringState.currentMaxScore = score;
			PlayerPrefs.SetString ("current_max_score", score.ToString());
			PlayerPrefs.Save();
		}

		return score;
	}

	void OnContinueClick()
	{
		postEvent (new CEFade (FadeMode.Out, 1f, null, fadeResultsFinished));
	}

	private void fadeResultsFinished(FadeMode mode) {
		if (mode == FadeMode.Out) {
			postEvent (new CEGameRestart ());
		}
	}

}
