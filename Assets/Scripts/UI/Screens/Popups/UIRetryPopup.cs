﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIRetryPopup : UIPopup {

	public Color32 radialPunchColor;
	public dfButton reassembleButton;
	public dfRadialSprite countdownSprite;
	public dfLabel remainingLabel;
	public dfLabel costLabel;

	private float elapsed;

	private bool dismiss=false;
	private uint numRepairKits;

	private bool doCountdown;

	private const float countdownDuration = 3f;
	private static int[] repairKits = {1, 4, 12, 26, 48, 75};
	private static int repairKitIndex;

	private int targetRepairKits;

	private bool isReassembled;

	protected override void Awake ()
	{
		base.Awake ();
	}

	public override void OnWillShow ()
	{
		base.OnWillShow ();

		postEvent (new CEDoScanlines(true));

		repairKitIndex = (++repairKitIndex) % repairKits.Length;

		numRepairKits = (uint)Unibiller.GetCurrencyBalance ("RepairKits");
		targetRepairKits = repairKits[repairKitIndex];
		
		costLabel.Text = "Cost: " + repairKits[repairKitIndex] + " repair kits";
		doCountdown = true;
	}

	public override void OnShow ()
	{
		base.OnShow ();
	}

	public override void OnWillHide ()
	{
		postEvent (new CEDoScanlines(false));

		base.OnWillHide ();
	}

	public override void OnHide ()
	{
		base.OnHide ();

	}

	public static void Reset() {
		repairKitIndex = -1;
	}

	private bool waitForStore=false;
	private float punchElapsed;
	void Update() {

		string str = numRepairKits.ToString() + " repair kits left";
		// df kills us for this
		if (remainingLabel.Text != str) {
			remainingLabel.Text = str;
		}

		// and this
		if (elapsed < 1f && !waitForStore) {
			elapsed += Time.deltaTime * 1f / countdownDuration;

			punchElapsed += Time.deltaTime;
			if (punchElapsed >= 1f) {
				punchElapsed = 0f;

				countdownSprite.transform.localScale = Vector3.one * Mathf.Lerp(1, 2.5f, Mathf.InverseLerp(0, countdownDuration, elapsed));
				countdownSprite.Color = radialPunchColor;
			}

			countdownSprite.FillAmount = Mathf.Clamp01(1f-elapsed);

			if (elapsed >= 1f || dismiss) {
				end ();
			}
		}

		countdownSprite.transform.localScale = Vector3.Lerp (countdownSprite.transform.localScale, Vector3.one, 0.05f);
		countdownSprite.Color = Color32.Lerp (countdownSprite.Color, new Color32(255,255,255,255), 0.05f);
	}

	public void OnEmptyClick( dfControl control, dfMouseEventArgs mouseEvent )
	{
		dismiss = true;
	}

	public void OnReassembleClick( dfControl control, dfMouseEventArgs mouseEvent )
	{
		if (Unibiller.DebitBalance ("RepairKits", targetRepairKits)) {
			reassemble();
			
			Dismiss();
		}
		else {
			waitForStore = true;
			postEvent (new CEPushPopup (typeof(UIGenericPopup)));
			postEvent (new CEPrepareGenericPopup("Repair kit needed!", "Not enough repair kits! Buy some at the store?", 
			                                     "Yes!", "No, thanks",
			                                     onAccept, onCancel));

			doCountdown = false;
		}
	}

	private void end() {
		postEvent (new CEPushPopup(typeof(UIResultsPopup)));
		Dismiss();
	}

	private void reassemble() {
		isReassembled = true;

		PauseState.mustShowCountdown = false; // hack to force the countdown to not show... why?

		postEvent (new CESpendRepairKit());
		postEvent (new CEContinue());
	}

	public override void OnGainFocus ()
	{
		base.OnGainFocus ();

		waitForStore = false;

		// we assume that it's guaranteed to have enough repair kits. the user can only purchase the max amount at the store.
		// if the following funciton call fails, it means he didn't make a purchase.
		if (Unibiller.DebitBalance("RepairKits", targetRepairKits)) {
			reassemble();
			Dismiss();
		}
		else {
			//end ();
			doCountdown = true;
		}

	}

	private void onAccept() {
		postEvent (new CEPushPopup (typeof(UIStorePopup)));

	}

	private void onCancel() {
		end ();
	}

}
