﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SocialPlatforms;
using Facebook.Unity;

public class UILeaderboardsPopup : UIPopup {

	public Selector timeScopeSelector;
	public Selector leaderboardSelector;

	public dfLabel notAvailableLabel;
	public dfSprite loadingSprite;
	public dfScrollPanel scrollPanel;
	public LeaderboardRow rowPrefab;

	private UserScope userScope;
	private TimeScope timeScope;

	private enum LeaderboardSource {
		Facebook,
		Native,
		Unknown
	}
	private LeaderboardSource source;
	private LeaderboardSource prevSource;

	private UserScope prevUserScope;
	private TimeScope prevTimeScope;

	protected override void Awake ()
	{
		base.Awake ();

		loadingSprite.enabled = false;
		notAvailableLabel.enabled = false;
	}

	public override void OnWillShow ()
	{
		base.OnWillShow ();

		timeScopeSelector.onSelectionChanged += onTimeScopeChanged;
		leaderboardSelector.onSelectionChanged += onLeaderboardChanged;

		updateLeaderboardMode (leaderboardSelector.CurrentSelection);
		updateTimeScope (timeScopeSelector.CurrentSelection);
		
		prevSource = LeaderboardSource.Unknown;
		prevTimeScope = timeScope;
		prevUserScope = userScope;
	}

	private void onTimeScopeChanged (string selection, int index) {
		updateTimeScope (selection);
	}

	private void onLeaderboardChanged (string selection, int index) {
		updateLeaderboardMode (selection);
	}

	private void updateLeaderboardMode(string selection) {
		if (selection == "FRIENDS") {
			timeScopeSelector.ButtonsEnabled = true;
			source = LeaderboardSource.Native;
			userScope = UserScope.FriendsOnly;
		}
		else if (selection == "GLOBAL") {
			timeScopeSelector.ButtonsEnabled = true;
			source = LeaderboardSource.Native;
			userScope = UserScope.Global;
		}
		else if (selection == "FACEBOOK") {
			timeScopeSelector.CurrentSelectionIndex = 0;
			timeScopeSelector.ButtonsEnabled = false;

			source = LeaderboardSource.Facebook;
			userScope = UserScope.FriendsOnly;
		}
	}

	private void updateTimeScope(string selection) {
		if (selection == "ALL TIME") {
			timeScope = TimeScope.AllTime;
		}
		else if (selection == "THIS WEEK") {
			timeScope = TimeScope.Week;
		}
		else if (selection == "TODAY") {
			timeScope = TimeScope.Today;
		}
	}

	void Update() {
		if ((prevSource != source) || (prevUserScope != userScope) || (prevTimeScope != timeScope))  {
			scrollPanel.ResetVirtualScrollingData();

			switch (source) {
			case LeaderboardSource.Facebook:
			if (FB.IsLoggedIn) {
					notAvailableLabel.enabled = false;

					StopCoroutine ("updateFacebook");
					StartCoroutine("updateFacebook");
				}
				else {
					notAvailableLabel.enabled = true;
					notAvailableLabel.Text = "Facebook scores are not available at the moment";
				}
				break;

			case LeaderboardSource.Native:
				if (ScoringState.isAuthenticated) {
					notAvailableLabel.enabled = false;

					StopCoroutine ("updateNative");
					StartCoroutine("updateNative");
				}
				else {
					notAvailableLabel.enabled = true;
#if UNITY_IPHONE
					notAvailableLabel.Text = "GameCenter scores are not available at the moment";
#endif
				}
				break;
			}

			prevTimeScope = timeScope;
			prevUserScope = userScope;
			prevSource = source;
		}
	}

	public override void OnWillHide ()
	{
		base.OnWillHide ();
	}
	
	private IEnumerator updateNative() {
		yield return null;
		postEvent (new CELoadScores(Constants.leaderboardID, userScope, timeScope, new Range(1, 99)));

		List<LeaderboardRowData> rowData = new List<LeaderboardRowData>();

		loadingSprite.enabled = true;

#if UNITY_EDITOR
		rowData = populateDummyData();
#else
		while (ScoringState.isWorking) {
			updateLoadingSprite();
			yield return null;
		}

		if (ScoringState.scores != null && ScoringState.scores.Length > 0) {

			for (int i=0; i<ScoringState.scores.Length; i++) {
				IScore score = ScoringState.scores[i];
				IUserProfile profile = ScoringState.profiles[i];

				LeaderboardRowData rd = new LeaderboardRowData();
				rd.rank = score.rank;
				rd.score = score.value;
				rd.username = profile.userName;
				rd.isFriend = profile.isFriend;
				rd.isPlayer = (score.userID == Social.localUser.id);

				rowData.Add (rd);
			}
		}
#endif

		loadingSprite.enabled = false;

		updateScrollPanel(rowData);
	}

	private List<LeaderboardRowData> populateDummyData() {
		var rowData = new List<LeaderboardRowData>();

		for (int i=0; i<100; i++) {
			LeaderboardRowData rd = new LeaderboardRowData();
			rd.rank = i+1;
			rd.score = Random.Range(100, 100000);
			rd.username = "user" + i.ToString();
			rd.isFriend = (i % 2 == 0) && (i > 0);
			rd.isPlayer = (i == 2);
			
			rowData.Add (rd);
		}

		return rowData;
	}

	private IEnumerator updateFacebook() {
		yield return null;
		postEvent (new CELoadFacebookScores());
		
		List<LeaderboardRowData> rowData = new List<LeaderboardRowData>();

		loadingSprite.enabled = true;

		#if UNITY_EDITOR
		rowData = populateDummyData();
		#else
		while (FacebookState.isWorking) {
			updateLoadingSprite();
			yield return null;
		}
		
		if (FacebookState.scores != null && FacebookState.scores.Length > 0) {
			
			for (int i=0; i<FacebookState.scores.Length; i++) {
				FBScore s = FacebookState.scores[i];
				
				LeaderboardRowData rd = new LeaderboardRowData();
				rd.rank = s.rank;
				rd.score = s.score;
				rd.username = s.username;
				rd.isPlayer = (s.id == AccessToken.CurrentAccessToken.UserId);
				
				rowData.Add (rd);
			}
		}
		#endif

		loadingSprite.enabled = false;

		updateScrollPanel(rowData);
	}

	private void updateLoadingSprite() {
		loadingSprite.transform.Rotate (new Vector3(0,0,1), -Time.deltaTime * 100);
	}

	private void updateScrollPanel(List<LeaderboardRowData> data) {
		notAvailableLabel.enabled = (data.Count == 0);
		notAvailableLabel.Text = "No scores are available for this mode!";

		scrollPanel.Virtualize<LeaderboardRowData> (data, rowPrefab.GetComponent<dfPanel>());
	}
}
