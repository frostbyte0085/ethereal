﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class PauseState {
	public static bool isPaused;
	public static bool mustShowCountdown;
}

public class UIPausePopup : UIPopup {

	private bool forfeitClicked;

	protected override void Awake ()
	{
		base.Awake ();
	}

	public override void OnWillShow ()
	{
		postEvent (new CEDoSound (DoSoundAction.PauseComponent, "Sfx"));

		PauseState.isPaused = true;
		PauseState.mustShowCountdown = true;

		postEvent (new CEDoScanlines(true));

		Time.timeScale = 0f;
		AudioListener.volume = 0.35f;

		base.OnWillShow ();
	}

	public override void OnWillHide ()
	{
		postEvent (new CEDoScanlines(false));

		//if (!forfeitClicked) {
			Time.timeScale = 1f;
		//}

		PauseState.isPaused = false;
		base.OnWillHide ();
	}

	void OnForfeitClick()
	{
		if (forfeitClicked) return;

		AudioListener.volume = 1f;
		postEvent (new CEFade (FadeMode.Out, 1f, null, fadePauseFinished));
		forfeitClicked = true;
	}

	private void fadePauseFinished(FadeMode mode) {
		if (mode == FadeMode.Out) {

			Time.timeScale = 1f;
			PauseState.isPaused = false;
			postEvent (new CEGameRestart ());
		}
	}
}
