﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

public class UISettingsPopup : UIPopup {
	
	public Selector sfxSelector;
	public Selector menuSfxSelector;
	public Selector musicSelector;
	public Selector tutorialSelector;
	public Selector controlsSelector;

	public ComplexButton facebookButton;

	protected override void Awake ()
	{
		base.Awake ();

		sfxSelector.onSelectionChanged += onSfxChanged;
		menuSfxSelector.onSelectionChanged += onMenuSfxChanged;
		musicSelector.onSelectionChanged += onMusicChanged;
		tutorialSelector.onSelectionChanged += onTutorialChanged;
		controlsSelector.onSelectionChanged += onControlsChanged;

		registerEventListener (typeof(CECurrentSaveData), saveDataEventListener);
	}

	protected override void Start ()
	{
		base.Start ();

		checkFBStatus();
	}

	private void saveDataEventListener (ConvexEvent evt) {
		CECurrentSaveData saveData = (CECurrentSaveData)evt;

		int sfx = (saveData.SfxEnabled ? 0 : 1);
		int menuSfx = (saveData.MenuSfxEnabled ? 0 : 1);
		int music = (saveData.MusicEnabled ? 0 : 1);
		int tutorial = saveData.NeedsTutorial ? 0 : 1;
		int controls = saveData.ControlScheme;

		if (sfxSelector.CurrentSelectionIndex != sfx) {
			sfxSelector.CurrentSelectionIndex = sfx;
		}

		if (menuSfxSelector.CurrentSelectionIndex != menuSfx) {
			menuSfxSelector.CurrentSelectionIndex = menuSfx;
		}

		if (musicSelector.CurrentSelectionIndex != music) {
			musicSelector.CurrentSelectionIndex = music;
		}

		if (tutorialSelector.CurrentSelectionIndex != tutorial) {
			tutorialSelector.CurrentSelectionIndex = tutorial;
		}

		if (controlsSelector.CurrentSelectionIndex != controls) {
			controlsSelector.CurrentSelectionIndex = controls;
		}
	}

	private void onControlsChanged (string selection, int index) {
		postEvent (new CEControlSchemeChanged ((ControlScheme)index));
	}

	private void onTutorialChanged (string selection, int index) {
		postEvent (new CENeedsTutorial ( index == 0 ));
	}

	private void onSfxChanged (string selection, int index) {
		if (index == 0) {
			postEvent (new CEDoSound(DoSoundAction.Unmute, "Sfx"));
		}
		else {
			postEvent (new CEDoSound(DoSoundAction.Mute, "Sfx"));
		}
	}

	private void onMenuSfxChanged (string selection, int index) {
		if (index == 0) {
			postEvent (new CEDoSound(DoSoundAction.Unmute, "MenuSfx"));
		}
		else {
			postEvent (new CEDoSound(DoSoundAction.Mute, "MenuSfx"));
		}
	}

	private void onMusicChanged (string selection, int index) {

		if (index == 0) {
			postEvent (new CEDoSound(DoSoundAction.Unmute, "Music"));
		}
		else {
			postEvent (new CEDoSound(DoSoundAction.Mute, "Music"));
		}
	}

	private void checkFBStatus() {
		bool loggedIn = FB.IsLoggedIn;


		facebookButton.IsEnabled = !loggedIn;

		if (loggedIn) {
			facebookButton.Text = "LOGGED IN";
			facebookButton.SnapDown();
			facebookButton.GetComponent<dfButton>().State = dfButton.ButtonState.Pressed;
		}
	}

	void Update() {

		checkFBStatus();
	}

	public void OnLoginClick()
	{
		postEvent (new CEFacebookLogin());
	}


}
