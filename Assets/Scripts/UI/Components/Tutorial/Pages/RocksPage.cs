﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RocksPage : TutorialPage {

	public dfTextureSprite[] rockSprites;

	private float[] rotations;

	void Awake ()
	{
		rotations = new float[rockSprites.Length];

		for (int i=0; i<rotations.Length; i++) {
			int sign = i % 2 == 0 ? -1 : 1;
			rotations[i] = Random.Range(10f, 30f) * sign;
		}
	}

	public override bool IsFinished {
		get {
			return true;
		}
	}

	protected override void Update ()
	{
		base.Update ();

		for (int i=0; i<rockSprites.Length; i++) {
			rockSprites[i].transform.Rotate (new Vector3(0,0,rotations[i]) * Time.deltaTime);
		}
	}
}
