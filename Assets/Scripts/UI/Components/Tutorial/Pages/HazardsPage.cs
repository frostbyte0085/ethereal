﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HazardsPage : TutorialPage {

	public dfTextureSprite zapTexture;
	public dfTextureSprite laserTexture;

	public Camera zapCamera;
	public Camera laserCamera;

	private RenderTexture zapRT;
	private RenderTexture laserRT;


	private GameObject particlesCamGO;

	void Awake ()
	{
		// disable the particles camera
		particlesCamGO = GameObject.FindGameObjectWithTag (TagStringManager.ParticlesCamera);
		particlesCamGO.SetActive (false);

		zapRT = new RenderTexture(64, 512, 0);
		zapRT.isPowerOfTwo = false;

		zapCamera.targetTexture = zapRT;
		zapCamera.aspect = ((float)zapRT.width / (float)zapRT.height) * 2f;
		zapTexture.Texture = zapRT;

		laserRT = new RenderTexture(512, 64, 0);
		laserRT.isPowerOfTwo = false;

		laserCamera.targetTexture = laserRT;
		laserCamera.aspect = ((float)laserRT.width / (float)laserRT.height) * 2f;
		laserTexture.Texture = laserRT;
	}

	public override bool IsFinished {
		get {
			return true;
		}
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();

		if (zapRT != null) {
			DestroyImmediate (zapRT);
		}

		if (laserRT != null) {
			DestroyImmediate (laserRT);
		}

		if (particlesCamGO != null) {
			particlesCamGO.SetActive (true);
		}
	}
}
