﻿using UnityEngine;
using System.Collections;

public class ControlsPage : TutorialPage {

	public dfSprite thumbSprite;

	private bool inputTestSuccess;

	private void tutorialInputSuccessEventListener (ConvexEvent evt) {
		CETutorialInputSuccess e = (CETutorialInputSuccess)evt;

		inputTestSuccess = true;

		// reset the input to disabled
		postEvent (new CEInputMode (InputMode.Disabled));
	}

	protected override void Start ()
	{
		base.Start ();

		// enable the tutorial input mode
		postEvent (new CEInputMode (InputMode.Tutorial));

		registerEventListener (typeof(CETutorialInputSuccess), tutorialInputSuccessEventListener);
	}

	protected override void Update ()
	{
		base.Update ();

		if (Input.GetMouseButtonDown(0) && thumbSprite != null) {
			Destroy (thumbSprite.gameObject);
		}
	}

	public override bool FinishNow {
		get {
			return inputTestSuccess;
		}
	}

	public override bool IsFinished {
		get {
			return false;
		}
	}
}
