﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TutorialQueue : MessageBehaviour {

	public TutorialPage[] pages;
	public float animationDuration = 0.5f;

	private CECurrentSaveData saveData;
	private Queue<TutorialPage> remainingPages;
	private TutorialPage activePage;

	private bool hasStarted;
	private float elapsed;
	private bool isBusy;

	private dfPanel panel;

	private void saveDataEventListener(ConvexEvent evt) {
		saveData = (CECurrentSaveData)evt;

	}

	// Use this for initialization
	void Awake () {
		registerEventListener (typeof(CECurrentSaveData), saveDataEventListener);

		remainingPages = new Queue<TutorialPage>(pages);
		foreach (TutorialPage page in pages) {
			page.gameObject.SetActive (false);
		}

		panel = GetComponent<dfPanel>();
	}

	// Update is called once per frame
	void Update () {
		if (!saveData.NeedsTutorial) { 
			panel.gameObject.SetActive (false);
			return;
		}

		// if the tutorial hasn't started yet, we need to get the first page
		if (!hasStarted) {
			hasStarted = true;
			StartCoroutine (showNextPage());
		}

		if (activePage != null && !isBusy && !isFading) {

			// keep track of time so we show each page at least 1 second before the user can dismiss it
			elapsed += Time.deltaTime;

			// if this page has finished telling what it needs to tell, check for touches to proceed to the next one
			if ( (activePage.IsFinished || activePage.FinishNow) && elapsed > 1f) {
			
				if (activePage.FinishNow || isClicked) {
					StartCoroutine (showNextPage());
					isClicked = false;
				}
			}
		}
	}

	private bool isClicked;
	public void OnClick( dfControl control, dfMouseEventArgs mouseEvent )
	{
		isClicked = true;
	}


	private bool isFading;
	private IEnumerator fadeAll() {
		isFading = true;

		float elapsed = 0;

		while (elapsed<1f) {
			elapsed += Time.deltaTime * 1f / animationDuration;

			panel.Opacity = Mathf.Lerp(1f, 0f, elapsed);

			yield return null;
		}

		// no more pages, finish tutorial
		postEvent (new CENeedsTutorial(false));
		// and enable proper input for the spaceship
		postEvent (new CEInputMode (InputMode.Free));

		panel.gameObject.SetActive (false);
	}

	void OnSkipTutorial()
	{
		// Add event handler code here
		StartCoroutine (fadeAll());
	}


	private IEnumerator showNextPage() {
		isBusy = true;

		dfPanel mainPanel = null;
		float t = 0f;
		if (activePage != null) {

			if (remainingPages.Count > 0) {
				mainPanel = activePage.GetComponent<dfPanel>();

				while ( t < 1f) {
					t += Time.deltaTime * 1f / animationDuration;
					mainPanel.Opacity = Mathf.Lerp(1f, 0f, t);

					yield return null;
				}

				Destroy (activePage.gameObject);
				activePage = null;
			}
			else {
				// no more pages, finish tutorial
				StartCoroutine (fadeAll());
			}
		}

		// get the next one
		if (remainingPages.Count > 0) {
			activePage = remainingPages.Dequeue();
			activePage.gameObject.SetActive (true);

			mainPanel = activePage.GetComponent<dfPanel>();
			t = 0f;
			while (t < 1f) {
				t += Time.deltaTime * 1f / animationDuration;
				mainPanel.Opacity = Mathf.Lerp(0f, 1f, t);
				yield return null;
			}
		}

		elapsed = 0f;
		isBusy = false;
	}
}
