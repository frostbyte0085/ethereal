﻿using UnityEngine;
using System.Collections;

public abstract class TutorialPage : MessageBehaviour {

	// Use this for initialization
	protected virtual void Start () {
	
	}
	
	// Update is called once per frame
	protected virtual void Update () {
	
	}

	public abstract bool IsFinished {
		get;
	}

	public virtual bool FinishNow {
		get {
			return false;
		}
	}
}
