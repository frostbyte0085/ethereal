﻿using UnityEngine;
using System.Collections;

public class LeaderboardRowData {
	public string username;
	public long score;
	public int rank;
	public bool isPlayer;
	public bool isFriend;
}

public class LeaderboardRow : MonoBehaviour, IDFVirtualScrollingTile {

	public Color oddTextColor;
	public Color evenTextColor;
	public string evenBackgroundSpriteName;

	public dfSprite playerMarker;
	public dfLabel rankLabel;
	public dfLabel usernameLabel;
	public dfLabel scoreLabel;

	private dfPanel panel;
	private int index;

	// Use this for initialization
	void Awake () {
		panel = GetComponent<dfPanel>();
	}

	#region IDFVirtualScrollingTile implementation

	public void OnScrollPanelItemVirtualize (object backingListItem)
	{
		LeaderboardRowData data = (LeaderboardRowData)backingListItem;

		rankLabel.Text = data.rank.ToString();
		usernameLabel.Text = data.username;
		scoreLabel.Text = data.score.ToString();

		if (index % 2 == 0) {
			rankLabel.Color = evenTextColor;
			usernameLabel.Color = evenTextColor;
			scoreLabel.Color = evenTextColor;
			panel.BackgroundSprite = evenBackgroundSpriteName;
		}
		else {
			rankLabel.Color = oddTextColor;
			usernameLabel.Color = oddTextColor;
			scoreLabel.Color = oddTextColor;
			panel.BackgroundSprite = "";
		}

		playerMarker.IsVisible = (data.isPlayer);
	}

	public dfPanel GetDfPanel ()
	{
		return panel;
	}

	public int VirtualScrollItemIndex {
		get {
			return index;
		}
		set {
			index = value;
		}
	}

	#endregion
}
