﻿using UnityEngine;
using System.Collections;

public delegate void OnSelectorSelectionChangedDelegate(string selection, int index);

public class Selector : MonoBehaviour {

	public string[] selections;

	private dfButton previousButton;
	private dfButton nextButton;
	private dfLabel selectionLabel;

	private int currentSelectionIndex;

	public OnSelectorSelectionChangedDelegate onSelectionChanged;

	// Use this for initialization
	void Awake () {
		previousButton = transform.Find ("previous_button").GetComponent<dfButton>();
		nextButton = transform.Find ("next_button").GetComponent<dfButton>();
		selectionLabel = transform.Find ("selection_label").GetComponent<dfLabel>();

		dfEventBinding clickPrevious = gameObject.AddComponent<dfEventBinding>();
		dfComponentMemberInfo mi_target_previous = new dfComponentMemberInfo();
		mi_target_previous.Component = this;
		mi_target_previous.MemberName = "OnPreviousClick";
		
		dfComponentMemberInfo mi_source_previous = new dfComponentMemberInfo();
		mi_source_previous.MemberName = "Click";
		mi_source_previous.Component = previousButton;
		
		clickPrevious.DataSource = mi_source_previous;
		clickPrevious.DataTarget = mi_target_previous;
		clickPrevious.Bind();
		
		dfEventBinding clickNext = gameObject.AddComponent<dfEventBinding>();
		dfComponentMemberInfo mi_target_next = new dfComponentMemberInfo();
		mi_target_next.Component = this;
		mi_target_next.MemberName = "OnNextClick";
		
		dfComponentMemberInfo mi_source_next = new dfComponentMemberInfo();
		mi_source_next.MemberName = "Click";
		mi_source_next.Component = nextButton;
		
		clickNext.DataSource = mi_source_next;
		clickNext.DataTarget = mi_target_next;
		clickNext.Bind();

		buttonsEnabled = true;
		updateSelection(false);
	}

	public void OnPreviousClick( dfControl control, dfMouseEventArgs mouseEvent ) {
		currentSelectionIndex--;
		if (currentSelectionIndex < 0)
			currentSelectionIndex = selections.Length-1;

		updateSelection();
	}

	public void OnNextClick( dfControl control, dfMouseEventArgs mouseEvent ) {
		currentSelectionIndex = (++currentSelectionIndex) % selections.Length;

		updateSelection();
	}

	private void updateSelection(bool fireEvent=true) {
		selectionLabel.Text = selections[currentSelectionIndex];

		if (onSelectionChanged != null && fireEvent) {
			onSelectionChanged (selections[currentSelectionIndex], currentSelectionIndex);
		}
	}

	public string CurrentSelection {
		get {
			return selections[currentSelectionIndex];
		}
	}

	private bool buttonsEnabled;
	public bool ButtonsEnabled {
		get {
			return ButtonsEnabled;
		}
		set {
			if (value != buttonsEnabled) {
				buttonsEnabled = value;

				previousButton.IsEnabled = buttonsEnabled;
				nextButton.IsEnabled = buttonsEnabled;
			}
		}
	}

	public int CurrentSelectionIndex {
		get {
			return currentSelectionIndex;
		}
		set {
			currentSelectionIndex = value;
			updateSelection();
		}
	}



	// Update is called once per frame
	void Update () {
	
	}
}
