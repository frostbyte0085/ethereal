﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StartButton : MonoBehaviour {

	public dfLabel label;

	// Use this for initialization
	void Awake () {

	}
	
	// Update is called once per frame
	void Update () {

		switch (CurrentShipState) {
		case UnlockType.Unlocked:
			label.Text = "Engage!";
			break;
		case UnlockType.Locked:
			label.Text = "Unlock";
			break;

		default:break;
		}
	}

	private UnlockType type;

	public UnlockType CurrentShipState {
		get {
			return type;
		}
		set {
			type = value;
		}
	}
}
