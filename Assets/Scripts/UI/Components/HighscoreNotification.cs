﻿using UnityEngine;
using System.Collections;

public class HighscoreNotification : MonoBehaviour {

	public dfLabel label;
	private dfPanel panel;

	// Use this for initialization
	void Awake () {

		panel = GetComponent<dfPanel>();

		label.Text = ScoringState.currentMaxScore.ToString();
		panel.transform.localScale = Vector3.one * 2f;
	}

	void Update() {
		panel.transform.localScale = Vector3.Lerp (panel.transform.localScale, Vector3.one, Time.deltaTime*3);
	}
}
