﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpaceshipMenu : MessageBehaviour {

	public SpaceshipDetails details;
	public dfLabel specialLabel;

	private Transform unlockTrans;

	#region event listeners
	private void saveDataEventListener(ConvexEvent evt) {
		CECurrentSaveData e = (CECurrentSaveData)evt;

		if (unlockTrans != null) {

			unlockTrans.gameObject.SetActive (! e.ShipData[(int)details.type].isUnlocked);

		}
	}
	#endregion

	// Use this for initialization
	void Awake() {

		unlockTrans = transform.Find ("unlock_panel");

		registerEventListener (typeof(CECurrentSaveData), saveDataEventListener);
	}

}
