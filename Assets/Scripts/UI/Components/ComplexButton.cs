﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ComplexButton : MonoBehaviour {

	public bool fitToContents = false;
	public float fitPadding = 5f;
	//public float minWidth = 
	public float disabledOpacity = 0.5f;
	public float downOffset = 4f;
	public bool sendMessageUpwards = true;
	public string receiverName = "";

	private dfPanel panel;

	private Vector3 originalRelativePosition;
	private dfLabel mainLabel;

	void Awake() {
	
		panel = transform.Find ("Panel").GetComponent<dfPanel>();

		mainLabel = GetComponentInChildren<dfLabel>();
		IsEnabled = true;

		Debug.Log (mainLabel.Text);
	}

	// Use this for initialization
	void Start () {

		dfEventBinding mouseDown = gameObject.AddComponent<dfEventBinding>();
		dfComponentMemberInfo mi_target_down = new dfComponentMemberInfo();
		mi_target_down.Component = this;
		mi_target_down.MemberName = "OnMouseDown";

		dfComponentMemberInfo mi_source_down = new dfComponentMemberInfo();
		mi_source_down.MemberName = "MouseDown";
		mi_source_down.Component = GetComponent<dfButton>();

		mouseDown.DataSource = mi_source_down;
		mouseDown.DataTarget = mi_target_down;
		mouseDown.Bind();

		dfEventBinding mouseUp = gameObject.AddComponent<dfEventBinding>();
		dfComponentMemberInfo mi_target_up = new dfComponentMemberInfo();
		mi_target_up.Component = this;
		mi_target_up.MemberName = "OnMouseUp";
		
		dfComponentMemberInfo mi_source_up = new dfComponentMemberInfo();
		mi_source_up.MemberName = "MouseUp";
		mi_source_up.Component = GetComponent<dfButton>();
		
		mouseUp.DataSource = mi_source_up;
		mouseUp.DataTarget = mi_target_up;
		mouseUp.Bind();

		originalRelativePosition = panel.RelativePosition;

		fit ();

	}

	private void fit() {
		if (!fitToContents) return;

		dfControl[] controls = panel.GetComponentsInChildren<dfControl>(true);
		float width = 0f;
		if (controls != null) {
			foreach (dfControl c in controls) {
				if (c is dfPanel) continue;

				width += c.Width;
			}
		}

		dfButton button = GetComponent<dfButton>();
		button.Width = width + fitPadding;
	}

	public string Text {
		get {
			return mainLabel.Text;
		}
		set {
			mainLabel.Text = value;
			fit();
		}
	}

	private bool isEnabled;
	public bool IsEnabled {
		get {
			return isEnabled;
		}
		set {
			isEnabled = true;
			dfControl[] controls = GetComponentsInChildren<dfControl>(true);
			foreach (dfControl p in controls) {
				p.IsInteractive = value;
			}
		}
	}

	public void SnapDown() {
		panel.RelativePosition = originalRelativePosition + new Vector3(downOffset, downOffset, 0f);
	}

	private bool sent=false;
	public void OnMouseDown( dfControl control, dfMouseEventArgs mouseEvent )
	{
		panel.RelativePosition += new Vector3(downOffset, downOffset, 0f);
		sent = false;
	}

	public void OnMouseUp( dfControl control, dfMouseEventArgs mouseEvent )
	{
		panel.RelativePosition -= new Vector3(downOffset, downOffset, 0f);

		// send the message is the mouse is over this button
		if (control.ContainsMouse && !sent) {
			if (sendMessageUpwards && !string.IsNullOrEmpty(receiverName)) {
				SendMessageUpwards (receiverName);
			}
		}

		sent = true;
	}

}
