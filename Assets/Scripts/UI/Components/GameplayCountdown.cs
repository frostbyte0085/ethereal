﻿using UnityEngine;
using System.Collections;

public class GameplayCountdown : MonoBehaviour {

	private dfLabel label;
	private float elapsed;

	private string[] texts = {"3", "2", "1", "Go!"};
	private int currentIndex;

	private const float punchStrength = 4f;
	private const float punchDecay = 0.1f;
	private const float countdownDuration = 0.8f;

	// Use this for initialization
	void Awake () {
		label = GetComponentInChildren<dfLabel>();
	}

	void OnEnable() {
		Time.timeScale = 0f;
		elapsed = 0f;
		currentIndex = 0;
		label.Text = texts[currentIndex];
		label.transform.localScale = Vector3.one * punchStrength;
	}

	void OnDisable() {
		Time.timeScale = 1f;
	}
	
	// Update is called once per frame
	void Update () {
		elapsed += Time.unscaledDeltaTime * 1f/ countdownDuration;
		if (elapsed >= 1f) {
			elapsed = 0f;

			currentIndex++;
			if (currentIndex == texts.Length) {
				gameObject.SetActive (false);
				return;
			}

			label.Text = texts[currentIndex];
			label.transform.localScale = Vector3.one * punchStrength;
		}

		label.transform.localScale = Vector3.Lerp (label.transform.localScale, Vector2.one, punchDecay);
	}
}
