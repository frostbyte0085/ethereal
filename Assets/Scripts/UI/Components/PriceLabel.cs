﻿using UnityEngine;
using System.Collections;

public class PriceLabel : MonoBehaviour {

	public string identifier="";
	public bool isNonConsumable = false;
	public bool isFree = false;
	public dfLabel label;
	public dfLabel ownedLabel;
	public string currencyId="";
	
	// Use this for initialization
	void Start () {
		Update();
	}

	public bool IsPurchased {
		get; private set;
	}

	void Update() {

		// default - error
		label.Text = "UNKNOWN";
		
		if (isFree) {
			label.Text = "FREE!";
		}
		else {
			PurchasableItem item = Unibiller.GetPurchasableItemById (identifier);
			if (item != null) {
				string s = item.localizedPriceString;
				if (string.IsNullOrEmpty(s)) {
					s = item.priceInLocalCurrency.ToString();
				}
				
				label.Text = s;
			}
		}

		// if it's a non-consumable, inform the user if it's been purchased.
		if (isNonConsumable) {
			if (Unibiller.GetPurchaseCount (identifier) == 1) {
				label.Text = "PURCHASED!";
				IsPurchased = true;
				// get the button and disable it.
				//dfButton button = GetComponent<dfButton>();
				//button.Disable();
			}
		}
		else {
			if (ownedLabel != null) {
				ownedLabel.Text = "Owned: " + Unibiller.GetCurrencyBalance (currencyId).ToString();
			}
		}
	}
}
