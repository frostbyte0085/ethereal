using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PlatformQuery {
	
	public enum Platform
	{
		PC,
		Mobile,
		Console,
		Unknown
	}
	
	public enum PlatformSubset
	{
		OSX,
		Windows,
		iOS,
		Android,
		Android_Ouya,
		Unknown
	}
	
	private static Platform platform;
	private static PlatformSubset platform_subset;
	
	public static void Initialize() {
		platform = Platform.Unknown;
		platform_subset = PlatformSubset.Unknown;
		
		// are we in hardware / simulator mode?
		// get the platform from the application.platform enum.
		if (!IsEditor) {
			
			switch (Application.platform) {
				
				case RuntimePlatform.OSXPlayer:
				case RuntimePlatform.OSXWebPlayer:
					platform = Platform.PC;
					platform_subset = PlatformSubset.OSX;
				break;
				
				case RuntimePlatform.WindowsPlayer:
				case RuntimePlatform.WindowsWebPlayer:
					platform = Platform.PC;
					platform_subset = PlatformSubset.Windows;
				break;
				
				case RuntimePlatform.IPhonePlayer:
					platform = Platform.Mobile;
					platform_subset = PlatformSubset.iOS;
				break;
				
				case RuntimePlatform.Android:
					if (SystemInfo.deviceModel.ToUpper().Contains ("OUYA")) {
						platform = Platform.Console;
						platform_subset = PlatformSubset.Android_Ouya;
					}
					else {
						platform = Platform.Mobile;
						platform_subset = PlatformSubset.Android;
					}
				break;
			}
		}
		
		// we are in the editor. we must get the platform from the current build target.
		else {
#if UNITY_EDITOR
			BuildTarget t = EditorUserBuildSettings.activeBuildTarget;
			
			switch (t) {
				case BuildTarget.StandaloneOSXIntel:
					platform = Platform.PC;
					platform_subset = PlatformSubset.OSX;
					break;
				
				case BuildTarget.StandaloneWindows:
				case BuildTarget.StandaloneWindows64:
					platform = Platform.PC;
					platform_subset = PlatformSubset.Windows;
					break;
				
				case BuildTarget.Android:
					platform = Platform.Mobile;
					platform_subset = PlatformSubset.Android;
					break;
				
				case BuildTarget.iOS:
					platform = Platform.Mobile;
					platform_subset = PlatformSubset.iOS;
					break;
			}
#endif
		}
	}
	
	public static Platform SystemPlatform {
		get
		{
			return platform;
		}
	}
	
	public static PlatformSubset SystemPlatformSubset {
		get
		{
			return platform_subset;
		}
	}
	
#if UNITY_IPHONE


	public static UnityEngine.iOS.DeviceGeneration Generation {
		get
		{
			return UnityEngine.iOS.Device.generation;
		}
	}

	public static bool IsIPad {
		get {
			return ((float)Screen.width / (float)Screen.height) < 1.35f;
		}
	}


#endif
	public static bool IsWebPlayer {
		get
		{
			return (Application.platform == RuntimePlatform.WindowsWebPlayer || Application.platform == RuntimePlatform.OSXWebPlayer);
		}
	}
	
	public static bool IsEditor {
		get
		{
			return (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor);
		}
	}
}
