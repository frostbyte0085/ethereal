using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public enum DeviceFamily {
	iPhone,
	iPad,
	iPod,
	Editor,
	Unknown
	// more
}

// this is where we keep all our qualityControl instances, they are loaded at game start.
// the game then chooses the correct quaityControl based on the device and applies the settings
public static class QualityControlManager {

    private static QualitySetting[] settings;
    private static QualitySetting currentSetting;

	private static bool isInit = false;

    public static void Initialize() {

		if (isInit) return;

        load();
        detect();
        apply();

		isInit = true;
    }
   
    private static string SettingsDirectory {
        get {
            return "QualitySettings";
        }
    }

    private static void load() {
        Object[] settingsObj = Resources.LoadAll (SettingsDirectory, typeof(QualitySetting));

        settings = new QualitySetting[settingsObj.Length];
        for (int i=0; i<settings.Length; i++) {
            settings[i] = (QualitySetting)settingsObj[i];
        }
    }

	public static DeviceFamily Family {
		get; private set;
	}

    private static void detect() {

        HardwareType type = HardwareType.MaxQuality;
		Family = DeviceFamily.Editor;

#if UNITY_EDITOR
        // if we are in editor, try to get the MaxQuality setting.
        // see above
        Debug.Log ("Running in Editor, will use the maximum quality setting.");
#else
#   if UNITY_IPHONE
        type = detect_iOS();
#   elif UNITY_ANDROID
        type = detect_Android();
#   endif

#endif

        // if the returned type is AutoDetect, then create a new QualitySetting using the 
        // following function
        if (type == HardwareType.AutoDetect) {
            currentSetting = autoDetect();
        }
        else {
            // if this is not available, auto-detect one
            currentSetting = GetQualitySetting (type);

            if (currentSetting == null) {
                currentSetting = autoDetect();
            }
        }
    }

    #region quality detection
#if UNITY_IPHONE

	private static DeviceFamily detectDeviceFamily_iOS(UnityEngine.iOS.DeviceGeneration generation) {
		DeviceFamily family = DeviceFamily.Unknown;

		string genName = generation.ToString().ToLower();

		if (genName.Contains ("ipod"))
			family = DeviceFamily.iPod;
		else if (genName.Contains ("ipad"))
			family = DeviceFamily.iPad;
		else if (genName.Contains ("iphone"))
			family = DeviceFamily.iPhone;

		return family;
	}

    private static HardwareType detect_iOS() {

		Family = detectDeviceFamily_iOS(UnityEngine.iOS.Device.generation);

        // load the unsupported devices asset.
        IgnoreQualitySettings_iOS unsupportedDevices = (IgnoreQualitySettings_iOS)Resources.Load (SettingsDirectory + "/iOS/Unsupported", typeof(IgnoreQualitySettings_iOS));
        // if the iPhone generation exists in this unsupported list, then simply choose the minimum quality setting
        foreach (UnityEngine.iOS.DeviceGeneration gen in unsupportedDevices.unsupportedDevices) {
            if (gen == UnityEngine.iOS.Device.generation) {
                Debug.Log ("Unsupported device, falling back to minimum quality settings");
                return HardwareType.MinQuality;
            }
        }

        // if the device is unknown,
        // this will fallback to the maximum quality setting, most probably a newer, more powerful device
        switch (UnityEngine.iOS.Device.generation) {
            case UnityEngine.iOS.DeviceGeneration.iPodTouchUnknown:
            case UnityEngine.iOS.DeviceGeneration.iPadUnknown:
            case UnityEngine.iOS.DeviceGeneration.iPhoneUnknown:
                Debug.Log ("Unknown device, risking maximum quality settings");
                return HardwareType.MaxQuality;
        }

        // find the generation in the supported, enumerated devices.
        string generation = UnityEngine.iOS.Device.generation.ToString();

        foreach (QualitySetting s in settings) {
            string hw = s.hardwareType.ToString();

            if (hw == generation) {
                HardwareType type = (HardwareType)System.Enum.Parse (typeof(HardwareType), generation);
                Debug.Log ("Selected quality settings for hardware type: " + type.ToString());
                return type;
            }
        }

        // no quality setting was found, try to detect it here.
        // this should never happen.
        Debug.Log ("Device is known but quality setting is not available. Auto-detecting.");
		// set to max for now
        return HardwareType.MaxQuality;
    }
#endif

#if UNITY_ANDROID
    private static HardwareType detect_Android() {

        return HardwareType.AutoDetect;
    }

#endif
    #endregion

    // selects a quality setting configuration after measuring the performance
    // on this device.
    private static QualitySetting autoDetect() {

        QualitySetting detectedSetting = (QualitySetting)ScriptableObject.CreateInstance(typeof(QualitySetting));

        return detectedSetting;
    }

    private static void apply() {

        // if for some reason we don't have a current quality setting, make one and use it
        if (currentSetting == null) {

            // if we are running in the editor, use the maximum quality setting
            if (!Application.isPlaying) {
				currentSetting = (QualitySetting)ScriptableObject.CreateInstance(typeof(QualitySetting));
            }
            // otherwise, auto-detect it
            else {
                currentSetting = autoDetect();
            }
        }

        // application framerate
        Application.targetFrameRate = 60;

        // shaders
        Shader.globalMaximumLOD = currentSetting.maxShaderLOD;
        Shader.WarmupAllShaders();
        
        // timings
        Time.fixedDeltaTime = currentSetting.fixedDeltaTime;

        currentSetting.Apply();
    }

    public static QualitySetting CurrentSetting {
        get {
            // if it's null for any reason, re-apply it.
            // this will force the quality setting to max!
            // this is only supposed to be called when calling CurrentSetting from an editor script.
            if (currentSetting == null) {
                apply();
            }

            return currentSetting;
        }
    }

    public static QualitySetting GetQualitySetting (HardwareType type) {
        for (int i=0; i<settings.Length; i++) {
            if (settings[i].hardwareType == type)
                return settings[i];
        }
        
        return null;
    }
}
