using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum HardwareType {
    // default qualities
    MaxQuality = 0,
    MinQuality = 1,
    AutoDetect = 2,

    // these must correspond to the supported devices in iPhoneGeneration enumeration, case matters!
    // iphones
    iPhone = 1000,
    iPhone3G = 1001,
    iPhone3GS = 1002,
    iPhone4 = 1003,
    iPhone4S = 1004,
    iPhone5 = 1005,
    iPhone5C = 1006,
    iPhone5S = 1007,
    // ipads
    iPad1Gen = 1100,
    iPad2Gen = 1101,
    iPad3Gen = 1102,
    iPad4Gen = 1103,
    iPad5Gen = 1104,
    // ipad mini
    iPadMini1Gen = 1200,
    iPadMini2Gen = 1201,
    // ipods
	iPodTouch1Gen = 1300,
	iPodTouch2Gen = 1301,
	iPodTouch3Gen = 1302,
	iPodTouch4Gen = 1303,
	iPodTouch5Gen = 1304
}

public enum AntialiasQuality {
    None=0,
    MSAA_2X=2,
    MSAA_4X=4   
}

public enum Quality {
	Off,
    Low,
    Medium,
    High
}

// set the values to default, as high!
// can be extended by game code
public partial class QualitySetting : ScriptableObject {
    
    public HardwareType hardwareType = HardwareType.MaxQuality;
    
    // core engine features
    public Quality maxParticleQuality = Quality.High;
    public int maxShaderLOD = 500;
    public float fixedDeltaTime = 0.04f;
	public bool distortionEffects = true;
	public bool extraStarLayer = true;

    public void Apply() {
        onApply();
    }

    partial void onApply();
}