﻿Shader "Convex/PostProcess/Flicker" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	
	SubShader {
		
		Pass {
		
			Cull Off
			ZWrite Off
			ZTest Always
			Fog {Mode Off}
		
			CGPROGRAM
			
			
			#pragma vertex vert_img
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			
			half _ChromaOffset;
			half _UVSpeed;
			half _UVPhase;
			half _UVAmplitude;
			
			half _AnimationFactor;
			
			fixed4 frag(v2f_img i):COLOR {
			
				float2 uv = i.uv + (sin(_Time.y*_UVSpeed + i.uv.xy*_UVPhase) * _UVAmplitude) * _AnimationFactor;
			
				fixed red = tex2D (_MainTex, uv + float2(_ChromaOffset, _ChromaOffset*0.75)).r;
				fixed green = tex2D (_MainTex, uv).g;
				fixed blue = tex2D (_MainTex, uv - float2(_ChromaOffset, _ChromaOffset*0.5)).b;
				
				fixed4 original = tex2D (_MainTex, i.uv);
				
				return lerp(original, half4(red, green, blue, 1), _AnimationFactor);
			}
			
			
			ENDCG
		}
	} 
}