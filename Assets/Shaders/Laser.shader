﻿Shader "Convex/Laser" {
	Properties {
		_MainTex ("Base (RGBA)", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent+10" }
		
		Pass {
		
			Blend SrcAlpha One
			Cull Off
			ZWrite off
			Lighting off
			
			CGPROGRAM
			#include "UnityCG.cginc"
			#pragma vertex vert
			#pragma fragment frag
			
			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			struct v2f {
				float4 pos: SV_POSITION;
				float2 uv1: TEXCOORD0;
				float2 uv2: TEXCOORD1;
				fixed4 color: COLOR0;
			};
			
			v2f vert(appdata_full v) {
				v2f o;
				
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv1 = v.texcoord * _MainTex_ST.xy + _MainTex_ST.zw * _Time.xx;
				o.uv2 = v.texcoord * _MainTex_ST.xy - _MainTex_ST.zw * _Time.xx;
				o.color = v.color;
				
				return o;
			}
			
			fixed4 frag(v2f i):COLOR {
				
				fixed4 t1 = tex2D (_MainTex, i.uv1);
				fixed4 t2 = tex2D (_MainTex, i.uv2);
				
				return (t1 + t2) * 0.5 * i.color;
			}
			
			ENDCG
		}
	} 
}
