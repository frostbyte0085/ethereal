﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public static class FixCrystals {

	[MenuItem("Tools/Fix Crystals")]
	public static void Fix() {

		EditorUtility.ClearProgressBar();

		for (int i=0; i<Selection.gameObjects.Length; i++) {
			GameObject go = Selection.gameObjects[i];

			SpriteRenderer current = go.GetComponent<SpriteRenderer>();
			if (current == null) continue;

			GameObject child = new GameObject("sprite");

			SpriteRenderer sp = child.AddComponent <SpriteRenderer>();
			sp.sprite = current.sprite;
			sp.sortingLayerID = current.sortingLayerID;
			sp.sortingLayerName = current.sortingLayerName;
			sp.sortingOrder = current.sortingOrder;
			sp.color = current.color;
			sp.sharedMaterial = current.sharedMaterial;

			Object.DestroyImmediate (current);

			child.transform.parent = go.transform;
			child.transform.localPosition = Vector3.zero;

			Collider2D c2d = go.GetComponent<Collider2D>();
			if (c2d != null) {
				Object.DestroyImmediate (c2d);
			}

			CircleCollider2D circle = go.AddComponent <CircleCollider2D>();
			circle.radius = sp.bounds.size.magnitude * 0.5f;
			circle.isTrigger = true;

			EditorUtility.DisplayProgressBar ("Working", "Fixing crystals", (float)i/(float)(Selection.gameObjects.Length-1));
		}

		EditorUtility.ClearProgressBar();

	}
}