﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using UnityEditor.Callbacks;	

public static class DeviceSettingsCopy {

	[PostProcessBuild]
	public static void OnPostProcessBuild (BuildTarget target, string path) {

#if UNITY_IPHONE
		string source = path + "/../XCodeReplaceFiles";

		string[] files = Directory.GetFiles (source);
		if (files != null && files.Length > 0) {

			for (int i=0; i<files.Length; i++) {
				string filename = Path.GetFileName (files[i]);

				// search in the build path for this file
				string[] targetFiles = Directory.GetFiles (path, "*", SearchOption.AllDirectories);
				if (targetFiles != null && targetFiles.Length > 0) {

					for (int targetID=0; targetID<targetFiles.Length; targetID++) {

						string targetFilename = Path.GetFileName(targetFiles[targetID]);



						// we found it.
						if (targetFilename == filename) {

							//Debug.Log ("source: " + files[i] + ", " + "dest: " + targetFiles[targetid]);
							File.Copy (files[i], targetFiles[targetID], true);
							Debug.LogWarning ("overwrite: " + targetFiles[targetID]);
						}
					}
				}
			}

		}

#endif

	}
}
