using UnityEngine;
using UnityEditor;
using System.Collections;

public class PlayerPrefsShortcuts : ScriptableObject
{

	[MenuItem("Tools/Clear Prefs")]
	static void Clear()
	{
		if(!EditorUtility.DisplayDialog("Are you sure?", "Clear Player Prefs?", "No", "Yes"))
		{
			PlayerPrefs.DeleteAll();		
		}
	}

}

