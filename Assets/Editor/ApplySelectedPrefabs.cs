﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public static class ApplySelectedPrefabs {
	
	[MenuItem("Tools/Apply Prefab(s)")]
	public static void Apply() {

		for (int i=0; i<Selection.gameObjects.Length; i++) {
			GameObject go = Selection.gameObjects[i];

			PrefabUtility.ReplacePrefab(go, PrefabUtility.GetPrefabParent(go), ReplacePrefabOptions.ConnectToPrefab);
		}

	}
	
}