﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public static class SpaceshipDetailsAsset {

	[MenuItem("Assets/Create/Spaceship Details")]
	public static void Create() {
		ScriptableObjectUtility.CreateAsset <SpaceshipDetails>();
	}

}