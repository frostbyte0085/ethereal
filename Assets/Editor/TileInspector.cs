﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CanEditMultipleObjects]
[CustomEditor(typeof(Tile))]
public class TileInspector : Editor {

	// Use this for initialization
	void Awake() {
	}

	public override void OnInspectorGUI() {
		this.DrawDefaultInspector();

		EditorGUILayout.Separator();
		Color prevColor = GUI.backgroundColor;

		EditorGUILayout.BeginHorizontal();

		EditorGUILayout.LabelField ("Elements");
		GUI.backgroundColor = Color.green;
		if (GUILayout.Button("+", GUILayout.MaxWidth(32))) {
			for (int i=0; i<targets.Length; i++) {
				Tile t = (Tile)targets[i];
				t.elements.Insert (0, null);
			}
		}

		GUI.backgroundColor = prevColor;

		EditorGUILayout.EndHorizontal();

		if (targets.Length == 1) {
			Tile tile = (Tile)targets[0];

			for (int i=tile.elements.Count-1; i>=0; i--) {
				EditorGUILayout.BeginHorizontal();

				tile.elements[i] = (TileElement)EditorGUILayout.ObjectField (tile.elements[i], typeof(TileElement), true);
				GUI.backgroundColor = Color.red;
				if (GUILayout.Button ("-", GUILayout.MaxWidth(32))) {
					tile.elements.RemoveAt (i);
				}
				GUI.backgroundColor = prevColor;

				EditorGUILayout.EndHorizontal();
			}
		}
		else {
			GUILayout.Label ("Multiple editing is not available for tile elements");
		}

		GUI.backgroundColor = Color.red;
		if (GUILayout.Button ("Clear")) {
			clear ();
		}

		GUI.backgroundColor = new Color(1, 0.8f, 0f, 1f);
		if (GUILayout.Button ("Clean")) {
			for (int i=0; i<targets.Length; i++) {
				clean ((Tile)targets[i]);
			}
		}

		GUI.backgroundColor = Color.green;
		if (GUILayout.Button ("Build")) {

			for (int i=0; i<targets.Length; i++) {
				Tile t = (Tile)targets[i];

				EditorUtility.DisplayProgressBar ("Building...", "Building tiles, please wait", ((float)(i+1))/(float)targets.Length);

				if (t.elements.Count == 0) {
					Debug.LogWarning ("Tile: " + t.name + " has no elements, ignoring...");
				}
				else {

					build (t);
				}

				EditorUtility.SetDirty (targets[i]);
			}

			EditorUtility.ClearProgressBar();
		}

		if (GUILayout.Button ("Apply Prefab")) {
			for (int i=0; i<targets.Length; i++) {
				Tile t = (Tile)targets[i];

				EditorUtility.DisplayProgressBar ("Applying...", "Applying tile prefabs, please wait", ((float)(i+1))/(float)targets.Length);

				PrefabUtility.ReplacePrefab(t.gameObject, PrefabUtility.GetPrefabParent(t.gameObject), ReplacePrefabOptions.ConnectToPrefab);
			}
			EditorUtility.ClearProgressBar();
			AssetDatabase.SaveAssets();
		}

		GUI.backgroundColor = prevColor;
	}

	private void clear() {

		for (int i=0; i<targets.Length; i++) {
			Tile t = (Tile)targets[i];

			t.elements.Clear();

			clean(t);
		}
	}

	private void clean(Tile tile) {

		Transform elementsTrans = tile.transform.Find ("elements");
		if (elementsTrans != null) {
			DestroyImmediate (elementsTrans.gameObject);
		}

		Transform warpdriveTrans = tile.transform.Find ("warpdrivePath");
		if (warpdriveTrans != null) {
			DestroyImmediate (warpdriveTrans.gameObject);
		}
	}

	private void build(Tile tile) {
		clean (tile);


		GameObject elementsGO = new GameObject ("elements");
		elementsGO.transform.parent = tile.transform;
		elementsGO.transform.localPosition = Vector3.zero;


		List<TileElement> reversedElements = new List<TileElement> (tile.elements);
		reversedElements.Reverse();


		GameObject warpdrivePathGO = new GameObject("warpdrivePath");
		warpdrivePathGO.transform.parent = tile.transform;
		warpdrivePathGO.transform.localPosition = Vector3.zero;

		List<GameObject> currentWarpdrivePoints = new List<GameObject>();

		GameObject prevElement = null;

		for (int i=0; i<reversedElements.Count; i++) {

			GameObject e = Instantiate (reversedElements[i].gameObject) as GameObject;
			e.transform.parent = elementsGO.transform;
			e.transform.localPosition = Vector3.right * Constants.tileScreenSize * i;

			TileElement script = e.GetComponent<TileElement>();

			// get the multimask texture name from the object name, stripping the Clone prefix.
			string texname = e.name;//e.name.Substring (0, e.name.LastIndexOf ("(Clone)"));
			Utilities.RemovePostfix (ref texname, "(Clone)");
			texname += "_multimask";

			Texture2D mm = Resources.Load (texname) as Texture2D;

			//warpdrive path based on texture alpha
			if (mm != null) {

				for (int x=0; x<mm.width; x+=script.warpdriveStepX) {

					List<float> avgY = new List<float>();

					for (int y=0; y<mm.height; y+=script.warpdriveStepY) {
						
						Color c = mm.GetPixel (x,y);
						if (c.a > 0.9f) {
							avgY.Add (y);
						}
					}

					/*
					GameObject pointGO = createAveraged (null, x, avgY, warpdrivePathGO.transform, mm.width, mm.height);
					if (pointGO != null) {

						//Vector3 pos = pointGO.transform.localPosition;
						//pos.x -= mm.width/2;
						//pointGO.transform.localPosition = pos;

						currentWarpdrivePoints.Add (pointGO);
						pointGO.tag = TagStringManager.WarpdrivePoint;
					}
					*/

					if (avgY.Count > 0) {

						GameObject pointGO = new GameObject("point"+currentWarpdrivePoints.Count);
						pointGO.transform.parent = warpdrivePathGO.transform;

						float pointY=0;

						foreach (float f in avgY) {
							pointY += f;
						}

						pointY /= avgY.Count;

						Vector3 pointPos = new Vector3( (Constants.tileScreenSize * 1f / mm.width) * (x-mm.width/2), 
						                               (Constants.tileScreenSize * 1f / mm.height) * (pointY-mm.height/2));

						pointGO.transform.localPosition = pointPos;
						if (prevElement != null) {
							pointGO.transform.localPosition += prevElement.transform.localPosition + Vector3.right * Constants.tileScreenSize;
						}

						pointGO.tag = TagStringManager.WarpdrivePoint;

						currentWarpdrivePoints.Add (pointGO);

					}


				}
			}
			else {
				// if we can't find this multimask for the warpdrive, create points across the length of the tile
				// 8 colinear points should be enough
				int wpcount = 8;
				for (int p=0; p<wpcount; p++) {
					if (prevElement != null && p == 0) continue;

					GameObject pointGO = new GameObject("point"+currentWarpdrivePoints.Count);
					pointGO.transform.parent = warpdrivePathGO.transform;

					float xx = Mathf.Lerp (0, Constants.tileScreenSize, (float)p / (float)(wpcount-1));

					pointGO.transform.localPosition = new Vector3 (xx-Constants.tileScreenSize*0.5f, 0f, 0f);
					if (prevElement != null) {
						pointGO.transform.localPosition += prevElement.transform.localPosition + Vector3.right * Constants.tileScreenSize;
					}
					
					pointGO.tag = TagStringManager.WarpdrivePoint;
					
					currentWarpdrivePoints.Add (pointGO);
				}
			}

			if (script.Foregrounds != null) {
				for (int fgIdx=0; fgIdx<script.Foregrounds.Length; fgIdx++) {
					GameObject fgGO = script.Foregrounds[fgIdx].gameObject;

					switch (script.foregroundColliderType) {
					case ColliderType.Polygon:
						fgGO.AddComponent<PolygonCollider2D>();
						break;

					case ColliderType.Circle:
						fgGO.AddComponent<CircleCollider2D>();
						break;

					case ColliderType.Box:
						fgGO.AddComponent<BoxCollider2D>();
						break;

					default:break;
					}

				}

			}

			prevElement = e;
		}

		if (tile.type == TileType.Adaptor) {
			// setup the adaptor start/end. let's pick the first for the start, and the last for the end!
			if (tile.elements.Count > 1) {

				// beware, they are reversed, remember!
				tile.adaptorEnd = elementsGO.transform.GetChild(elementsGO.transform.childCount-1);
				tile.adaptorStart = elementsGO.transform.GetChild(0);

			}

			//tile.adaptorEnd.transform.localPosition += Vector3.right * tile.adaptorEmptySpace;
		}

		smoothenCollectables (currentWarpdrivePoints);
		// delete the first and final warpdrive points
		GameObject.DestroyImmediate (currentWarpdrivePoints[0]);
		currentWarpdrivePoints.RemoveAt(0);

		GameObject.DestroyImmediate (currentWarpdrivePoints[currentWarpdrivePoints.Count-1]);
		currentWarpdrivePoints.RemoveAt(currentWarpdrivePoints.Count-1);

		/*
		for (int i=0; i<currentWarpdrivePoints.Count; i++) {
			Vector3 p = currentWarpdrivePoints[i].transform.localPosition;
			p.x -= (Constants.tileScreenSize * reversedElements.Count / 2);
			p.y -= (Constants.tileScreenSize / 2);

			currentWarpdrivePoints[i].transform.localPosition = p;
		}
		*/
	}

	private void smoothenCollectables (List<GameObject> collection) {

		List<Vector3> sources = new List<Vector3>();
		for (int i=0; i<collection.Count; i++) {
			sources.Add (collection[i].transform.localPosition);
		}

		for (int i=1; i<sources.Count-1; i++) {

			float y=0;

			y += sources[i-1].y;
			y += sources[i].y;
			y += sources[i+1].y;

			y /= 3;

			Vector3 pos = collection[i].transform.localPosition;
			pos.y = y;
			collection[i].transform.localPosition = pos;

		}
	}

	private GameObject createAveraged (Object o, float x, List<float> yCollection, Transform parent, float width, float height) {
		if (yCollection.Count > 0) {
			
			float avgY = 0;
			foreach (float y in yCollection) {
				avgY += y;
			}
			avgY /= yCollection.Count;

			GameObject go = null;
			if (o != null) {
				go = Instantiate (o) as GameObject;
			}
			else {
				// simply cerate a point
				go = new GameObject("point");
			}

			if (go != null) {
				go.transform.parent = parent;
				go.transform.localPosition = getCollectablePosition ( x, avgY, width, height);
			}

			return go;
		}

		return null;
	}

	private Vector3 getCollectablePosition(float x, float y, float width, float height) {
		return Constants.tileScreenSize * (1f / width) * (new Vector3(x,y,0) - new Vector3(width/2, height/2,0));
	}

}
