using UnityEngine;
using UnityEditor;
using System.Collections;

public class TakeScreenshot : ScriptableObject
{
	private static int ordinal;

	[MenuItem("Tools/Screenshot (Upsample)")]
	static void ScreenshotUpsampled()
	{
		Application.CaptureScreenshot(Application.dataPath + "/../shot_upsampled" + ordinal.ToString() + ".png", 2);
		ordinal++;
	}

	[MenuItem("Tools/Screenshot")]
	static void Screenshot()
	{
		Application.CaptureScreenshot(Application.dataPath + "/../shot" + ordinal.ToString() + ".png");
		ordinal++;
	}

}

